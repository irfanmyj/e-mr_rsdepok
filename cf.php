<?php 
class Cf{
	
	/*
	| -------------------------------------------------------------------
	| DATABASE CONNECTIVITY SETTINGS
	| -------------------------------------------------------------------
	| This file will contain the settings needed to access your database.
	|
	| For complete instructions please consult the 'Database Connection'
	| page of the User Guide.
	|
	| -------------------------------------------------------------------
	| EXPLANATION OF VARIABLES
	| -------------------------------------------------------------------
	|
	|	['dsn']      The full DSN string describe a connection to the database.
	|	['hostname'] The hostname of your database server.
	|	['username'] The username used to connect to the database
	|	['password'] The password used to connect to the database
	|	['database'] The name of the database you want to connect to
	|	['dbdriver'] The database driver. e.g.: mysqli.
	|			Currently supported:
	|				 cubrid, ibase, mssql, mysql, mysqli, oci8,
	|				 odbc, pdo, postgre, sqlite, sqlite3, sqlsrv
	|	['dbprefix'] You can add an optional prefix, which will be added
	|				 to the table name when using the  Query Builder class
	|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
	|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
	|	['cache_on'] TRUE/FALSE - Enables/disables query caching
	|	['cachedir'] The path to the folder where cache files should be stored
	|	['char_set'] The character set used in communicating with the database
	|	['dbcollat'] The character collation used in communicating with the database
	|				 NOTE: For MySQL and MySQLi databases, this setting is only used
	| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
	|				 (and in table creation queries made with DB Forge).
	| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
	| 				 can make your site vulnerable to SQL injection if you are using a
	| 				 multi-byte character set and are running versions lower than these.
	| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
	|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
	|	['encrypt']  Whether or not to use an encrypted connection.
	|
	|			'mysql' (deprecated), 'sqlsrv' and 'pdo/sqlsrv' drivers accept TRUE/FALSE
	|			'mysqli' and 'pdo/mysql' drivers accept an array with the following options:
	|
	|				'ssl_key'    - Path to the private key file
	|				'ssl_cert'   - Path to the public key certificate file
	|				'ssl_ca'     - Path to the certificate authority file
	|				'ssl_capath' - Path to a directory containing trusted CA certificates in PEM format
	|				'ssl_cipher' - List of *allowed* ciphers to be used for the encryption, separated by colons (':')
	|				'ssl_verify' - TRUE/FALSE; Whether verify the server certificate or not
	|
	|	['compress'] Whether or not to use client compression (MySQL only)
	|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
	|							- good for ensuring strict SQL while developing
	|	['ssl_options']	Used to set various SSL options that can be used when making SSL connections.
	|	['failover'] array - A array with 0 or more data for connections if the main should fail.
	|	['save_queries'] TRUE/FALSE - Whether to "save" all executed queries.
	| 				NOTE: Disabling this will also effectively disable both
	| 				$this->db->last_query() and profiling of DB queries.
	| 				When you run a query, with this setting set to TRUE (default),
	| 				CodeIgniter will store the SQL statement for debugging purposes.
	| 				However, this may cause high memory usage, especially if you run
	| 				a lot of SQL queries ... disable this to avoid that problem.
	|
	| The $active_group variable lets you choose which connection group to
	| make active.  By default there is only one group (the 'default' group).
	|
	| The $query_builder variables lets you determine whether or not to load
	| the query builder class.
	*/

	public $_hostname1		= '172.16.200.5'; // 172.16.200.5 
	public $_database1		= 'rsud_dpk_17';//'db_coba/rsud_dpk_17';
	public $_username1		= 'rsud'; // rsud 
	public $_password1		= 'rsud99'; // rsud99
	public $_dbprefix1		= ''; 
	public $_dbdriver1		= 'mysqli'; 
	public $_pconnect1		= FALSE; 
	public $_db_debug1		= (ENVIRONMENT !== 'production');
	public $_cache_on1		= FALSE;
	public $_cachedir1		= '';
	public $_char_set1		= 'utf8';
	public $_dbcollat1		= 'utf8_unicode_ci';
	public $_swap_pre1		= ''; 
	public $_encrypt1		= FALSE; 
	public $_compress1		= FALSE; 
	public $_stricton1		= FALSE; 
	public $_failover1		= array(); 
	public $_save_queries1	= TRUE; 
	public $_port1			= ''; 
	public $_group1 		= 'default';

	public $_hostname2		= ''; 
	public $_database2		= '';
	public $_username2		= ''; 
	public $_password2		= ''; 
	public $_dbprefix2		= ''; 
	public $_dbdriver2		= 'mysqli'; 
	public $_pconnect2		= FALSE; 
	public $_db_debug2		= (ENVIRONMENT !== 'production');
	public $_cache_on2		= FALSE;
	public $_cachedir2		= '';
	public $_char_set2		= 'utf8';
	public $_dbcollat2		= 'utf8_unicode_ci';
	public $_swap_pre2		= ''; 
	public $_encrypt2		= FALSE; 
	public $_compress2		= FALSE; 
	public $_stricton2		= FALSE; 
	public $_failover2		= array(); 
	public $_save_queries2	= TRUE; 
	public $_port2			= '';  

	/*
	public $_hostname1		= 'localhost'; // localhost
	public $_database1		= 'postgres'; // nama database
	public $_username1		= 'postgres'; // username database
	public $_password1		= 'bismillah'; // password database
	public $_dbprefix1		= 'bt_'; // prefix tabel database
	public $_dbdriver1		= 'postgre'; // prefix tabel database
	public $_port1			= 5432; // prefix tabel database
	public $_group1 		= 'default'; // nama database group
	*/

	/*
	| -------------------------------------------------------------------------
	| URI ROUTING
	| -------------------------------------------------------------------------
	| This file lets you re-map URI requests to specific controller functions.
	|
	| Typically there is a one-to-one relationship between a URL string
	| and its corresponding controller class/method. The segments in a
	| URL normally follow this pattern:
	|
	|	example.com/class/method/id/
	|
	| In some instances, however, you may want to remap this relationship
	| so that a different class/function is called than the one
	| corresponding to the URL.
	|
	| Please see the user guide for complete details:
	|
	|	https://codeigniter.com/user_guide/general/routing.html
	|
	| -------------------------------------------------------------------------
	| RESERVED ROUTES
	| -------------------------------------------------------------------------
	|
	| There are three reserved routes:
	|
	|	$route['default_controller'] = 'welcome';
	|
	| This route indicates which controller class should be loaded if the
	| URI contains no data. In the above example, the "welcome" class
	| would be loaded.
	|
	|	$route['404_override'] = 'errors/page_missing';
	|
	| This route will tell the Router which controller/method to use if those
	| provided in the URL cannot be matched to a valid route.
	|
	|	$route['translate_uri_dashes'] = FALSE;
	|
	| This is not exactly a route, but allows you to automatically route
	| controller and method names that contain dashes. '-' isn't a valid
	| class or method name character, so it requires translation.
	| When you set this option to TRUE, it will replace ALL dashes in the
	| controller and method URI segments.
	|
	| Examples:	my-controller/index	-> my_controller/index
	|		my-controller/my-method	-> my_controller/my_method
	*/
	public $route = array(
		'default_controller' => 'Auth',
		'login' => 'Auth/login',
		'login' => 'Auth/index',
		'404_override' => '',
		'translate_uri_dashes' => FALSE
	);

	/*
	---------------------------------------------------------------------------
	|##########################################################################
	| AUTO-LOADER -> dev/application/config/autoload.php
	|##########################################################################
	---------------------------------------------------------------------------
	*/

	/*
	| -------------------------------------------------------------------
	| AUTO-LOADER
	| -------------------------------------------------------------------
	| This file specifies which systems should be loaded by default.
	|
	| In order to keep the framework as light-weight as possible only the
	| absolute minimal resources are loaded by default. For example,
	| the database is not connected to automatically since no assumption
	| is made regarding whether you intend to use it.  This file lets
	| you globally define which systems you would like loaded with every
	| request.
	|
	| -------------------------------------------------------------------
	| Instructions
	| -------------------------------------------------------------------
	|
	| These are the things you can load automatically:
	|
	| 1. Packages
	| 2. Libraries
	| 3. Drivers
	| 4. Helper files
	| 5. Custom config files
	| 6. Language files
	| 7. Models
	|
	*/
	
	/*
	| -------------------------------------------------------------------
	|  Auto-load Packages
	| -------------------------------------------------------------------
	| Prototype:
	|
	|  $autoload['packages'] = array(APPPATH.'third_party', '/usr/local/shared');
	|
	*/
	public $packages 	= array();

	/*
	| -------------------------------------------------------------------
	|  Auto-load Libraries
	| -------------------------------------------------------------------
	| These are the classes located in system/libraries/ or your
	| application/libraries/ directory, with the addition of the
	| 'database' library, which is somewhat of a special case.
	|
	| Prototype:
	|
	|	$autoload['libraries'] = array('database', 'email', 'session');
	|
	| You can also supply an alternative library name to be assigned
	| in the controller:
	|
	|	$autoload['libraries'] = array('user_agent' => 'ua');
	*/
	public $libraries 	= array('database');

	/*
	| -------------------------------------------------------------------
	|  Auto-load Drivers
	| -------------------------------------------------------------------
	| These classes are located in system/libraries/ or in your
	| application/libraries/ directory, but are also placed inside their
	| own subdirectory and they extend the CI_Driver_Library class. They
	| offer multiple interchangeable driver options.
	|
	| Prototype:
	|
	|	$autoload['drivers'] = array('cache');
	|
	| You can also supply an alternative property name to be assigned in
	| the controller:
	|
	|	$autoload['drivers'] = array('cache' => 'cch');
	|
	*/
	public $drivers 	= array();

	/*
	| -------------------------------------------------------------------
	|  Auto-load Helper Files
	| -------------------------------------------------------------------
	| Prototype:
	|
	|	$autoload['helper'] = array('url', 'file');
	*/
	public $helper 		= array('url');

	/*
	| -------------------------------------------------------------------
	|  Auto-load Config files
	| -------------------------------------------------------------------
	| Prototype:
	|
	|	$autoload['config'] = array('config1', 'config2');
	|
	| NOTE: This item is intended for use ONLY if you have created custom
	| config files.  Otherwise, leave it blank.
	|
	*/
	public $config 		= array();

	/*
	| -------------------------------------------------------------------
	|  Auto-load Language files
	| -------------------------------------------------------------------
	| Prototype:
	|
	|	$autoload['language'] = array('lang1', 'lang2');
	|
	| NOTE: Do not include the "_lang" part of your file.  For example
	| "codeigniter_lang.php" would be referenced as array('codeigniter');
	|
	*/
	public $language 	= array();

	/*
	| -------------------------------------------------------------------
	|  Auto-load Models
	| -------------------------------------------------------------------
	| Prototype:
	|
	|	$autoload['model'] = array('first_model', 'second_model');
	|
	| You can also supply an alternative model name to be assigned
	| in the controller:
	|
	|	$autoload['model'] = array('first_model' => 'first');
	*/
	public $model 		= array();

	/*
	---------------------------------------------------------------------------
	|##########################################################################
	| CONFIG -> dev/application/config/config.php
	|##########################################################################
	---------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| Index File
	|--------------------------------------------------------------------------
	|
	| Typically this will be your index.php file, unless you've renamed it to
	| something else. If you are using mod_rewrite to remove the page set this
	| variable so that it is blank.
	|
	*/
	public $index_page		= '';

	/*
	|--------------------------------------------------------------------------
	| URI PROTOCOL
	|--------------------------------------------------------------------------
	|
	| This item determines which server global should be used to retrieve the
	| URI string.  The default setting of 'REQUEST_URI' works for most servers.
	| If your links do not seem to work, try one of the other delicious flavors:
	|
	| 'REQUEST_URI'    Uses $_SERVER['REQUEST_URI']
	| 'QUERY_STRING'   Uses $_SERVER['QUERY_STRING']
	| 'PATH_INFO'      Uses $_SERVER['PATH_INFO']
	|
	| WARNING: If you set this to 'PATH_INFO', URIs will always be URL-decoded!
	*/
	public $uri_protocol	= 'REQUEST_URI';

	/*
	|--------------------------------------------------------------------------
	| URL suffix
	|--------------------------------------------------------------------------
	|
	| This option allows you to add a suffix to all URLs generated by CodeIgniter.
	| For more information please see the user guide:
	|
	| https://codeigniter.com/user_guide/general/urls.html
	*/
	public $url_suffix	= 'REQUEST_URI';

	/*
	|--------------------------------------------------------------------------
	| Default Language
	|--------------------------------------------------------------------------
	|
	| This determines which set of language files should be used. Make sure
	| there is an available translation if you intend to use something other
	| than english.
	|
	*/
	public $languagec	= 'bahasa';

	/*
	|--------------------------------------------------------------------------
	| Default Character Set
	|--------------------------------------------------------------------------
	|
	| This determines which character set is used by default in various methods
	| that require a character set to be provided.
	|
	| See http://php.net/htmlspecialchars for a list of supported charsets.
	|
	*/
	public $charset = 'UTF-8';

	/*
	|--------------------------------------------------------------------------
	| Enable/Disable System Hooks
	|--------------------------------------------------------------------------
	|
	| If you would like to use the 'hooks' feature you must enable it by
	| setting this variable to TRUE (boolean).  See the user guide for details.
	|
	*/
	public $enable_hooks = TRUE;

	/*
	|--------------------------------------------------------------------------
	| Class Extension Prefix
	|--------------------------------------------------------------------------
	|
	| This item allows you to set the filename/classname prefix when extending
	| native libraries.  For more information please see the user guide:
	|
	| https://codeigniter.com/user_guide/general/core_classes.html
	| https://codeigniter.com/user_guide/general/creating_libraries.html
	|
	*/
	public $subclass_prefix = 'MY_';

	/*
	|--------------------------------------------------------------------------
	| Composer auto-loading
	|--------------------------------------------------------------------------
	|
	| Enabling this setting will tell CodeIgniter to look for a Composer
	| package auto-loader script in application/vendor/autoload.php.
	|
	|	$config['composer_autoload'] = TRUE;
	|
	| Or if you have your vendor/ directory located somewhere else, you
	| can opt to set a specific path as well:
	|
	|	$config['composer_autoload'] = '/path/to/vendor/autoload.php';
	|
	| For more information about Composer, please visit http://getcomposer.org/
	|
	| Note: This will NOT disable or override the CodeIgniter-specific
	|	autoloading (application/config/autoload.php)
	*/
	public $composer_autoload = 'vendor/autoload.php';

	/*
	|--------------------------------------------------------------------------
	| Allowed URL Characters
	|--------------------------------------------------------------------------
	|
	| This lets you specify which characters are permitted within your URLs.
	| When someone tries to submit a URL with disallowed characters they will
	| get a warning message.
	|
	| As a security measure you are STRONGLY encouraged to restrict URLs to
	| as few characters as possible.  By default only these are allowed: a-z 0-9~%.:_-
	|
	| Leave blank to allow all characters -- but only if you are insane.
	|
	| The configured value is actually a regular expression character group
	| and it will be executed as: ! preg_match('/^[<permitted_uri_chars>]+$/i
	|
	| DO NOT CHANGE THIS UNLESS YOU FULLY UNDERSTAND THE REPERCUSSIONS!!
	| 'a-z 0-9~%.:_\-@\='; 'a-z 0-9~%.:_\-';
	*/
	public $permitted_uri_chars  = 'a-z 0-9~%.:_\-@\=';

	/*
	|--------------------------------------------------------------------------
	| Enable Query Strings
	|--------------------------------------------------------------------------
	|
	| By default CodeIgniter uses search-engine friendly segment based URLs:
	| example.com/who/what/where/
	|
	| You can optionally enable standard query string based URLs:
	| example.com?who=me&what=something&where=here
	|
	| Options are: TRUE or FALSE (boolean)
	|
	| The other items let you set the query string 'words' that will
	| invoke your controllers and its functions:
	| example.com/index.php?c=controller&m=function
	|
	| Please note that some of the helpers won't work as expected when
	| this feature is enabled, since CodeIgniter is designed primarily to
	| use segment based URLs.
	|
	*/
	public $enable_query_strings = FALSE;
	public $controller_trigger = 'c';
	public $function_trigger = 'm';
	public $directory_trigger = 'd';

	/*
	|--------------------------------------------------------------------------
	| Allow $_GET array
	|--------------------------------------------------------------------------
	|
	| By default CodeIgniter enables access to the $_GET array.  If for some
	| reason you would like to disable it, set 'allow_get_array' to FALSE.
	|
	| WARNING: This feature is DEPRECATED and currently available only
	|          for backwards compatibility purposes!
	|
	*/
	public $allow_get_array = TRUE;

	/*
	|--------------------------------------------------------------------------
	| Error Logging Threshold
	|--------------------------------------------------------------------------
	|
	| You can enable error logging by setting a threshold over zero. The
	| threshold determines what gets logged. Threshold options are:
	|
	|	0 = Disables logging, Error logging TURNED OFF
	|	1 = Error Messages (including PHP errors)
	|	2 = Debug Messages
	|	3 = Informational Messages
	|	4 = All Messages
	|
	| You can also pass an array with threshold levels to show individual error types
	|
	| 	array(2) = Debug Messages, without Error Messages
	|
	| For a live site you'll usually only enable Errors (1) to be logged otherwise
	| your log files will fill up very fast.
	|
	*/
	public $log_threshold = array(1);

	/*
	|--------------------------------------------------------------------------
	| Error Logging Directory Path
	|--------------------------------------------------------------------------
	|
	| Leave this BLANK unless you would like to set something other than the default
	| application/logs/ directory. Use a full server path with trailing slash.
	|
	*/
	public $log_path = '';

	/*
	|--------------------------------------------------------------------------
	| Log File Extension
	|--------------------------------------------------------------------------
	|
	| The default filename extension for log files. The default 'php' allows for
	| protecting the log files via basic scripting, when they are to be stored
	| under a publicly accessible directory.
	|
	| Note: Leaving it blank will default to 'php'.
	|
	*/
	public $log_file_extension = 'txt';

	/*
	|--------------------------------------------------------------------------
	| Log File Permissions
	|--------------------------------------------------------------------------
	|
	| The file system permissions to be applied on newly created log files.
	|
	| IMPORTANT: This MUST be an integer (no quotes) and you MUST use octal
	|            integer notation (i.e. 0700, 0644, etc.)
	*/
	public $log_file_permissions = 0644;

	/*
	|--------------------------------------------------------------------------
	| Date Format for Logs
	|--------------------------------------------------------------------------
	|
	| Each item that is logged has an associated date. You can use PHP date
	| codes to set your own date formatting
	|
	*/
	public $log_date_format = 'Y-m-d H:i:s';

	/*
	|--------------------------------------------------------------------------
	| Error Views Directory Path
	|--------------------------------------------------------------------------
	|
	| Leave this BLANK unless you would like to set something other than the default
	| application/views/errors/ directory.  Use a full server path with trailing slash.
	|
	*/
	public $error_views_path = '';

	/*
	|--------------------------------------------------------------------------
	| Cache Directory Path
	|--------------------------------------------------------------------------
	|
	| Leave this BLANK unless you would like to set something other than the default
	| application/cache/ directory.  Use a full server path with trailing slash.
	|
	*/
	public $cache_path = '';

	/*
	|--------------------------------------------------------------------------
	| Cache Include Query String
	|--------------------------------------------------------------------------
	|
	| Whether to take the URL query string into consideration when generating
	| output cache files. Valid options are:
	|
	|	FALSE      = Disabled
	|	TRUE       = Enabled, take all query parameters into account.
	|	             Please be aware that this may result in numerous cache
	|	             files generated for the same page over and over again.
	|	array('q') = Enabled, but only take into account the specified list
	|	             of query parameters.
	|
	*/
	public $cache_query_string = FALSE;

	/*
	|--------------------------------------------------------------------------
	| Encryption Key
	|--------------------------------------------------------------------------
	|
	| If you use the Encryption class, you must set an encryption key.
	| See the user guide for more info.
	|
	| https://codeigniter.com/user_guide/libraries/encryption.html
	|
	*/
	public $encryption_key = 'mencoba#membuat~karya2019';

	/*
	|--------------------------------------------------------------------------
	| Session Variables
	|--------------------------------------------------------------------------
	|
	| 'sess_driver'
	|
	|	The storage driver to use: files, database, redis, memcached
	|
	| 'sess_cookie_name'
	|
	|	The session cookie name, must contain only [0-9a-z_-] characters
	|
	| 'sess_expiration'
	|
	|	The number of SECONDS you want the session to last.
	|	Setting to 0 (zero) means expire when the browser is closed.
	|
	| 'sess_save_path'
	|
	|	The location to save sessions to, driver dependent.
	|
	|	For the 'files' driver, it's a path to a writable directory.
	|	WARNING: Only absolute paths are supported!
	|
	|	For the 'database' driver, it's a table name.
	|	Please read up the manual for the format with other session drivers.
	|
	|	IMPORTANT: You are REQUIRED to set a valid save path!
	|
	| 'sess_match_ip'
	|
	|	Whether to match the user's IP address when reading the session data.
	|
	|	WARNING: If you're using the database driver, don't forget to update
	|	         your session table's PRIMARY KEY when changing this setting.
	|
	| 'sess_time_to_update'
	|
	|	How many seconds between CI regenerating the session ID.
	|
	| 'sess_regenerate_destroy'
	|
	|	Whether to destroy session data associated with the old session ID
	|	when auto-regenerating the session ID. When set to FALSE, the data
	|	will be later deleted by the garbage collector.
	|
	| Other session cookie settings are shared with the rest of the application,
	| except for 'cookie_prefix' and 'cookie_httponly', which are ignored here.
	|
	*/
	public $sess_driver = 'files';
	public $sess_cookie_name = NULL;
	public $sess_expiration = 7200;
	public $sess_save_path = '/opt/lampp/temp/';
	public $sess_match_ip = FALSE;
	public $sess_time_to_update = 300;
	public $sess_regenerate_destroy = FALSE;
	
	/*
	|--------------------------------------------------------------------------
	| Cookie Related Variables
	|--------------------------------------------------------------------------
	|
	| 'cookie_prefix'   = Set a cookie name prefix if you need to avoid collisions
	| 'cookie_domain'   = Set to .your-domain.com for site-wide cookies
	| 'cookie_path'     = Typically will be a forward slash
	| 'cookie_secure'   = Cookie will only be set if a secure HTTPS connection exists.
	| 'cookie_httponly' = Cookie will only be accessible via HTTP(S) (no javascript)
	|
	| Note: These settings (with the exception of 'cookie_prefix' and
	|       'cookie_httponly') will also affect sessions.
	|
	*/
	public $cookie_prefix = '';
	public $cookie_domain = '';
	public $cookie_path = '/';
	public $cookie_secure = FALSE;
	public $cookie_httponly = FALSE;

	/*
	|--------------------------------------------------------------------------
	| Standardize newlines
	|--------------------------------------------------------------------------
	|
	| Determines whether to standardize newline characters in input data,
	| meaning to replace \r\n, \r, \n occurrences with the PHP_EOL value.
	|
	| WARNING: This feature is DEPRECATED and currently available only
	|          for backwards compatibility purposes!
	|
	*/
	public $standardize_newlines = FALSE;

	/*
	|--------------------------------------------------------------------------
	| Global XSS Filtering
	|--------------------------------------------------------------------------
	|
	| Determines whether the XSS filter is always active when GET, POST or
	| COOKIE data is encountered
	|
	| WARNING: This feature is DEPRECATED and currently available only
	|          for backwards compatibility purposes!
	|
	*/
	public $global_xss_filtering = FALSE;

	/*
	|--------------------------------------------------------------------------
	| Cross Site Request Forgery
	|--------------------------------------------------------------------------
	| Enables a CSRF cookie token to be set. When set to TRUE, token will be
	| checked on a submitted form. If you are accepting user data, it is strongly
	| recommended CSRF protection be enabled.
	|
	| 'csrf_token_name' = The token name
	| 'csrf_cookie_name' = The cookie name
	| 'csrf_expire' = The number in seconds the token should expire.
	| 'csrf_regenerate' = Regenerate token on every submission
	| 'csrf_exclude_uris' = Array of URIs which ignore CSRF checks
	*/
	public $csrf_protection = FALSE;
	public $csrf_token_name = 'csrf_test_name';
	public $csrf_cookie_name = 'csrf_cookie_name';
	public $csrf_expire = 7200;
	public $csrf_regenerate = TRUE;
	public $csrf_exclude_uris = array();

	/*
	|--------------------------------------------------------------------------
	| Output Compression
	|--------------------------------------------------------------------------
	|
	| Enables Gzip output compression for faster page loads.  When enabled,
	| the output class will test whether your server supports Gzip.
	| Even if it does, however, not all browsers support compression
	| so enable only if you are reasonably sure your visitors can handle it.
	|
	| Only used if zlib.output_compression is turned off in your php.ini.
	| Please do not use it together with httpd-level output compression.
	|
	| VERY IMPORTANT:  If you are getting a blank page when compression is enabled it
	| means you are prematurely outputting something to your browser. It could
	| even be a line of whitespace at the end of one of your scripts.  For
	| compression to work, nothing can be sent before the output buffer is called
	| by the output class.  Do not 'echo' any values with compression enabled.
	|
	*/
	public $compress_output = FALSE;

	/*
	|--------------------------------------------------------------------------
	| Master Time Reference
	|--------------------------------------------------------------------------
	|
	| Options are 'local' or any PHP supported timezone. This preference tells
	| the system whether to use your server's local time as the master 'now'
	| reference, or convert it to the configured one timezone. See the 'date
	| helper' page of the user guide for information regarding date handling.
	|
	*/
	public $time_reference = 'local';

	/*
	|--------------------------------------------------------------------------
	| Rewrite PHP Short Tags
	|--------------------------------------------------------------------------
	|
	| If your PHP installation does not have short tag support enabled CI
	| can rewrite the tags on-the-fly, enabling you to utilize that syntax
	| in your view files.  Options are TRUE or FALSE (boolean)
	|
	| Note: You need to have eval() enabled for this to work.
	|
	*/
	public $rewrite_short_tags = FALSE;

	/*
	|--------------------------------------------------------------------------
	| Reverse Proxy IPs
	|--------------------------------------------------------------------------
	|
	| If your server is behind a reverse proxy, you must whitelist the proxy
	| IP addresses from which CodeIgniter should trust headers such as
	| HTTP_X_FORWARDED_FOR and HTTP_CLIENT_IP in order to properly identify
	| the visitor's IP address.
	|
	| You can use both an array or a comma-separated list of proxy addresses,
	| as well as specifying whole subnets. Here are a few examples:
	|
	| Comma-separated:	'10.0.1.200,192.168.5.0/24'
	| Array:		array('10.0.1.200', '192.168.5.0/24')
	*/
	public $proxy_ips = '';

	/*
	|--------------------------------------------------------------------------
	| LOG VIEWR
	|--------------------------------------------------------------------------
	| konfigurasi log
	*/
	public $clv_log_folder_path = "logs";
	public $clv_log_file_pattern = "log-*.txt";

	/*
	KONFIGURASI
	*/
	public $port 			= '';
	
	public $default_limit	= 5; //Limit minimal jumlah data tabel yang dimunculkan 
	
	public $limit_rows		= array(
								5 => 5, 
								10 => 10, 
								25 => 25,
								50 => 50, 
								100 => 100, 
								500 => 500, 
								1000 => 1000
							); // Pilihan jumlah limit

	public $access_level	= array(
								'View', 
								'Add', 
								'Update', 
								'Delete', 
								'Download', 
								'Upload'
							); // Akses level setiap module/controller

	public $status			= array(
								'Y' => 'Aktif',
								'N' => 'Tidak Aktif'
							);

	public $dayShow		= 7;

	public $configEmail	= array(
		'protocol' => 'smtp',
		'smtp_host' => 'ssl://smtp.googlemail.com',
		'smtp_port' => 465,
		'smtp_timeout' => '7',
		'smtp_user' => 'rsuddepokinfo@gmail.com',
		'smtp_pass' => 'rsud99!@',
		'charset' => 'utf-8',
		'newline' => "\r\n",
		'mailtype' => 'html'
	);

	public $_kategoriform = array(
		'RAJAL' => array(
			1 => 'SOAP',
			2 => 'DIAGNOSA',
			3 => 'RESEP',
			4 => 'LABORATORIUM',
			5 => 'RADIOLOGI',
			6 => 'TINDAKAN'
		)
	);
}