// lazyload config
var MODULE_CONFIG = {
    dataTable:      [
                      'http://localhost/appcore/vendor/assets_flatkit/libs/jquery/datatables/media/js/jquery.dataTables.min.js',
                      'http://localhost/appcore/vendor/assets_flatkit/libs/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.js',
                      'http://localhost/appcore/vendor/assets_flatkit/libs/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.css'],
    footable:       [
                      'http://localhost/appcore/vendor/assets_flatkit/libs/jquery/footable/dist/footable.all.min.js',
                      'http://localhost/appcore/vendor/assets_flatkit/libs/jquery/footable/css/footable.core.css'
                    ],
    screenfull:     [
                      'http://localhost/appcore/vendor/assets_flatkit/libs/jquery/screenfull/dist/screenfull.min.js'
                    ],
    sortable:       [
                      'http://localhost/appcore/vendor/assets_flatkit/libs/jquery/html.sortable/dist/html.sortable.min.js'
                    ],
    summernote:     [
                      'http://localhost/appcore/vendor/assets_flatkit/libs/jquery/summernote/dist/summernote.css',
                      'http://localhost/appcore/vendor/assets_flatkit/libs/jquery/summernote/dist/summernote.js'
                    ],
    parsley:        [
                      'http://localhost/appcore/vendor/assets_flatkit/libs/jquery/parsleyjs/dist/parsley.css',
                      'http://localhost/appcore/vendor/assets_flatkit/libs/jquery/parsleyjs/dist/parsley.min.js'
                    ],
    select2:        [
                      'http://localhost/appcore/vendor/assets_flatkit/libs/jquery/select2/dist/css/select2.min.css',
                      'http://localhost/appcore/vendor/assets_flatkit/libs/jquery/select2-bootstrap-theme/dist/select2-bootstrap.min.css',
                      'http://localhost/appcore/vendor/assets_flatkit/libs/jquery/select2-bootstrap-theme/dist/select2-bootstrap.4.css',
                      'http://localhost/appcore/vendor/assets_flatkit/libs/jquery/select2/dist/js/select2.min.js'
                    ],
    datetimepicker: [
                      'http://localhost/appcore/vendor/assets_flatkit/libs/jquery/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css',
                      'http://localhost/appcore/vendor/assets_flatkit/libs/jquery/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.dark.css',
                      'http://localhost/appcore/vendor/assets_flatkit/libs/js/moment/moment.js',
                      'http://localhost/appcore/vendor/assets_flatkit/libs/jquery/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'
                    ],
    bootstrapWizard:[
                      'http://localhost/appcore/vendor/assets_flatkit/libs/jquery/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js'
                    ]
  };
