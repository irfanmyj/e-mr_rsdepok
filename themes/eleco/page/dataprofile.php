<?php get_template('inc/navbar-page.html'); ?>
<!-- single post -->
	<div class="segments-page">
		<div class="container">
			<div class="wrap-title">
				<h5>NAMA : <?php echo $row[0]->nm_pasien; ?> | NO RKM MEDIS : <?php echo $row[0]->no_rkm_medis; ?></h5>
			</div>
			<div class="wrap-content b-shadow">
				<table class="table table-default">
					<thead>
						<tr>
							<th width="200">Kolom</th>
							<th width="10"></th>
							<th>Keterangan</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><b>No KTP</b></td>
							<td>:</td>
							<td><?php echo $row[0]->no_ktp; ?> (Ini adalah kode password anda)</td>
						</tr>
						<tr>
							<td><b>Jenis Kelamin</b></td>
							<td>:</td>
							<td><?php echo ($row[0]->jk=='L') ? 'Laki-laki' : 'Perempuan'; ?></td>
						</tr>
						<tr>
							<td><b>Tempat Lahir</b></td>
							<td>:</td>
							<td><?php echo $row[0]->tmp_lahir; ?></td>
						</tr>
						<tr>
							<td><b>Tanggal Lahir</b></td>
							<td>:</td>
							<td><?php echo date('D m Y',strtotime($row[0]->tgl_lahir)); ?></td>
						</tr>
						<tr>
							<td><b>Nama Ibu</b></td>
							<td>:</td>
							<td><?php echo $row[0]->nm_ibu; ?></td>
						</tr>
						<tr>
							<td><b>Golongan Darah</b></td>
							<td>:</td>
							<td><?php echo $row[0]->gol_darah; ?></td>
						</tr>
						<tr>
							<td><b>Pekerjaan</b></td>
							<td>:</td>
							<td><?php echo $row[0]->pekerjaan; ?></td>
						</tr>
						<tr>
							<td><b>Status Nikah</b></td>
							<td>:</td>
							<td><?php echo $row[0]->stts_nikah; ?></td>
						</tr>
						<tr>
							<td><b>Agama</b></td>
							<td>:</td>
							<td><?php echo $row[0]->agama; ?></td>
						</tr>
						<tr>
							<td><b>Alamat</b></td>
							<td>:</td>
							<td><?php echo $row[0]->alamat; ?></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<!-- end single post -->
<?php get_template('inc/footer.html'); ?>

<?php get_template('inc/endhtml.html'); ?>