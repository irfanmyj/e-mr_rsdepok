<?php get_template('inc/navbar-page.html'); ?>
<div class="tabs-page segments-page">
	<div class="container">
		<div class="wrap-title">
			<h5><?php echo strtoupper($title); ?></h5>
		</div>
		<div class="tabs b-shadow">
			<div class="nav nav-tabs justify-content-center" id="nav-tab" role="tablist">
				<a href="#nav-tabs1" class="nav-item nav-link active" id="nav-tabs1-tab" data-toggle="tab" role="tab" aria-controls="nav-tabs1" aria-selected="true">Riwayat Booking</a>
				<a href="#nav-tabs2" class="nav-item nav-link" id="nav-tabs2-tab" data-toggle="tab" role="tab" aria-controls="nav-tabs2" aria-selected="false">Riwayat Pemeriksaan</a>
			</div>
		</div>
		<div class="tab-content b-shadow" id="nav-tabContent">
			<div class="tab-pane fade show active" id="nav-tabs1" role="tabpanel" aria-labelledby="nav-tabs1-tab">
				<div class="container" style="padding: 10px; background: #ffffff;">
					<table class="table table-striped table-responsive display" style="width:100%" id="example">
						<thead>
							<tr>
								<th>Nomor Antrian</th>
								<th>Waktu Datang</th>
								<th>No Rm</th>
								<th>Poliklinik</th>
								<th>Dokter</th>
								<th>Cara Bayar</th>
								<th>Tanggal Periksa</th>
								<th>Status</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$dNow = date('Y-m-d');

							foreach ($r1 as $k => $v) { 
								$cd = base_url('booking/cetak_data/'.$v->kd_poli.'/'.$v->kd_dokter.'/'.$v->tanggal_periksa.'');

							?>
							<tr>
								<td><?php echo $v->no_reg; ?></td>
								<td><?php echo $v->waktu_kunjungan; ?></td>
								<td><?php echo $v->no_rkm_medis; ?></td>
								<td><?php echo $v->nm_poli; ?></td>
								<td><?php echo $v->nm_dokter; ?></td>
								<td><?php echo $v->png_jawab; ?></td>
								<td><?php echo $v->tanggal_periksa; ?></td>
								<td>
								<?php
								if($v->status == 'Belum')
								{
									echo 'Belum Registrasi Ulang';
								}
								else if($v->status == 'Dokter Berhalangan')
								{
									echo 'Dibatalakan Dokter Tidak Praktik';
								}
								else if ($v->status == 'Batal')
								{
									echo 'Booking Telah Dibatalkan';
								}
								else if($v->status == 'Terdaftar')
								{
									echo 'Sudah Terdafatar';
								}
								?>
								</td>
								<td>
									<div class="col-12">
										<div class="row">
											<div class="col-12">
												<a href="<?php echo $cd;?>" class="btn btn-success" target="_blank" title="Tombol untuk cetak data"><i class="fa fa-print"></i></a> 
											</div>
										</div>
									</div>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>

			<div class="tab-pane fade show" id="nav-tabs2" role="tabpanel" aria-labelledby="nav-tabs2-tab">
				<div class="container" style="padding: 10px; background: #ffffff;">
					<table class="table table-striped table-responsive display" style="width:100%" id="example1">
						<thead>
							<tr>
								<th>Nomor Rawat</th>
								<th>No Registrasi</th>
								<th>No Rm</th>
								<th>Nama Pasien</th>
								<th>Status Lanjut</th>
								<th>Dokter</th>
								<th>Cara Bayar</th>
								<th>Tanggal Registrasi</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$dNow = date('Y-m-d');
							foreach ($r2 as $k => $v) { 
							?>
							<tr <?php echo showValRec($v);?> style="cursor:pointer;">
								<td class="tr_mod"><?php echo $v->no_rawat; ?></td>
								<td class="tr_mod"><?php echo $v->no_reg; ?></td>
								<td class="tr_mod"><?php echo $v->no_rkm_medis; ?></td>
								<td class="tr_mod"><?php echo $v->nm_pasien; ?></td>
								<td class="tr_mod"><?php echo $v->status_lanjut; ?></td>
								<td class="tr_mod"><?php echo $v->nm_dokter; ?></td>
								<td class="tr_mod"><?php echo $v->png_jawab; ?></td>
								<td class="tr_mod"><?php echo tanggal_indo($v->tgl_registrasi,true); ?></td>
								<!-- <td class="tr_mod">
									<div class="col-12">
										<div class="row">
											<div class="col-12">
												<button class="btn btn-info" title="Tombol untuk lihat hasil pemeriksaan"><i class="fa fa-newspaper-o"></i></button>
											</div>
										</div>
									</div>
								</td> -->
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content" style="display: none;" id="modalUmum">
	<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModal2">
	    <div class="modal-dialog" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title">Riwayat Pemeriksaan</h5>
	                <button class="close" data-dismiss="modal" aria-label="close">
	                    <span aria-hidden="true"><i class="fa fa-close"></i></span>
	                </button>
	            </div>
	            <div class="modal-body" id="riwayat_pemeriksaan">
	            	
	            </div>
	            <div class="modal-footer">
	                <button type="button" id="tutups" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
	            </div>
	        </div>
	    </div>
	</div>
</div>
<?php get_template('inc/footer.html'); ?>
<script type="text/javascript">
	$(document).ready(function() {
		var link = '<?php echo base_url('Get_ajax');?>';
	    $('#example').DataTable();
	    $('#example1').DataTable();

	    $('.tr_mod').click(function(){
	    	var tr = $(this).parent();
	    	/*if(status == 'Terdaftar')
	    	{*/
	    		$.ajax({
	    			type : 'post',
	    			url : link+'/getRiwayatPemeriksaan',
	    			data : 'tgl_registrasi='+tr.data('tgl_registrasi')+'&kd_poli='+tr.data('kd_poli')+'&kd_dokter='+tr.data('kd_dokter')+'&no_reg='+tr.data('no_reg')+'&no_rawat='+tr.data('no_rawat')+'&nm_pasien='+tr.data('nm_pasien')+'&nm_dokter='+tr.data('nm_dokter')+'&png_jawab='+tr.data('png_jawab')+'&nm_poli='+tr.data('nm_poli'),
	    			success : function(res)
	    			{
	    				$('#riwayat_pemeriksaan').html(res);
	    				$('#modalUmum').css('display','block');
	    				$('#exampleModal2').modal();
	    			}
	    		});
	    	/*}else{
	    		alert('Anda tidak memiliki data pemeriksaan.');
	    	}*/
	    });
	} );
</script>
<?php get_template('inc/endhtml.html'); ?>