	<?php get_template('inc/navbar-page.html'); global $Cf;?>
	<!-- single post -->
	<div class="segments-page">
		<div class="container">
			<div class="single-post">
				<img src="<?php //echo user_files($r[0]->gambar);//$Cf->_file_akses.'/'.$r[0]->gambar; ?>" alt="">
				<a href=""><h3><?php echo $r[0]->judul; ?></h3></a>
				<p class="date"><i class="fa fa-clock-o"></i><?php echo date('d M Y',strtotime($r[0]->masuk_tgl)); ?></p>
				<p style="text-align: justify; font-size: 16px;"><?php echo $r[0]->artikel; ?></p>
			</div>
			<div class="author">
				<div class="content-image">
					
				</div>
				<div class="content-text">
					<h5>Admin Rsud Kota Depok</h5>
					<ul>
						<li>&nbsp;</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="related-post">
			<div class="section-title">
				<div class="container">
					<h3>Related Post</h3>
				</div>
			</div>
			<div class="container">
				<?php foreach ($rr as $k => $v) { ?>
				<div class="row">
					<div class="col-4">
						<div class="content-image">
							<img src="<?php echo $v->gambar; ?>" alt="">
						</div>
					</div>
					<div class="col-8">
						<div class="content-text">
							<a href="<?php echo base_url('artikel/detail/'.$v->id_artikel.'/'.str_replace(' ', '', $v->judul)); ?>"><h5><?php echo $v->judul; ?></h5></a>
							<p class="date"><i class="fa fa-clock-o"></i><?php echo date('d M Y',strtotime($v->masuk_tgl)); ?></p>
						</div>
					</div>
				</div>	
				<?php } ?>
			</div>
		</div>
	</div>
	<!-- end single post -->
	<?php get_template('inc/footer.html'); ?>
	<?php get_template('inc/endhtml.html'); ?>