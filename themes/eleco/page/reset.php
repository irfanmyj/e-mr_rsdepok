<?php
	$this->site->get_template('inc/header.html');
	$this->site->get_template('inc/navbar-publik.html');
	
?>
<!-- sign in -->
	<div class="sign-in segments-page">
		<div class="container">
			<div class="content b-shadow" id="reset">
					<div class="form-group">
						<input type="hidden" name="code" value="<?php echo $code;?>">
						<input type="hidden" name="kd_dokter" value="<?php echo $kd_dokter;?>">
						<input type="text" name="password" placeholder="Masukan password baru anda." class="form-control">
					</div>
					<button class="btn btn-primary active" id="simpan">Simpan</button>
			</div>
		</div>
	</div>

	<div class="content" style="display: none;" id="modalUmum">
		<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModal2">
		    <div class="modal-dialog" role="document">
		        <div class="modal-content">
		            <div class="modal-header">
		                <h5 class="modal-title">Informasi</h5>
		            </div>
		            <div class="modal-body" id="info">
		            	
		            </div>
		            <div class="modal-footer">
		                <a href="<?php echo base_url('auth/login');?>" class="btn btn-secondary">Kemabli Login</button>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
<?php 
	$this->site->get_template('inc/footer.html');
?>
<script type="text/javascript">
	var link = "<?php echo base_url('auth');?>";
	$(document).ready(function(){
		$('#simpan').click(function(){
			var kd_dokter = $('[name="kd_dokter"]').val();
			var code = $('[name="code"]').val();
			var password = $('[name="password"]').val();
			if(password)
			{
				$.ajax({
					type : 'post',
					url : link+'/updatepassword',
					data : 'kd_dokter='+kd_dokter+'&code='+code+'&password='+password,
					success : function(res)
					{
						var dt = JSON.parse(res);
						$('#info').html(dt.msg);
						$('#modalUmum').css('display','block');
						$('#exampleModal2').modal();
					}
				});
			}
		});
	});
</script>