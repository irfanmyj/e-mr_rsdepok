<?php get_template('inc/navbar-page.html'); ?>
<!-- Booking Umum -->
<div class="form-element segments-page">
	<div class="container">
		<div class="content no-mb">
			<div class="form-group">
				<h5>Nama Lengkap</h5>
				<input type="text" class="form-control" value="<?php echo $this->session->userdata('nm_lengkap');?>" name="nm_lengkap" id="no_kartu" readonly="">
			</div>
		</div>

		<div class="content no-mb">
			<div class="form-group">
				<h5>No Rekam Medis</h5>
				<input type="text" class="form-control" value="<?php echo $this->session->userdata('no_rkm_medis');?>" name="no_rkm_medis" id="no_rkm_medis" readonly="">
			</div>
		</div>

		<div class="content no-mb">
			<div class="form-group">
				<h5>Tanggal Kunjungan</h5>
				<input type="date" class="form-control" name="tgl_registrasi" id="tgl_registrasi">
			</div>
		</div>

		<div class="content no-mb">
			<div class="text">
				<p>Informasi : Jika pasien sudah terdaftar kemudian tidak bisa hadir, mohon untuk segera membatalakan pendaftaranya melalui menu pembatan booking.</p>
			</div>
		</div>

		<div class="content no-mb">
			<button type="submit" class="btn btn-primary" id="tombol1"><i class="fa fa-gear"></i> Proses</button>
		</div>
	</div>
</div>
<div class="content" style="display: none;" id="modalUmum">
	<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModal2">
	    <div class="modal-dialog" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title">Informasi Gagal</h5>
	                <button class="close" data-dismiss="modal" aria-label="close">
	                    <span aria-hidden="true"><i class="fa fa-close"></i></span>
	                </button>
	            </div>
	            <div class="modal-body" id="msg"></div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
	            </div>
	        </div>
	    </div>
	</div>
</div>
<!-- end Booking Umum -->
<?php get_template('inc/footer.html'); ?>
<script type="text/javascript">
	$(document).ready(function(){
		var link = '<?php echo base_url('Get_ajax');?>';
		$('#tombol1').click(function(){
			var tgl_registrasi = $('#tgl_registrasi').val();
			$.ajax({
				type : 'post',
				url : link+'/umum_step1',
				data : 'tgl_registrasi='+tgl_registrasi,
				success : function(res)
				{
					var JsDt = JSON.parse(res);
					if(JsDt.msg == '')
					{
						window.location = '<?php echo base_url('booking/umum_step2'); ?>';
					}
					else
					{
						$('#modalUmum').css('display','block');
						$('#msg').html(JsDt.msg);
						$('#exampleModal2').modal();
					}
				}
			});
		});

		$('#tgl_registrasi').change(function(){
			var tgl_registrasi = $(this).val();
			$.ajax({
				type : 'post',
				url : link+'/getLiburNasional',
				data : 'tgl_registrasi='+tgl_registrasi,
				success : function(res){
					var JsLn = JSON.parse(res);
					if(JsLn.msg!='')
					{
						$('#modalUmum').css('display','block');
						$('#msg').html(JsLn.msg);
						$('#exampleModal2').modal();
					}
				}
			});
		});
	});
</script>
<?php get_template('inc/endhtml.html'); ?>