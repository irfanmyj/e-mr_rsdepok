<?php get_template('inc/navbar-page.html'); ?>
<div class="open-hours segments-page">
	<div class="container">
		<div class="content b-shadow">
			<div class="title">
				<h5><?php echo strtoupper($title); ?> | <?php echo strtoupper(tanggal_indo(date('Y-m-d')));?></h5>
			</div>
			<div class="content no-mb" style="padding: 5px;">
				<div class="form-group">
					<input type="text" id="myInput" onkeyup="search()" placeholder="Cari nama dokter,Poliklinik" class="form-control">
				</div>
			</div>
		</div>
		<table class="table table-striped" id="myTable">
			<thead>
				<tr class="header">
					<th>Nama Dokter</th>
					<th>Poliklinik</th>
					<th>Jam Mulai</th>
					<th>Jam Selesai</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($r as $k => $v) { ?>
				<tr>
					<td><?php echo $v->nm_dokter; ?></td>
					<td><?php echo $v->nm_poli; ?></td>
					<td><?php echo date('H:i',strtotime($v->jam_mulai)); ?></td>
					<td><?php echo date('H:i',strtotime($v->jam_selesai)); ?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>
<?php get_template('inc/footer.html'); ?>
<script type="text/javascript">
	function search() {
	  // Declare variables 
	  var input, filter, table, tr, td, i, txtValue;
	  input = document.getElementById("myInput");
	  filter = input.value.toUpperCase();
	  table = document.getElementById("myTable");
	  tr = table.getElementsByTagName("tr");

	  // Loop through all table rows, and hide those who don't match the search query
	  for (i = 0; i < tr.length; i++) {
	    td = tr[i].getElementsByTagName("td")[0];
	    if (td) {
	      txtValue = td.textContent || td.innerText;
	      if (txtValue.toUpperCase().indexOf(filter) > -1) {
	        tr[i].style.display = "";
	      } else {
	        tr[i].style.display = "none";
	      }
	    } 
	  }
	}
</script>
<?php get_template('inc/endhtml.html'); ?>