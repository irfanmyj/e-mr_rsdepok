<?php get_template('inc/navbar-page.html'); ?>
<div class="table-page segments-page">
		<div class="container">
			<div class="wrap-title">
				<h5><?php echo strtoupper($title); ?></h5>
			</div>
			<div class="wrap-content b-shadow">
				<table class="table table-striped table-responsive display" style="width:100%" id="example">
					<thead>
						<tr>
							<th>Nomor Antrian</th>
							<th>Waktu Datang</th>
							<th>No Rm</th>
							<th>Poliklinik</th>
							<th>Dokter</th>
							<th>Cara Bayar</th>
							<th>Tanggal Periksa</th>
							<th>Status</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php 
						foreach ($r as $k => $v) { 
						?>
						<tr style="cursor:pointer;" <?php echo showValRec($v); ?>>
							<td class="tr_mod"><?php echo $v->no_reg; ?></td>
							<td class="tr_mod"><?php echo $v->waktu_kunjungan; ?></td>
							<td class="tr_mod"><?php echo $v->no_rkm_medis; ?></td>
							<td class="tr_mod"><?php echo $v->nm_poli; ?></td>
							<td class="tr_mod"><?php echo $v->nm_dokter; ?></td>
							<td class="tr_mod"><?php echo $v->png_jawab; ?></td>
							<td class="tr_mod"><?php echo $v->tanggal_periksa; ?></td>
							<td class="tr_mod"><?php echo $v->status; ?></td>
							<td class="tr_mod"><button class="btn btn-secondary tr_mod">Ganti Poli</button></td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
</div>
<div class="content" style="display: none;" id="modalUmum">
	<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModal2">
	    <div class="modal-dialog" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title">Batal Booking</h5>
	                <button class="close" data-dismiss="modal" aria-label="close">
	                    <span aria-hidden="true"><i class="fa fa-close"></i></span>
	                </button>
	            </div>
	            <div class="modal-body">
	            	<div class="content">
	            		<table class="table table-responsive table-striped">
	            			<tr>
	            				<td>Poli Awal</td>
	            				<td width="10">:</td>
	            				<td id="poli_awal"></td>
	            			</tr>
	            			<tr>
	            				<td>Dokter Awal</td>
	            				<td width="10">:</td>
	            				<td id="dokter_awal"></td>
	            			</tr>
	            		</table>
	            	</div>

	            	<div class="content no-mb">
						<div class="form-group">
							<h5>Poliklinik Tujuan</h5>
							<?php
			                echo htmlSelectFromArray($poli, 'name="kd_poli" id="kd_poli" style="width:100%;" class="form-control search2"', true);
			                ?>
						</div>
					</div>

					<div class="content no-mb">
						<div class="form-group">
							<h5>Dokter Tujuan</h5>
							<div id="id_dokter">
							<?php
			                echo htmlSelectFromArray($dokter, 'name="kd_dokter" id="kd_dokter" style="width:100%;" class="form-control"', true);
			                ?>
							</div>
						</div>
					</div>

					<div class="content no-mb">
						<div class="form-group">
							<div class="text">
								<h3 id="info_kuota"></h3>
							</div>
						</div>
					</div>

					<div class="content no-mb">
						<button type="submit" class="btn btn-primary" id="tombol2"><i class="fa fa-gear"></i> Update</button>
						<a href="<?php echo base_url('booking/gantipoli'); ?>" class="btn btn-primary" style="display: none;" id="kembali"><i class="fa fa-gear"></i> Kembali</a>
					</div>	
	            </div>
	            <div class="modal-footer">
	                <button type="button" id="tutups" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
	            </div>
	        </div>
	    </div>
	</div>
</div>

<div class="content" style="display: none;" id="modalUmum2">
	<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModal3">
	    <div class="modal-dialog" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title">Perubahan Booking</h5>
	                <button class="close" data-dismiss="modal" aria-label="close">
	                    <span aria-hidden="true"><i class="fa fa-close"></i></span>
	                </button>
	            </div>
	            <div class="modal-body">
	            	<div id="perubahan_booking">
	            		
	            	</div>
	            </div>
	            <div class="modal-footer">
	                <a href="<?php echo base_url('booking/gantipoli');?>" class="btn btn-secondary">Tutup</a>
	            </div>
	        </div>
	    </div>
	</div>
</div>
<?php get_template('inc/footer.html'); ?>
<script type="text/javascript">
	var link = '<?php echo base_url('Get_ajax');?>'
	$(document).ready(function() {
	    $('#example').DataTable();

	    $('.tr_mod').click(function(){
	    	var tr = $(this).parent();
	    	var poli_awal = tr.data('kd_poli');
	    	var dokter_awal = tr.data('kd_dokter');

	    	$('#poli_awal').html(tr.data('nm_poli'));
	    	$('#dokter_awal').html(tr.data('nm_dokter'));
	    	$('#modalUmum').css('display','block');
	    	$('#exampleModal2').modal();

	    	$('#kd_poli').change(function(){
				var kd_poli = $(this).val();
				$.ajax({
					type : 'post',
					url : link+'/getDokter',
					data : 'kd_poli='+kd_poli,
					success: function(res)
					{
						$('#id_dokter').html(res);

						$('#kd_dokter').change(function(){
						var kd_dokter = $(this).val();
						var kd_poli = $('#kd_poli').val();

						if(kd_dokter!='')
						{
							$.ajax({
								type : 'post',
								url : link+'/umum_step2',
								data : 'kd_dokter='+kd_dokter+'&kd_poli='+kd_poli+'&tgl_registrasi='+tr.data('tanggal_periksa'),
								success : function(res){
									var JsDt = JSON.parse(res);
									if(JsDt.sts==1)
									{
										$('#info_kuota').css('display','none');
										$('#tombol2').css('display','none');
										$('#kembali').css('display','block');
										alert(JsDt.msg);
									}
									else
									{
										$('#tombol2').css('display','block');
										$('#kembali').css('display','none');
										$('#info_kuota').css('display','block');
										$('#info_kuota').html(JsDt.msg);
									}
								}
							});
						}
						else
						{
							alert('Silahkan pilih nama dokter yang ingin anda tuju.');
						}
					});
					}
				});
			});

			$('#tombol2').click(function(){
				var kd_dokter = $('#kd_dokter').val();
				var kd_poli = $('#kd_poli').val();
				if(kd_poli=='' && kd_dokter=='')
				{
					alert('Silahkan poli Poliklinik dan Dokter');
				}
				else
				{
					$.ajax({
						type : 'post',
						url : link+'/gantipoli2',
						data : 'kd_poli='+kd_poli+'&kd_dokter='+kd_dokter+'&tgl_registrasi='+tr.data('tanggal_periksa')+'&poli_awal='+poli_awal+'&dokter_awal='+dokter_awal,
						success : function(res)
						{
							var JsDt2 = JSON.parse(res);
							if(JsDt2.sts == 1)
							{
								$('#modalUmum').css('display','none');
								$('#modalUmum2').css('display','block');
								$('#perubahan_booking').html(JsDt2.msg);
								$('#exampleModal3').modal();

							}else{
								$('#modalUmum').css('display','block');
								$('#kembali').css('display','block');
								$('#tutups').css('display','none');
								alert(JsDt2.msg);
							}
						}
					});
				}
			});
	    });
	} );
</script>
<?php get_template('inc/endhtml.html'); ?>