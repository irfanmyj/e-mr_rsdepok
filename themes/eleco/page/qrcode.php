<?php get_template('inc/navbar-page.html'); ?>
	<!-- qrcode -->
	<div class="container">
		<div class="wrap-title">
				<h5><?php echo strtoupper($title); ?></h5>
			</div>
		<div class="row">
			<div class="col-12">
				<div class="col-4 px-1">&nbsp;</div>
				<div class="col-4 px-2"><div id="code" style="padding-top: 10px;"></div></div>
				<div class="col-4 px-3">&nbsp;</div>
			</div>
		</div>
	</div>
	<!-- end qrcode -->
<?php get_template('inc/footer.html'); ?>
 <script type="text/javascript" src="<?php echo base_url('vendor/qrcode/qrcode.min.js');?>"></script>
    <script>
        const formName = '<?php echo $this->session->userdata('no_rkm_medis');?>';
        new QRCode('code', {
            text: JSON.stringify({
              name: formName
            }),
            colorDark : "#000000",
            colorLight : "#ffffff",
            correctLevel : QRCode.CorrectLevel.H
        });        
    </script>
<?php get_template('inc/endhtml.html'); ?>