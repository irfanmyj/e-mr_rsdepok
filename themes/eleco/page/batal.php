<?php get_template('inc/navbar-page.html'); ?>
<div class="table-page segments-page">
		<div class="container">
			<div class="wrap-title">
				<h5><?php echo strtoupper($title); ?></h5>
			</div>
			<div class="wrap-content b-shadow">
				<table class="table table-striped table-responsive display" style="width:100%" id="example">
					<thead>
						<tr>
							<th>Nomor Antrian</th>
							<th>Waktu Datang</th>
							<th>No Rm</th>
							<th>Poliklinik</th>
							<th>Dokter</th>
							<th>Cara Bayar</th>
							<th>Tanggal Periksa</th>
							<th>Status</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php 
						foreach ($r as $k => $v) { 
						?>
						<tr style="cursor:pointer;" <?php echo showValRec($v); ?>>
							<td class="tr_mod"><?php echo $v->no_reg; ?></td>
							<td class="tr_mod"><?php echo $v->waktu_kunjungan; ?></td>
							<td class="tr_mod"><?php echo $v->no_rkm_medis; ?></td>
							<td class="tr_mod"><?php echo $v->nm_poli; ?></td>
							<td class="tr_mod"><?php echo $v->nm_dokter; ?></td>
							<td class="tr_mod"><?php echo $v->png_jawab; ?></td>
							<td class="tr_mod"><?php echo $v->tanggal_periksa; ?></td>
							<td class="tr_mod"><?php echo $v->status; ?></td>
							<td class="tr_mod"><button class="btn btn-secondary tr_mod">Batalkan</button></td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
</div>
<div class="content" style="display: none;" id="modalUmum">
	<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModal2">
	    <div class="modal-dialog" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title">Batal Booking</h5>
	                <button class="close" data-dismiss="modal" aria-label="close">
	                    <span aria-hidden="true"><i class="fa fa-close"></i></span>
	                </button>
	            </div>
	            <div class="modal-body">
	            	<p id="msg"></p>		
	            </div>
	            <div class="modal-footer">
	                <button type="button" id="tutups" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
	                <a href="<?php echo base_url('home/view');?>" type="button" id="sukses" class="btn btn-secondary" style="display: none;">Kembali</a>
	            </div>
	        </div>
	    </div>
	</div>
</div>
<?php get_template('inc/footer.html'); ?>
<script type="text/javascript">
	var link = '<?php echo base_url('Get_ajax');?>'
	$(document).ready(function() {
	    $('#example').DataTable();

	    $('.tr_mod').click(function(){
	    	var tr = $(this).parent();
	    	$.ajax({
	    		type : 'post',
	    		url : link+'/batal',
	    		data : 'kd_poli='+tr.data('kd_poli')+'&kd_dokter='+tr.data('kd_dokter')+'&tanggal_periksa='+tr.data('tanggal_periksa'),
	    		success : function(res)
	    		{
	    			var JsDt = JSON.parse(res);
	    			$('#msg').html(JsDt.msg);
	    			$('#modalUmum').css('display','block');
	    			$('#sukses').css('display','block');
	    			$('#tutups').css('display','none');
	    			$('#exampleModal2').modal();
	    		}
	    	});
	    	
	    });
	} );
</script>
<?php get_template('inc/endhtml.html'); ?>