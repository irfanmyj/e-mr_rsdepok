<?php get_template('inc/navbar-page.html'); ?>
<div class="open-hours segments-page">
	<div class="container">
		<div class="content b-shadow">
			<div class="title">
				<h5><?php echo strtoupper($title);?></h5>
			</div>
		</div>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Nama Dokter</th>
					<th>Poliklinik</th>
					<th>Tanggal Awal</th>
					<th>Tanggal Akhir</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($r as $k => $v) { ?>
				<tr>
					<td><?php echo $v->nm_dokter; ?></td>
					<td><?php echo $v->nm_poli; ?></td>
					<td><?php echo tanggal_indo($v->tgl_awal,true); ?></td>
					<td><?php echo tanggal_indo($v->tgl_akhir,true); ?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>
<?php get_template('inc/footer.html'); ?>
<?php get_template('inc/endhtml.html'); ?>