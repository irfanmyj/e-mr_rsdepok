<?php get_template('inc/navbar-page.html'); ?>
<div class="table-page segments-page">
	<div class="container">
		<div class="wrap-title">
			<h5><?php echo strtoupper($title); ?> | <?php echo strtoupper(tanggal_indo(date('Y-m-d')));?></h5>
		</div>

		<div class="wrap-content b-shadow">
			<div class="content no-mb" id="spn1" style="display: none;">
				<div class="form-group">
					<button class="btn btn-primary" type="button" disabled>
					  <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
					  Silahkan tunggu, data sedang di proses...
					</button>
				</div>
			</div>
			<div class="content no-mb">
				<div class="form-group">
					<h5>Tulis Nama Daerah Faskes Yang Ingin Dicari</h5>
					<br>
					<input type="text" class="form-control" name="nm_daerah" id="nm_daerah">
				</div>
			</div>
			<div class="content no-mb">
				<h5>Pilih Kategori Faskes</h5>
				<br>
				<select class="custom-select" name="nm_faskes" id="nm_faskes">
					<option selected>Pilih Faskes</option>
					<option value="1">Faskes 1 (Puskesmas)</option>
					<option value="2">Faskes 2 (RS)</option>
				</select>
			</div>
			<div class="content no-mb" style="margin: 10px 0 0 0;">
				<button type="submit" class="btn btn-primary" id="proses"><i class="fa fa-gear"></i> Proses</button>
			</div>
		</div>
	</div>

	<div class="container" id="showFaskes"></div>
</div>
<?php get_template('inc/footer.html'); ?>
<script type="text/javascript">
	$(document).ready(function(){
		var link = '<?php echo base_url('infopublik');?>';
		$('#proses').click(function(){
			$('#spn1').css('display','block');
			var nm_daerah = $('#nm_daerah').val();
			var nm_faskes = $('#nm_faskes').val();
			if(nm_daerah != '' && nm_faskes!='')
			{
				$.ajax({
					type	: 'post',
					url 	: link+'/getfaskes',
					data 	: 'nm_daerah='+nm_daerah+'&nm_faskes='+nm_faskes,
					success	: function(res){
						$('#spn1').css('display','none');
						$('#showFaskes').show(1000);
						$('#showFaskes').html(res);
					}
				});
			}
			else
			{
				$('#spn1').css('display','none');
				alert('Mohon maaf form yang anda kirim kosong.');
			}
		});
	});
</script>
<?php get_template('inc/endhtml.html'); ?>