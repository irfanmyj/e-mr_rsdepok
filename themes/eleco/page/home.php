<?php  
	$this->site->get_template('inc/navbar.html');
	$this->site->get_template('inc/sidebar-header.html');
	$this->site->get_template('inc/slide.html');
	$this->site->get_template('inc/filter-home.html');
	$this->site->get_template('inc/features.html');
	$this->site->get_template('inc/footer.html');
?>
<div class="content" style="display: none;" id="modalUmum">
	<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModal2">
	    <div class="modal-dialog" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title">Informasi</h5>
	            </div>
	            <div class="modal-body" id="msg">
	            	<h1>Segera akan hadir...</h1>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
	            </div>
	        </div>
	    </div>
	</div>
</div>
<script src="https://code.highcharts.com/highcharts.js"></script>
<!-- <script src="https://code.highcharts.com/modules/timeline.js"></script> -->
<script type="text/javascript">
	$(document).ready(function(){
		$('#artikel').click(function(){
			$('#modalUmum').css('display','block');
			$('#exampleModal2').modal();
		});
	});
</script>
<?php
	$this->site->get_template('inc/endhtml.html');
?>
