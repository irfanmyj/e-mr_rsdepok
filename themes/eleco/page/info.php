<?php get_template('inc/navbar-page.html'); ?>
<!-- single post -->
	<div class="segments-page">
	<?php if(!empty($r)){ ?>
		<?php foreach ($r as $k => $v) { ?>
		<div class="container">
			<div class="single-post">
				<!-- <img src="images/post1.jpg" alt=""> -->
				<a href=""><h5><?php echo ($v->judul) ? $v->judul : ''; ?></h5></a>
				<p class="date"><?php echo ($v->masuk_tgl) ? '<i class="fa fa-clock-o"></i>'. tanggal_indo($v->masuk_tgl,true) : ''; ?></p>
				<?php echo ($v->keterangan) ? $v->keterangan : '';?>				
			</div>
			<?php if($v->judul) { ?>
			<div class="author">
				<div class="content-text">
					<h5>Admin RSUD KOTA DEPOK></h5>
				</div>
			</div>
			<?php }else{ echo ''; } ?>
		</div>
		<?php } ?>
	<?php }else{ echo 'Data tidak tersedia.'; } ?>
	</div>
	<!-- end single post -->
<?php get_template('inc/footer.html'); ?>

<?php get_template('inc/endhtml.html'); ?>