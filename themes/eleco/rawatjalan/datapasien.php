<div class="card">
	<div class="card-header panel-heading no-border bg-primary" style="padding: 5px; background: blue;"><h5 class="text-white"><?php echo ($dtpasien[0]->jk == 'L') ? '<i class="fa fa-male text-dark"></i>' : '<i class="fa fa-female text-dark"></i>'; ?> <?php echo $dtpasien[0]->nm_pasien; ?></h5>
	</div>
	<div class="col-md-12" style="padding: 5px 0;">
		<table class="table-responsive-md" style="padding: 0px; margin: 0px;">
			<tr style="padding: 0px; margin: 0px;">
				<td>&nbsp; - Nomor Antrian</td>
				<td>:</td>
				<td><b><?php echo $dtpasien[0]->no_reg; ?></b></td>
				<td>&nbsp; -</td>
				<td>Poliklinik</td>
				<td>:</td>
				<td><b><?php echo $dtpasien[0]->nm_poli; ?></b></td>
			</tr>
			<tr>
				<td>&nbsp; - No Rekamedis</td>
				<td>:</td>
				<td><b><?php echo $dtpasien[0]->no_rkm_medis; ?></b></td>
				<td>&nbsp; -</td>
				<td>Cara Bayar</td>
				<td>:</td>
				<td><b><?php echo $dtpasien[0]->png_jawab; ?></b></td>
			</tr>
			<tr>
				<td>&nbsp; - Nama Pasien</td>
				<td>:</td>
				<td><b><?php echo $dtpasien[0]->nm_pasien; ?></b></td>
				<td>&nbsp; -</td>
				<td>No Rawat</td>
				<td>:</td>
				<td><b><?php echo $noRawat; ?></b></td>
			</tr>
			<tr>
				<td>&nbsp; - Tanggal Lahir</td>
				<td>:</td>
				<td><?php echo '<b>'. date('d M Y', strtotime($dtpasien[0]->tgl_lahir)) .'</b>';?> | Umur (<b><?php echo $usia['year'] . ' Tahun, ' . $usia['month'] .' Bulan, '. $usia['day'] .' Hari'; ?></b>)</td>
				<td>&nbsp; -</td>
				<td>Bahasa Pasien</td>
				<td>:</td>
				<td><b><?php echo $dtpasien[0]->nama_bahasa; ?></b></td>
			</tr>
			<tr>
				<td>&nbsp; - Jenis Kelamin</td>
				<td>:</td>
				<td><b><?php echo ($dtpasien[0]->jk == 'L') ? '<h6><i class="fa fa-male text-dark"></i>  Laki-laki</h6>' : '<h6><i class="fa fa-female text-dark"></i> Perempuan</h6>'; ?></b></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td colspan="2">&nbsp;
					<?php  
					if(@$pemeriksaan[0]->alergi=='-' OR @$pemeriksaan[0]->alergi=='')
					{
						echo '';
					}
					else
					{
						echo '<span class="border border-danger bg-danger text-white p-1" id="ralert">Alergi</span>';
					}
					?>
				</td>
			</tr>
		</table>
	</div>
</div>