<div class="border bg-light p-2" id="soap">
	<button type="button" class="btn btn-sm btn-primary" id="rect">Start</button>
	<button type="button" class="btn btn-sm btn-danger" id="rectstop">Stop</button>
	<div class="form-group mt-1">
		<textarea class="form-control" id="voicetotexts" style="display: none;"></textarea>
	</div>

	<form method="post" id="formsimpan_pemeriksaan" action="#">					
	<div class="form-group">
		<label>S (subjektif) :  Data subjektif Berisi data dari pasien melalui anamnesis (wawancara) yang merupakan ungkapan langsung.</label>
		<textarea class="form-control" id="subjektif" name="s"><?php echo isset($soap['s']) ? substr($soap['s'],2) : ''; ?></textarea>
	</div>

	<div class="form-group">
		<label>O (objektif) :  Data objektif Data yang dari hasil observasi melalui pemeriksaan fisik.</label>
		<textarea class="form-control" id="objective" name="o"><?php echo isset($soap['o']) ? substr($soap['o'],2) : ''; ?></textarea>
	</div>

	<div class="form-group">
		<label>A (assesment) : Analisis dan interpretasi Berdasarkan data yang terkumpul kemudian dibuat kesimpulan yang meliputi diagnosis, antisipasi diagnosis atau masalah potensial, serta perlu tidaknya dilakukan tindakan segera.</label>
		<textarea class="form-control" id="pengkajian" name="a"><?php echo isset($soap['a']) ? substr($soap['a'],2) : ''; ?></textarea>
	</div>

	<div class="form-group">
		<label>P (plan) : Perencanaan Merupakan rencana dari tindakan yang akan diberikan termasuk asuhan mandiri, kolaborasi, diagnosis atau labolatorium, serta konseling untuk tindak lanjut.</label>
		<textarea class="form-control" id="rencana" name="p"><?php echo isset($soap['p']) ? substr($soap['p'],2) : ''; ?></textarea>
	</div>

	<div class="form-group">
		<input type="text" style="border: 0px;width: 100px;box-shadow: none;background: none;-webkit-box-shadow:none;-moz-box-shadow:none;border-radius:none;-webkit-border-radius:none;-moz-border-radius:none;text-align: left;padding-right: 20px;" readonly="true" name="timerDurasiSoap" id="timerDurasiSoap" value="00:00:00"/>
	</div>
	
	<button type="button" id="simpanpemeriksaan" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Simpan</button>
	</form>
</div>