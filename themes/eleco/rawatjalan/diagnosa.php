<div class="border bg-light" id="fdp">
	<form method="post" id="formsimpan_diagnosa" action="#">
	
	
	<div class="form-group mt-1 p-2">
		<button type="button" class="btn btn-sm btn-primary" id="rectdiag">Start</button>
		<button type="button" class="btn btn-sm btn-danger" id="rectstopdiag">Stop</button>
		<input type="text" id="voicetotextsdiag" class="form-control mt-2" style="display: none;">
	</div>

	<div class="form-group" id="recordsdiagnosa">
			
	</div>

	<div class="form-group m-2">
		<label><h5>Cari Data Diagnosis</h5></label>
		<input type="text" id="getdiagnosa" class="form-control" style="border-radius:0px;" placeholder="Pencarian minimal 3 karakter" />
	</div>	
	
	<table class="table table-striped table-responsive-md" id="list-datadignosa"><tbody></tbody></table>

	<div class="form-group m-2">
		<input type="text" style="border: 0px;width: 100px;box-shadow: none;background: none;-webkit-box-shadow:none;-moz-box-shadow:none;border-radius:none;-webkit-border-radius:none;-moz-border-radius:none;text-align: left;padding-right: 20px;" readonly="true" name="timerDurasiDiagnosa" id="timerDurasiDiagnosa" value="00:00:00"/>
	</div>
	<button type="button" id="simpandiagnosa" class="btn btn-primary btn-sm m-2"><i class="fa fa-save"></i> Simpan</button>
	</form>
</div>