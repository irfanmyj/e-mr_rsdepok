
<script type="text/javascript">
	var link = '<?php echo base_url($this->uri->segment(1));?>';
	var NoRawat = '<?php echo $noRawat;?>';
	var noRawatTerakhir = '<?php echo $noRawatTerakhir;?>';
	var NoRM = '<?php echo $no_rkm_medis;?>';
	var TglPerawatan = '<?php echo date('Y-m-d');?>';
	var status_poli = '<?php echo $dtpasien[0]->status_poli;?>';
	var kd_poli = '<?php echo $dtpasien[0]->kd_poli;?>';
	var kd_dokter = '<?php echo $this->session->userdata('id_user');?>';
	var kd_pj = '<?php echo $dtpasien[0]->kd_pj; ?>';
	var SI;
	
	$(document).ready(function(){
		$('.search2').select2();
		$('.search_soap').select2();
		$('.search_diagnosa').select2();
		$('.search_obat').select2();
		$('.search_laboratorium').select2();
		$('.search_radiologi').select2();
		tgl_kunjungan(noRawatTerakhir,link);
				
		/*
		START AREA TIMER FORMULIR
		*/
		$('#perawat_tabs-tab').click(function(){
			reset_timer();
	        clearInterval(SI);
	   	});

		$('#soap-tab').click(function(){
			reset_timer();
	        clearInterval(SI);
	        voicesoap();
	        SI = setInterval("startTimer($('#timerDurasiSoap'))",1000);	        
	        $.ajax({
	    		type : 'post',
	    		url : link+'/history_soap_last',
	    		data : 'no_rawat='+noRawatTerakhir,
	    		success : function(res)
	    		{
	    			$('#history_soap_last').html(res);
	    		}
	    	});
	   	});

		$('#diagnosa-tab').click(function(){
			reset_timer();
	        clearInterval(SI);
	        SI = setInterval("startTimer($('#timerDurasiDiagnosa'))",1000);
	        $.ajax({
	    		type : 'post',
	    		url : link+'/history_diagnosa_last',
	    		data : 'no_rawat='+noRawatTerakhir,
	    		success : function(res)
	    		{
	    			$('#history_diagnosa_last').html(res);
	    			$('#list-datadignosa-riwayat .pilihdiagnosa').click(function(){
			        	var id = $(this).data('id');
			        	var keys = $(this).data('keys');
			        	var nama = $(this).data('nama');
			        	$("#list-datadignosa").append('\n\
	                    <tr id="'+keys+'">\n\
	                        <td class="tr_mod"><input type="hidden" name="kd_penyakit[]" value='+id+'><h6>'+nama+'</h6></td>\n\
	                        <td style="cursor:pointer;" class="delete" data-id="'+keys+'"><h6><i class="fa fa-delete"> Hapus</i></h6></td>\n\
	                    </tr>');
	                    //$('#list-datadignosa #'+id).remove();
			        });
	    		}
	    	});
	   	});

	   	$('#resepobat-tab').click(function(){
	   		reset_timer();
	        clearInterval(SI);
	        SI = setInterval("startTimer($('#timerDurasiObat'))",1000);

	        $.ajax({
	    		type : 'post',
	    		url : link+'/history_obat_last',
	    		data : 'no_rawat='+noRawatTerakhir,
	    		success : function(res)
	    		{
	    			$('#history_obat_last').html(res);
	    			// Delete
	    			$('.delcr').click(function(){
	    				$(".tblInput2 tbody").find('input[name="records"]').each(function(){
			            	if($(this).is(":checked")){
			            		if($(this).is(":checked")){
				            		$(this).parents("tr").remove();
				                }
			                }
			            });
	    			});	

	    			// Simpan
	    			$('.copyr').click(function(){
	    				var jlm_obat = $('[name="jlm_obat[]"]').serialize();
			        	var kode_obat = $('[name="kode_obat[]"]').serialize();
			        	var aturan_pakai = $('[name="aturan_pakai[]"]').serialize();
			        	var keterangan = $('[name="keterangan[]"]').serialize();
			        	var t = $('[name="timerDurasiObat"]').val();

			        	$("#tb-resepobatcopy tr").remove();
			        	if(kode_obat!='' && jlm_obat!='' && aturan_pakai!='')
				    	{
				    		$.ajax({
				        		type : 'post',
				        		url : link+'/saveResepObat1',
				        		data : jlm_obat+'&'+kode_obat+'&tgl_perawatan='+TglPerawatan+'&no_rawat='+NoRawat+'&'+keterangan+'&'+aturan_pakai+'&kd_dokter='+kd_dokter+'&kd_poli='+kd_poli+'&t='+t,
				        		success : function(res)
				        		{
				        			var r = JSON.parse(res);
				        			if(r.error=='')
				        			{
				        				showmodal('Data berhasil disimpan.');
				        				$('.showresepobat').html(r.view);
				        				$('.delcr').css('display','none');
				        				$('.copyr').css('display','none');
				        				delete_resepobat();
				        				reset_timer();
				        			}
				        			else
				        			{
				        				showmodal('Data gagal disimpan.');
				        				$('.showresepobat').html(r.view);
				        			}
				        		}
				        	});
				    	}	   
	    			});	
	    		}
	    	});
	   	});
		
		$('#laboratorium-tab').click(function(){
	   		reset_timer();
	        clearInterval(SI);
	        SI = setInterval("startTimer($('#timerDurasiLab'))",1000);
	        $.ajax({
	    		type : 'post',
	    		url : link+'/history_laboratorium_last',
	    		data : 'no_rawat='+noRawatTerakhir,
	    		success : function(res)
	    		{
	    			$('#history_laboratorium_last').html(res);
	    			$('#list-datalaboratorium-riwayat .pilihlaboratorium').click(function(){

			        	var id = $(this).data('id');
			        	var keys = $(this).data('keys');
			        	var nama = $(this).data('nama');
			        	$("#labs").append('\n\
	                    <tr id="'+keys+'">\n\
	                        <td class="tr_mod"><input type="hidden" name="kd_jenis_prw_labs[]" value='+id+'><h6>'+nama+'</h6></td>\n\
	                        <td style="cursor:pointer;" class="delete" data-id="'+keys+'"><h6><i class="fa fa-delete"> Hapus</i></h6></td>\n\
	                    </tr>');
	                    //$('#list-datadignosa #'+id).remove();
			        });
	    		}
	    	});
	   	});

	   	$('#radiologi_tabs-tab').click(function(){
	   		reset_timer();
	        clearInterval(SI);
	        SI = setInterval("startTimer($('#timerDurasiRad'))",1000);

	        $.ajax({
	    		type : 'post',
	    		url : link+'/history_radiologi_last',
	    		data : 'no_rawat='+noRawatTerakhir,
	    		success : function(res)
	    		{
	    			$('#history_radiologi_last').html(res);
	    			$('#list-dataradiologi-riwayat .pilihradiologi').click(function(){

			        	var id = $(this).data('id');
			        	var keys = $(this).data('keys');
			        	var nama = $(this).data('nama');
			        	$("#rads").append('\n\
	                    <tr id="'+keys+'">\n\
	                        <td class="tr_mod"><input type="hidden" name="kd_jenis_prw_rad[]" value="'+id+'"><h6>'+nama+'</h6></td>\n\
	                        <td style="cursor:pointer;" class="delete" data-id="'+keys+'"><h6><i class="fa fa-delete"> Hapus</i></h6></td>\n\
	                    </tr>');
	                    //$('#list-datadignosa #'+id).remove();
			        });
	    		}
	    	});
	   	});

	   	$('#tindakan_tabs-tab').click(function(){
	   		reset_timer();
	        clearInterval(SI);
	        SI = setInterval("startTimer($('#timerDurasiTindakan'))",1000);

	        var no_rawat_soap_change = $('#no_rawat_tindakan').val();
			$.ajax({
	    		type : 'post',
	    		url : link+'/history_tindakan_last',
	    		data : 'no_rawat='+no_rawat_soap_change,
	    		success : function(res)
	    		{
	    			tgl_kunjungan(no_rawat_soap_change,link);
	    			$('#history_tindakan_last').html(res);
	    			$('#list-datatindakan-riwayat .pilihtindakan').click(function(){
			        	var kd_jenis_prw = $(this).data('kd_jenis_prw');
			        	var material = $(this).data('material');
			        	var bhp = $(this).data('bhp');
			        	var kso = $(this).data('kso');
			        	var menejemen = $(this).data('menejemen');
			        	var tarif_tindakanpr = $(this).data('tarif_tindakanpr');
			        	var tarif_tindakandr = $(this).data('tarif_tindakandr');
			        	var biaya_rawat = $(this).data('biaya_rawat');
			        	var nm_perawatan = $(this).data('nm_perawatan');
			        	var nm_kategori = $(this).data('nm_kategori');
			        	$("#tind").append('\n\
	                    <tr id="'+kd_jenis_prw+'">\n\
	                        <td class="tr_mod">\n\
                        <input type="hidden" name="kd_jenis_prw_tindakan[]" value='+kd_jenis_prw+'>\n\
                        <input type="hidden" name="material[]" value='+material+'>\n\
                        <input type="hidden" name="bhp[]" value='+bhp+'>\n\
                        <input type="hidden" name="tarif_tindakanpr[]" value='+tarif_tindakanpr+'>\n\
                        <input type="hidden" name="tarif_tindakandr[]" value='+tarif_tindakandr+'>\n\
                        <input type="hidden" name="kso[]" value='+kso+'>\n\
                        <input type="hidden" name="menejemen[]" value='+menejemen+'>\n\
                        <input type="hidden" name="biaya_rawat[]" value='+biaya_rawat+'>\n\
                        <h6>'+nm_perawatan+'</h6></td>\n\
	                        <td style="cursor:pointer;" class="delete" data-id="'+kd_jenis_prw+'"><h6><i class="fa fa-delete"> Hapus</i></h6></td>\n\
	                    </tr>');
	                    //$('#list-datadignosa #'+id).remove();
			        });
	    		}
	    	});
	   	});
	   	/*
		END AREA TIMER FORMULIR
		*/

		// Area awal untuk menampilkan history pasien
		$('#no_rawat').change(function(){
			var no_rawat 		= $(this).val();
			var no_rkm_medis 	= $('#no_rkm_medis').val();
			if(no_rawat)
			{
				$.ajax({
					type	: 'post',
					url 	: link+'/getHistory',
					data 	: 'no_rawat='+no_rawat+'&no_rkm_medis='+no_rkm_medis,
					success : function(res)
					{
						var data_view = JSON.parse(res);
						$('#nav-tabs1').html(data_view.poliklinik);
						//$('#nav-tabs2').html(data_view.rawatinap);
					}
				});
			}
			else
			{
				$('#nav-tabs1').html('<span class="text-center p-2"><p class="text-justify"><h5>Data tidak ditemukan.</h5></p></span>');
				$('#nav-tabs2').html('<span class="text-center p-2"><p class="text-justify"><h5>Data tidak ditemukan.</h5></p></span>');
			}
		});
		// area akhir

		// Area diagnosa 
		voicediagnosa();
		$("#getdiagnosa").autocomplete({
	        source: "<?php echo site_url($this->uri->segment(1).'/getDiagnosa');?>",
	        minLength: 3,
	        focus: function( event, ui ) {
	        	event.preventDefault();
	        	$(this).val(ui.item.label); //$('#voicetotextsdiag').val()
	        },
	        select: function(event, ui) {
	            //console.log(event);
	            $('#getDiagnosa').val();//.blur();
	            event.preventDefault(); // cancel default behavior which updates input field
	            
	            $("#list-datadignosa").append('\n\
	                <tr id="'+ui.item.keys+'">\n\
	                    <td class="tr_mod"><input type="hidden" name="kd_penyakit[]" value='+ui.item.id+'><h6>'+ui.item.value+'</h6></td>\n\
	                    <td style="cursor:pointer;" class="delete" data-id="'+ui.item.keys+'"><h6><i class="fa fa-delete"> Hapus</i></h6></td>\n\
	                </tr>');
	        }
	    });

        $('#formsimpan_diagnosa').on('click','#simpandiagnosa', function(){
        	var diagnosa = $('[name="kd_penyakit[]"]').serialize();
        	var t = $('[name="timerDurasiDiagnosa"]').val();

        	$("#list-datadignosa tr").remove();
        	if(diagnosa!='')
        	{
        		$.ajax({
	        		type : 'post',
	        		url : link+'/savediagnosa',
	        		data : 'kd_dokter='+kd_dokter+'&kd_poli='+kd_poli+'&'+diagnosa+'&no_rawat='+NoRawat+'&status_penyakit='+status_poli+'&t='+t,
	        		success : function(res)
	        		{
	        			var r = JSON.parse(res);
	        			if(r.error=='')
	        			{
	        				showmodal('Data berhasil disimpan.');
	        				$('.showdiagnosa').html(r.view);
	        				delete_diagnosa();
	        				sortTableDiag2();
	        				reset_timer();
	        			}
	        			else
	        			{
	        				showmodal('Data gagal disimpan.');
	        				$('.showdiagnosa').html(r.view);
	        			}
	        		}
	        	});
        	}
        	else
        	{
        		alert('Data tidak boleh kosong.');
        	}
        });

        $( ".tabledianosa1" ).sortable({
	        delay: 150,
	        stop: function() {
	            var selectedData = new Array();
	            $('.tabledianosa1>tr').each(function() {
	                selectedData.push($(this).attr("id"));
	            });
	            updateOrder(selectedData,NoRawat);
	        }
	    });

	    $('#list-datadignosa').on('click', '.delete', function(){
            var tr = $(this).data('id');
            $('#list-datadignosa #'+tr).remove();
        });

        $('#no_rawat_diagnosa').change(function(){
        	var no_rawat_soap_change = $('#no_rawat_diagnosa').val();
			$.ajax({
	    		type : 'post',
	    		url : link+'/history_diagnosa_last',
	    		data : 'no_rawat='+no_rawat_soap_change,
	    		success : function(res)
	    		{
	    			tgl_kunjungan(no_rawat_soap_change,link);
	    			$('#history_diagnosa_last').html(res);
	    			$('#list-datadignosa-riwayat .pilihdiagnosa').click(function(){
			        	var id = $(this).data('id');
			        	var keys = $(this).data('keys');
			        	var nama = $(this).data('nama');
			        	$("#list-datadignosa").append('\n\
	                    <tr id="'+keys+'">\n\
	                        <td class="tr_mod"><input type="hidden" name="kd_penyakit[]" value='+id+'><h6>'+nama+'</h6></td>\n\
	                        <td style="cursor:pointer;" class="delete" data-id="'+keys+'"><h6><i class="fa fa-delete"> Hapus</i></h6></td>\n\
	                    </tr>');
	                    //$('#list-datadignosa #'+id).remove();
			        });
	    		}
	    	});
        });

        $(function(){
		    $( "#list-datadignosa" ).sortable();
		    $( "#list-datadignosa" ).disableSelection();
	  	});

	  	sortTableDiag2();
	  	delete_diagnosa();
		// Area akhir diagnosa

		// Area awal module laboratorium
        //voicelaboratorium();
        $("#laboratorium_getdata").autocomplete({
            source: "<?php echo site_url($this->uri->segment(1).'/getLaboratorium');?>",
            minLength: 3,
            focus: function( event, ui ) {
            	event.preventDefault();
            	$(this).val(ui.item.label);
            },
            select: function(event, ui) {
                //console.log(event);
                $(this).val('');//.blur();
                event.preventDefault(); // cancel default behavior which updates input field
                
                $("#labs").append('\n\
                    <tr id="'+ui.item.keys+'">\n\
                        <td class="tr_mod"><input type="hidden" name="kd_jenis_prw_labs[]" value='+ui.item.id+'><h6>'+ui.item.value+'</h6></td>\n\
                        <td style="cursor:pointer;" class="delete" data-id="'+ui.item.keys+'"><h6><i class="fa fa-delete"> Hapus</i></h6></td>\n\
                    </tr>');
            }
        });

        $('#labs').on('click', '.delete', function(){
            var tr = $(this).data('id');
            $('#labs #'+tr).remove();
            //$('#labs tr #').remove();
        });

        $('#formsimpan_laboratorium').on('click','#simpanlabs', function(){
        	var labs = $('[name="kd_jenis_prw_labs[]"]').serialize();
        	var t = $('[name="timerDurasiLab"]').val(); 
        	$("#labs tr").remove();
        	if(labs!='')
        	{
        		$.ajax({
	        		type : 'post',
	        		url : link+'/savelabs',
	        		data : labs+'&no_rawat='+NoRawat+'&kd_dokter='+kd_dokter+'&tgl='+TglPerawatan+'&kd_poli='+kd_poli+'&t='+t,
	        		success : function(res)
	        		{
	        			//alert(res);
	        			var r = JSON.parse(res);
	        			if(r.error=='')
	        			{
	        				showmodal('Data berhasil disimpan.');
	        				$('.showlabs').html(r.view);
	        				delete_labs();
	        				reset_timer();
	        			}
	        			else
	        			{
	        				showmodal('Data gagal disimpan.');
	        				$('.showlabs').html(r.view);
	        			}
	        		}
	        	});
        	}
        	else
        	{
        		alert('Data tidak boleh kosong.');
        	}
        });

        $('#no_rawat_laboratorium').change(function(){
        	var no_rawat_soap_change = $('#no_rawat_laboratorium').val();
			$.ajax({
	    		type : 'post',
	    		url : link+'/history_laboratorium_last',
	    		data : 'no_rawat='+no_rawat_soap_change,
	    		success : function(res)
	    		{
	    			tgl_kunjungan(no_rawat_soap_change,link);
	    			$('#history_laboratorium_last').html(res);
	    			$('#list-datalaboratorium-riwayat .pilihlaboratorium').click(function(){
			        	var id = $(this).data('id');
			        	var keys = $(this).data('keys');
			        	var nama = $(this).data('nama');
			        	$("#labs").append('\n\
	                    <tr id="'+keys+'">\n\
	                        <td class="tr_mod"><input type="hidden" name="kd_jenis_prw_labs[]" value='+id+'><h6>'+nama+'</h6></td>\n\
	                        <td style="cursor:pointer;" class="delete" data-id="'+keys+'"><h6><i class="fa fa-delete"> Hapus</i></h6></td>\n\
	                    </tr>');
	                    //$('#list-datadignosa #'+id).remove();
			        });
	    		}
	    	});
        });

        delete_labs();
        // Area akhir module laboratorium

        // Area awal module radiologi
        $("#radiologi_getdata").autocomplete({
            source: "<?php echo site_url($this->uri->segment(1).'/getRadiologi');?>",
            minLength: 3,
            focus: function( event, ui ) {
            	event.preventDefault();
            	$(this).val(ui.item.label);
            },
            select: function(event, ui) {
                //console.log(event);
                $(this).val('');//.blur();
                event.preventDefault(); // cancel default behavior which updates input field
                
                $("#rads").append('\n\
                    <tr id="'+ui.item.keys+'">\n\
                        <td class="tr_mod"><input type="hidden" name="kd_jenis_prw_rad[]" value="'+ui.item.id+'"><h6>'+ui.item.value+'</h6></td>\n\
                        <td style="cursor:pointer;" class="delete" data-id="'+ui.item.keys+'"><h6><i class="fa fa-delete"> Hapus</i></h6></td>\n\
                    </tr>');
            }
        });

        $('#rads').on('click', '.delete', function(){
            var tr = $(this).data('id');
           $('#rads #'+tr).remove();
        });

        $('#formsimpan_radiologi').on('click','#simpanrads', function(){
        	var rads = $('[name="kd_jenis_prw_rad[]"]').serialize();
        	var t = $('[name="timerDurasiRad"]').val();
        	$("#rads tr").remove();
        	if(rads!='')
        	{
        		$.ajax({
	        		type : 'post',
	        		url : link+'/saverads',
	        		data : rads+'&no_rawat='+NoRawat+'&kd_dokter='+kd_dokter+'&tgl='+TglPerawatan+'&kd_poli='+kd_poli+'&t='+t,
	        		success : function(res)
	        		{
	        			//alert(res);
	        			var r = JSON.parse(res);
	        			if(r.error=='')
	        			{
	        				showmodal('Data berhasil disimpan.');
	        				$('.showrads').html(r.view);
	        				delete_rads();
	        				reset_timer();
	        			}
	        			else
	        			{
	        				showmodal('Data gagal disimpan.');
	        				$('.showrads').html(r.view);
	        			}
	        		}
	        	});
        	}
        	else
        	{
        		alert('Data tidak boleh kosong.');
        	}
        });

        $('#no_rawat_radiologi').change(function(){
        	var no_rawat_soap_change = $('#no_rawat_radiologi').val();
			$.ajax({
	    		type : 'post',
	    		url : link+'/history_radiologi_last',
	    		data : 'no_rawat='+no_rawat_soap_change,
	    		success : function(res)
	    		{
	    			tgl_kunjungan(no_rawat_soap_change,link);
	    			$('#history_radiologi_last').html(res);
	    			$('#list-dataradiologi-riwayat .pilihradiologi').click(function(){
			        	var id = $(this).data('id');
			        	var keys = $(this).data('keys');
			        	var nama = $(this).data('nama');
			        	$("#rads").append('\n\
	                    <tr id="'+keys+'">\n\
	                        <td class="tr_mod"><input type="hidden" name="kd_jenis_prw_rad[]" value="'+id+'"><h6>'+nama+'</h6></td>\n\
	                        <td style="cursor:pointer;" class="delete" data-id="'+keys+'"><h6><i class="fa fa-delete"> Hapus</i></h6></td>\n\
	                    </tr>');
	                    //$('#list-datadignosa #'+id).remove();
			        });
	    		}
	    	});
        });

        delete_rads();
        // Area akhir module radiologi

        // Area awal module tindakan
        $("#tindakan").autocomplete({
            source: function(request, response){
            	$.ajax({
            		url : "<?php echo site_url($this->uri->segment(1).'/getTindakan');?>",
            		dataType: "json",
            		data : {
            			nama_perawatan : request.term,
            			kd_pj : kd_pj,
            			kd_poli : kd_poli
            		},
            		success : function(data)
            		{
            			response(data);
            		}
            	});
            },
            minLength: 3,
            focus: function( event, ui ) {
            	event.preventDefault();
            	$(this).val(ui.item.label);
            },
            select: function(event, ui) {
                //console.log(event);
                $(this).val('');//.blur();
                event.preventDefault(); // cancel default behavior which updates input field
                
                $("#tind").append('\n\
                    <tr id="'+ui.item.id+'">\n\
                        <td class="tr_mod">\n\
                        <input type="hidden" name="kd_jenis_prw_tindakan[]" value='+ui.item.id+'>\n\
                        <input type="hidden" name="material[]" value='+ui.item.material+'>\n\
                        <input type="hidden" name="bhp[]" value='+ui.item.bhp+'>\n\
                        <input type="hidden" name="tarif_tindakanpr[]" value='+ui.item.tarif_tindakanpr+'>\n\
                        <input type="hidden" name="tarif_tindakandr[]" value='+ui.item.tarif_tindakanpr+'>\n\
                        <input type="hidden" name="kso[]" value='+ui.item.kso+'>\n\
                        <input type="hidden" name="menejemen[]" value='+ui.item.menejemen+'>\n\
                        <input type="hidden" name="biaya_rawat[]" value='+ui.item.biaya_rawat+'>\n\
                        <h6>'+ui.item.value+'</h6>\n\
                        </td>\n\
                        <td style="cursor:pointer;" class="delete" data-id="'+ui.item.id+'"><h6><i class="fa fa-delete"> Hapus</i></h6></td>\n\
                    </tr>');
            }
        });

        $('#tind').on('click', '.delete', function(){
            var tr = $(this).data('id');
           $('table#tind tr#'+tr).remove();
        });

        $('#formsimpan_tindakan').on('click','#simpantindakan', function(){
        	var tind = $('[name="kd_jenis_prw_tindakan[]"]').serialize();
        	var material = $('[name="material[]"]').serialize();
        	var bhp = $('[name="bhp[]"]').serialize();
        	var tarif_tindakandr = $('[name="tarif_tindakandr[]"]').serialize();
        	var tarif_tindakanpr = $('[name="tarif_tindakanpr[]"]').serialize();
        	var kso = $('[name="kso[]"]').serialize();
        	var menejemen = $('[name="menejemen[]"]').serialize();
        	var biaya_rawat = $('[name="biaya_rawat[]"]').serialize();
        	var t = $('[name="timerDurasiTindakan"]').val();
        	$("#tind tr").remove();
        	if(tind!='')
        	{
        		$.ajax({
	        		type : 'post',
	        		url : link+'/savetind',
	        		data : tind+'&'+material+'&'+bhp+'&'+tarif_tindakandr+'&'+tarif_tindakanpr+'&'+kso+'&'+menejemen+'&'+biaya_rawat+'&no_rawat='+NoRawat+'&kd_dokter='+kd_dokter+'&tgl='+TglPerawatan+'&kd_poli='+kd_poli+'&t='+t,
	        		success : function(res)
	        		{
	        			//alert(res);
	        			var r = JSON.parse(res);
	        			if(r.error=='')
	        			{
	        				showmodal('Data berhasil disimpan.');
	        				$('.showtindakan').html(r.view);
	        				delete_tindakan();
	        				reset_timer();
	        			}
	        			else
	        			{
	        				showmodal('Data gagal disimpan.');
	        				$('.showtindakan').html(r.view);
	        			}
	        		}
	        	});
        	}
        	else
        	{
        		alert('Data tidak boleh kosong.');
        	}
        });

        $('#no_rawat_tindakan').change(function(){
        	var no_rawat_soap_change = $('#no_rawat_tindakan').val();
			$.ajax({
	    		type : 'post',
	    		url : link+'/history_tindakan_last',
	    		data : 'no_rawat='+no_rawat_soap_change,
	    		success : function(res)
	    		{
	    			tgl_kunjungan(no_rawat_soap_change,link);
	    			$('#history_tindakan_last').html(res);
	    			$('#list-datatindakan-riwayat .pilihtindakan').click(function(){
			        	var kd_jenis_prw = $(this).data('kd_jenis_prw');
			        	var material = $(this).data('material');
			        	var bhp = $(this).data('bhp');
			        	var kso = $(this).data('kso');
			        	var menejemen = $(this).data('menejemen');
			        	var tarif_tindakanpr = $(this).data('tarif_tindakanpr');
			        	var tarif_tindakandr = $(this).data('tarif_tindakandr');
			        	var biaya_rawat = $(this).data('biaya_rawat');
			        	var nm_perawatan = $(this).data('nm_perawatan');
			        	var nm_kategori = $(this).data('nm_kategori');
			        	$("#tind").append('\n\
	                    <tr id="'+kd_jenis_prw+'">\n\
	                        <td class="tr_mod">\n\
                        <input type="hidden" name="kd_jenis_prw_tindakan[]" value='+kd_jenis_prw+'>\n\
                        <input type="hidden" name="material[]" value='+material+'>\n\
                        <input type="hidden" name="bhp[]" value='+bhp+'>\n\
                        <input type="hidden" name="tarif_tindakanpr[]" value='+tarif_tindakanpr+'>\n\
                        <input type="hidden" name="tarif_tindakandr[]" value='+tarif_tindakandr+'>\n\
                        <input type="hidden" name="kso[]" value='+kso+'>\n\
                        <input type="hidden" name="menejemen[]" value='+menejemen+'>\n\
                        <input type="hidden" name="biaya_rawat[]" value='+biaya_rawat+'>\n\
                        <h6>'+nm_perawatan+'</h6></td>\n\
	                        <td style="cursor:pointer;" class="delete" data-id="'+kd_jenis_prw+'"><h6><i class="fa fa-delete"> Hapus</i></h6></td>\n\
	                    </tr>');
	                    //$('#list-datadignosa #'+id).remove();
			        });
	    		}
	    	});
        });

        delete_tindakan();
        // Area akhir module tindakan

        // Area awal module obat
        $('.add-formobat').click(function(){
        	var rowobat = $('#tb-resepobat tr').length;
        	var inputId = $('#tb-resepobat tr').last().attr('id');
        	var counter = 1;
        	
        	if(rowobat >= 0)
        	{
        		$('#add-obat').css('display','block');
        		$('#simpanobat').css('display','block');
        	}

        	if(inputId)
        	{
        		//alert();
        		inputId = parseInt(/^.*\_(\d+)$/.exec(inputId)[1]) + counter;
        		var datas = '<tr class="addtr" id=resep_obat_'+inputId+' data-id=resep_obat_'+inputId+'>\n\
        				<td width="50"><input type="checkbox" class="form-control" style="width:20px;" name="record"></td>\n\
						<td>\n\
						<select name="kode_obat[]" class="kd_obat form-control" style="width:100%"></select>\n\
						</td>\n\
						<td width="100">\n\
						<input type="number" name="jlm_obat[]" list="jumlahobat" class="form-control jlm_obat">\n\
						</td>\n\
						<td width="100">\n\
						<textarea name="keterangan[]" class="form-control"></textarea>\n\
						</td>\n\
						<td width="200"><?php echo  $this->instrument->htmlSelectFromArray($aturan_pakai, 'name="aturan_pakai[]" id="kd_poli" class="form-control aturan_pakai"', true);?>\n\
						</td>\n\
						</tr>';
						
        	}
        	else
        	{
        		var datas = '<tr class="addtr" id=resep_obat_'+counter+' data-id=resep_obat_'+counter+' style="">\n\
    				<td width="50"><input type="checkbox" class="form-control" style="width:20px;" name="record"></td>\n\
					<td>\n\
					<select name="kode_obat[]" class="kd_obat form-control" style="width:100%"></select>\n\
					</td>\n\
					<td width="100">\n\
					<input type="number" name="jlm_obat[]" list="jumlahobat" class="form-control jlm_obat">\n\
					</td>\n\
					<td width="100">\n\
					<textarea name="keterangan[]" class="form-control"></textarea>\n\
					</td>\n\
					<td width="200"><?php echo  $this->instrument->htmlSelectFromArray($aturan_pakai, 'name="aturan_pakai[]" id="kd_poli" class="form-control aturan_pakai"', true);?>\n\
					</td>\n\
					</tr>';
        	} 	  

			$('.tblInputO tbody').append(datas);

			$('.tblInputO tbody .kd_obat').select2({
				minimumInputLength: 3,
				allowClear: true,
				placeholder: 'Pilih obat',
				ajax: 
				{
					url: '<?php echo site_url($this->uri->segment(1).'/getObat');?>',
					dataType: 'json',
					delay: 250,
					data : function(params)
					{
						return {
		                  search: params.term
		                }
					},
					processResults: function (data, page) 
					{
						return {
							results: data
						};
					},
				}
	        }).on('select2:select', function (evt) {
		        var data = $(".kode_obat option:selected").val();
		        
		        /*$.ajax({
		         	type 	: 'post',
		         	url 	: '<?php //echo site_url($this->uri->segment(1).'/getStatusObat');?>',
		         	data 	: 'kode_brng='+data+'&kd_bangsal=Far2',
		         	success : function(res)
		         	{
		         		var data_msg = JSON.parse(res);
		         		if(data_msg.msg!='')
		         		{
		         			showmodal(data_msg.msg +'<br>'+ data_msg.status);
		         			//$(".tblInputO tbody").find('.kd_obat option:selected').parents("tr").remove();
		         		}
		         		
		         	}
		        });*/
		    });

	        //$('.tblInputO tbody .aturan_pakai').change(function(){
	        //	var jlm_obat = $('[name="jlm_obat[]"]').serialize();
	        //	var kode_obat = $('[name="kode_obat[]"]').serialize();
	        //	var aturan_pakai = $('[name="aturan_pakai[]"]').serialize();
	        //	var keterangan = $('[name="keterangan[]"]').serialize();
	        //	$.ajax({
	        //		type : 'post',
	        //		url : link+'/getHargaObat',
	        //		data : jlm_obat+'&'+kode_obat+'&tgl_perawatan='+TglPerawatan+'&no_rawat='+NoRawat+'&'+keterangan+'&'+aturan_pakai,
	        //		success : function(res)
	        //		{
	        //			$('#biling_pasien').html(res);
	        //		}
	        //	});
	        //});

	        $(".del").click(function(){
		        $(".tblInputO tbody").find('input[name="record"]').each(function(){
	            	if($(this).is(":checked")){
	            		$(this).parents("tr").remove();
	                }
	            });
		    }); 
        });

        $('#formsimpan_obat').on('click','#simpanobat', function(){
	    	var jlm_obat = $('[name="jlm_obat[]"]').serialize();
        	var kode_obat = $('[name="kode_obat[]"]').serialize();
        	var aturan_pakai = $('[name="aturan_pakai[]"]').serialize();
        	var keterangan = $('[name="keterangan[]"]').serialize();
        	var t = $('[name="timerDurasiObat"]').val();

	    	$("#tb-resepobat tr").remove();
	    	if(kode_obat!='' && jlm_obat!='' && aturan_pakai!='')
	    	{
	    		$.ajax({
	        		type : 'post',
	        		url : link+'/saveResepObat1',
	        		data : jlm_obat+'&'+kode_obat+'&tgl_perawatan='+TglPerawatan+'&no_rawat='+NoRawat+'&'+keterangan+'&'+aturan_pakai+'&kd_dokter='+kd_dokter+'&kd_poli='+kd_poli+'&t='+t,
	        		success : function(res)
	        		{
	        			var r = JSON.parse(res);
	        			if(r.error=='')
	        			{
	        				showmodal('Data berhasil disimpan.');
	        				$('.showresepobat').html(r.view);
	        				delete_resepobat();
	        				reset_timer();
	        			}
	        			else
	        			{
	        				showmodal('Data gagal disimpan.');
	        				$('.showresepobat').html(r.view);
	        			}
	        		}
	        	});
	    	}	    	
	    });

        $('#no_rawat_obat').change(function(){
        	var no_rawat_soap_change = $('#no_rawat_obat').val();
			$.ajax({
	    		type : 'post',
	    		url : link+'/history_obat_last',
	    		data : 'no_rawat='+no_rawat_soap_change,
	    		success : function(res)
	    		{
	    			tgl_kunjungan(no_rawat_soap_change,link);
	    			$('#history_obat_last').html(res);
	    			
	    			// Delete
	    			$('.delcr').click(function(){
	    				$(".tblInput2 tbody").find('input[name="records"]').each(function(){
			            	if($(this).is(":checked")){
			            		if($(this).is(":checked")){
				            		$(this).parents("tr").remove();
				                }
			                }
			            });
	    			});	

	    			// Simpan
	    			$('.copyr').click(function(){
	    				var jlm_obat = $('[name="jlm_obat[]"]').serialize();
			        	var kode_obat = $('[name="kode_obat[]"]').serialize();
			        	var aturan_pakai = $('[name="aturan_pakai[]"]').serialize();
			        	var keterangan = $('[name="keterangan[]"]').serialize();
			        	var t = $('[name="timerDurasiObat"]').val();

			        	$("#tb-resepobatcopy tr").remove();
			        	if(kode_obat!='' && jlm_obat!='' && aturan_pakai!='')
				    	{
				    		$.ajax({
				        		type : 'post',
				        		url : link+'/saveResepObat1',
				        		data : jlm_obat+'&'+kode_obat+'&tgl_perawatan='+TglPerawatan+'&no_rawat='+NoRawat+'&'+keterangan+'&'+aturan_pakai+'&kd_dokter='+kd_dokter+'&kd_poli='+kd_poli+'&t='+t,
				        		success : function(res)
				        		{
				        			var r = JSON.parse(res);
				        			if(r.error=='')
				        			{
				        				showmodal('Data berhasil disimpan.');
				        				$('.showresepobat').html(r.view);
				        				$('.delcr').css('display','none');
				        				$('.copyr').css('display','none');
				        				delete_resepobat();
				        				reset_timer();
				        			}
				        			else
				        			{
				        				showmodal('Data gagal disimpan.');
				        				$('.showresepobat').html(r.view);
				        			}
				        		}
				        	});
				    	}	   
	    			});	
	    		}
	    	});
        });

	    delete_resepobat();
        // Area akhir module obat

	   	// Area awal module pemeriksaan soap
        $('#formsimpan_pemeriksaan').on('click','#simpanpemeriksaan', function(){
        	var s = $('[name="s"]').val();
        	var o = $('[name="o"]').val();
        	var a = $('[name="a"]').val();
        	var p = $('[name="p"]').val();
        	var t = $('[name="timerDurasiSoap"]').val();

        	if(s!='')
        	{
        		$.ajax({
	        		type : 'post',
	        		url : link+'/savepemeriksaan',
	        		data : 'no_rawat='+NoRawat+'&kd_dokter='+kd_dokter+'&kd_poli='+kd_poli+'&tgl='+TglPerawatan+'&s='+s+'&o='+o+'&a='+a+'&p='+p+'&t='+t,
	        		success : function(res)
	        		{
	        			/*alert(res);*/
	        			var r = JSON.parse(res);
	        			if(r.error=='')
	        			{
	        				$('[name="s"]').val(r.s);
	        				$('[name="o"]').val(r.o);
	        				$('[name="a"]').val(r.a);
	        				$('[name="p"]').val(r.p);
	        				$('#modalUmum').css('display','block');
	        				$('#msg').html(r.msg);
	        				$('#exampleModal2').modal();
	        				reset_timer();
	        			}
	        			else
	        			{
	        				$('#modalUmum').css('display','block');
	        				$('#msg').html(r.msg);
	        				$('#exampleModal2').modal();
	        			}
	        		}
	        	});
        	}
        	else
        	{
        		alert('Data tidak boleh kosong.');
        	}
        });

        $('#datapemeriksaan').on('click','.edit-pemeriksaan',function(){
        	$.ajax({
        		type : 'post',
        		url  : link+'/editpemeriksaan',
        		data : 'NoRawat='+NoRawat+'&edit=edit',
        		success : function(res)
        		{
        			$('#datapemeriksaan').html(res);
        		} 
        	});
        });

        $('#datapemeriksaan').on('click','.update-pemeriksaan',function(){
        	var data = $('#datapemeriksaan :input').serialize();
        	$.ajax({
        		type : 'post',
        		url  : link+'/updatepemeriksaan',
        		data : 'no_rawat='+NoRawat+'&'+data,
        		success : function(res)
        		{
        			var d = JSON.parse(res);
        			if(d.error==1)
        			{
        				$('#modalUmum').css('display','block');
        				$('#msg').html(d.html);
        				$('#exampleModal2').modal();
        				reset_timer();
        			}
        			else
        			{
        				$('#modalUmum').css('display','block');
        				$('#msg').html('Data berhasil disimpan');
        				$('#exampleModal2').modal();
        				$('#datapemeriksaan').html(d.html);
        			}
        		} 
        	});
        });

        $('#datapemeriksaan').on('click','.cancel-pemeriksaan',function(){
        	var data = $('#datapemeriksaan :input').serialize();
        	$.ajax({
        		type : 'post',
        		url  : link+'/editpemeriksaan',
        		data : 'NoRawat='+NoRawat,
        		success : function(res)
        		{
        			$('#datapemeriksaan').html(res);
        		} 
        	});
        });

        $('#history_soap_last').on('click','#simpanpemeriksaan_history', function(){
        	var s = $('#history_soap_last [name="s"]').val();
        	var o = $('#history_soap_last [name="o"]').val();
        	var a = $('#history_soap_last [name="a"]').val();
        	var p = $('#history_soap_last [name="p"]').val();
        	var t = $('[name="timerDurasiSoap"]').val();
        	
        	if(s!='')
        	{
        		$.ajax({
	        		type : 'post',
	        		url : link+'/savepemeriksaan',
	        		data : 'no_rawat='+NoRawat+'&kd_dokter='+kd_dokter+'&kd_poli='+kd_poli+'&tgl='+TglPerawatan+'&s='+s+'&o='+o+'&a='+a+'&p='+p+'&t='+t,
	        		success : function(res)
	        		{
	        			var r = JSON.parse(res);
	        			if(r.error=='')
	        			{
	        				$('[name="s"]').val(r.s);
	        				$('[name="o"]').val(r.o);
	        				$('[name="a"]').val(r.a);
	        				$('[name="p"]').val(r.p);
	        				$('#modalUmum').css('display','block');
	        				$('#msg').html(r.msg);
	        				$('#exampleModal2').modal();
	        				reset_timer();
	        			}
	        			else
	        			{
	        				$('#modalUmum').css('display','block');
	        				$('#msg').html(r.msg);
	        				$('#exampleModal2').modal();
	        			}
	        		}
	        	});
        	}
        	else
        	{
        		alert('Data tidak boleh kosong.');
        	}
        });

        $('#no_rawat_soap').change(function(){
        	var no_rawat_soap_change = $('#no_rawat_soap').val();
			$.ajax({
	    		type : 'post',
	    		url : link+'/history_soap_last',
	    		data : 'no_rawat='+no_rawat_soap_change,
	    		success : function(res)
	    		{
	    			tgl_kunjungan(no_rawat_soap_change,link);
	    			$('#history_soap_last').html(res);
	    		}
	    	});
        });
        // Area akhir module pemeriksaan soap
	});
	
	function tgl_kunjungan(norawatTgl,link)
	{
		//alert(norawatTgl.replace('/','-'));
		if(norawatTgl)
		{
			$.ajax({
				type : 'GET',
				url : link+'/norawatTglkunjungan/'+norawatTgl,
				success : function(data)
				{
					$('.tgl_kunjungan').html(data);
				}
			});
		}
	}
	
	function formatData (data) {
		var ret;
		if(data.stok <= 0)
    	{
    		ret = '<span class="text-primary">'+ data.text+'</span>';
    	}
    	else
    	{
    		ret = '<span>'+ data.text +'</span>';
    	}
        var $data = $(
        	ret
        );
        return $data;
    };

	function filterTable(event) {
	    var filter = event.target.value.toUpperCase();
	    var rows = document.querySelector("#myTable tbody").rows;
	    
	    for (var i = 0; i < rows.length; i++) {
	        var firstCol = rows[i].cells[0].textContent.toUpperCase();
	        var secondCol = rows[i].cells[1].textContent.toUpperCase();
	        if (firstCol.indexOf(filter) > -1 || secondCol.indexOf(filter) > -1) {
	            rows[i].style.display = "";
	        } else {
	            rows[i].style.display = "none";
	        }      
	    }
	}

	function updateOrder(data,norawat) {
        $.ajax({
            url:"<?php echo base_url($this->uri->segment(1));?>/updateDiagnosa",
            type:'post',
            data:'kd_penyakit='+data+'&no_rawat='+norawat,
            success:function(res){
            	var r = JSON.parse(res);
            	if(r.error=='')
            	{
            		//alert('perubahan Anda berhasil disimpan');
            		$('.showdiagnosa').html(r.view);
            		delete_diagnosa();
            		sortTableDiag2();
            	}
            }
        })
    }

    function delete_diagnosa()
    {
    	$('.delete_diagnosa').click(function(){
	    	var tr = $(this).parent();
	    	if(tr.data('kd_penyakit')!='')
	    	{
	    		$.ajax({
	    			type : 'post',
	    			url : link+'/deleteDiagnosa',
	    			data : 'kd_penyakit='+tr.data('kd_penyakit')+'&no_rawat='+tr.data('no_rawat'),
	    			success : function(res)
	    			{
	    				var r = JSON.parse(res);
	    				if(r.error=='')
	    				{
	    					showmodal('Data berhasil dihapus.');
	    					$('.showdiagnosa').html(r.view);
	        				delete_diagnosa();
	        				$( ".showdiagnosa .tabledianosa2" ).sortable({
						        delay: 150,
						        stop: function() {
						            var selectedData = new Array();
						            $('.tabledianosa2>tr').each(function() {
						                selectedData.push($(this).attr("id"));
						            });
						            updateOrder(selectedData,NoRawat);
						        }
						    });
	    				}
	    				else
	    				{
	    					showmodal('Data gagal dihapus.');
	    				}
	    			}
	    		});
	    	}
	    });
    }

    function delete_resepobat()
    {
    	$('.deleteResepObat').click(function(){
	    	var tr = $(this).parent();
	    	if(tr.data('no_rawat')!='' && tr.data('kode_brng')!='')
	    	{
	    		$.ajax({
	    			type : 'post',
	    			url : link+'/deleteResepobat1',
	    			data : 'no_rawat='+tr.data('no_rawat')+'&kode_brng='+tr.data('kode_brng')+'&no_resep='+tr.data('no_resep'),
	    			success : function(res)
	    			{
	    				var r = JSON.parse(res);
	    				if(r.error=='')
	    				{
	    					showmodal('Data berhasil dihapus.');
	    					$('.showresepobat').html(r.view);
	    					delete_resepobat();
	    				}
	    				else
	    				{
	    					showmodal('Data gagal dihapus.');
	    				}
	    			}
	    		});
	    	}
	    });
    }

    function delete_labs()
    {
    	$('.deletelabs').click(function(){
	    	var tr = $(this).parent();
	    	if(tr.data('kd_jenis_prw')!='')
	    	{
	    		$.ajax({
	    			type : 'post',
	    			url : link+'/deletelabs',
	    			data : 'kd_jenis_prw='+tr.data('kd_jenis_prw')+'&no_rawat='+tr.data('no_rawat')+'&noorder='+tr.data('noorder'),
	    			success : function(res)
	    			{
	    				var r = JSON.parse(res);
	    				if(r.error=='')
	    				{
	    					showmodal('Data berhasil dihapus.');
	    					$('.showlabs').html(r.view);
	    					delete_labs();
	    				}
	    				else
	    				{
	    					showmodal('Data gagal dihapus.');
	    				}
	    			}
	    		});
	    	}
	    	else
	    	{
	    		alert('Mohon maaf anda tidak bisa menghapus.')
	    	}
	    });
    }

    function delete_rads()
    {
    	$('.deleterads').click(function(){
	    	var tr = $(this).parent();
	    	if(tr.data('kd_jenis_prw')!='')
	    	{
	    		$.ajax({
	    			type : 'post',
	    			url : link+'/deleterads',
	    			data : 'kd_jenis_prw='+tr.data('kd_jenis_prw')+'&no_rawat='+tr.data('no_rawat')+'&noorder='+tr.data('noorder'),
	    			success : function(res)
	    			{
	    				var r = JSON.parse(res);
	    				if(r.error=='')
	    				{
	    					showmodal('Data berhasil dihapus.');
	    					$('.showrads').html(r.view);
	    					delete_rads();
	    				}
	    				else
	    				{
	    					showmodal('Data gagal dihapus.');
	    				}
	    				
	    			}
	    		});
	    	}
	    	else
	    	{
	    		alert('Mohon maaf anda tidak bisa menghapus.')
	    	}
	    });
    }

    function delete_tindakan()
    {
    	$('.deletetind').click(function(){
	    	var tr = $(this).parent();
	    	if(tr.data('kd_jenis_prw')!='')
	    	{
	    		$.ajax({
	    			type : 'post',
	    			url : link+'/deletetindakan',
	    			data : 'kd_jenis_prw='+tr.data('kd_jenis_prw')+'&no_rawat='+tr.data('no_rawat'),
	    			success : function(res)
	    			{
	    				var r = JSON.parse(res);
	    				if(r.error=='')
	    				{
	    					showmodal('Data berhasil dihapus.');
	    					$('.showtindakan').html(r.view);
	    					delete_tindakan();
	    				}
	    				else
	    				{
	    					showmodal('Data gagal dihapus.');
	    				}
	    				
	    			}
	    		});
	    	}
	    	else
	    	{
	    		alert('Mohon maaf anda tidak bisa menghapus.')
	    	}
	    });
    }

    function sortTableDiag2()
    {
    	$( ".tablediagnosa2" ).sortable({
	        delay: 150,
	        stop: function() {
	            var selectedData = new Array();
	            $('.tablediagnosa2>tr').each(function() {
	                selectedData.push($(this).attr("id"));
	            });
	            updateOrder(selectedData,NoRawat);
	        }
	    });
    }

    function showmodal(pesan)
    {
    	$('#modalUmum').css('display','block');
		$('#msg').html(pesan);
		$('#exampleModal2').modal();
    }

    function voicesoap()
	{
		window.SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition || null;
		if (window.SpeechRecognition === null) {
			document.getElementById('unsupported').classList.remove('hidden');
		}else {
		    var recognizer = new window.SpeechRecognition();
		    var transcription = $('#voicetotexts');
			//Para o reconhecedor de voz, não parar de ouvir, mesmo que tenha pausas no usuario
			recognizer.continuous = true;
			recognizer.onresult = function(event){
				transcription.textContent = "";
				for (var i = event.resultIndex; i < event.results.length; i++) {
					if(event.results[i].isFinal){
						transcription.textContent = event.results[i][0].transcript;
					}else{
		            	transcription.textContent += event.results[i][0].transcript;
					}
				}
				
				if(transcription.textContent!='')
				{
					var res = transcription.textContent;
					if(res.match('subjektif')=='subjektif' || res.match('subjek')=='subjek' || res.match('subjective')=='subjective' || res.match('satu')=='satu' || res.match('1')=='1')
					{
						if(res.match('delete')=='delete')
						{
							data = '';
						}
						else
						{
							var s = $('#subjektif').val();
							if(s.length > 0)
							{
								var textvoice = res;
								var subjective = $('#subjektif').val();
								var data = subjective +', '+ textvoice.replace(/subjektif/g,'').replace(/subjective/g,'').replace(/satu/g,'').replace(/1/g,'');
							}
							else
							{
								var textvoice = res;
								var data = textvoice.replace(/subjektif/g,'').replace(/subjective/g,'').replace(/satu/g,'');
							}
						}
						
						$('#subjektif').val(data);
						
					}
					else if(res.match('objective')=='objective' || res.match('objektif')=='objektif' || res.match('dua')=='dua' || res.match('1')=='1' || res.match('objek')=='objek')
					{
						if(res.match('delete')=='delete')
						{
							data = '';
						}
						else
						{
							var s = $('#objective').val();
							if(s.length > 0)
							{
								var textvoice = res;
								var subjective = $('#objective').val();
								var data = subjective +', '+ textvoice.replace(/objective/g,'').replace(/objektif/g,'').replace(/dua/g,'');
							}
							else
							{
								var textvoice = res;
								var data = textvoice.replace(/objective/g,'').replace(/objektif/g,'').replace(/dua/g,'');
							}
						}
						
						$('#objective').val(data);
					}
					else if(res.match('pengkajian')=='pengkajian' || res.match('tiga')=='tiga' || res.match('3')=='3' || res.match('kaji')=='kaji')
					{
						if(res.match('delete')=='delete')
						{
							data = '';
						}
						else
						{
							var s = $('#pengkajian').val();
							if(s.length > 0)
							{
								var textvoice = res;
								var subjective = $('#pengkajian').val();
								var data = subjective +', '+ textvoice.replace(/pengkajian/g,'').replace(/tiga/g,'');
							}
							else
							{
								var textvoice = res;
								var data = textvoice.replace(/pengkajian/g,'').replace(/tiga/g,'');
							}
						}
						
						$('#pengkajian').val(data);
					}
					else if(res.match('rencana')=='rencana' || res.match('plan')=='plan' || res.match('empat')=='empat' || res.match('4')=='4')
					{
						if(res.match('delete')=='delete')
						{
							data = '';
						}
						else
						{
							var s = $('#rencana').val();
							if(s.length > 0)
							{
								var textvoice = res;
								var subjective = $('#rencana').val();
								var data = subjective +', '+ textvoice.replace(/rencana/g,'').replace(/empat/g,'');
							}
							else
							{
								var textvoice = res;
								var data = textvoice.replace(/rencana/g,'').replace(/empat/g,'');
							}
						}
						
						$('#rencana').val(data);
					}
					else if(res.match('simpan')=='simpan')
					{
			        	var s = $('#formsimpan_pemeriksaan [name="s"]').val();
			        	var o = $('#formsimpan_pemeriksaan [name="o"]').val();
			        	var a = $('#formsimpan_pemeriksaan [name="a"]').val();
			        	var p = $('#formsimpan_pemeriksaan [name="p"]').val();
			        	var t = $('#formsimpan_pemeriksaan [name="timerDurasiSoap"]').val();

			        	if(s!='')
			        	{
			        		$.ajax({
				        		type : 'post',
				        		url : link+'/savepemeriksaan',
				        		data : 'no_rawat='+NoRawat+'&kd_dokter='+kd_dokter+'&kd_poli='+kd_poli+'&tgl='+TglPerawatan+'&s='+s+'&o='+o+'&a='+a+'&p='+p+'&t='+t,
				        		success : function(res)
				        		{
				        			/*alert(res);*/
				        			var r = JSON.parse(res);
				        			if(r.error=='')
				        			{
				        				$('[name="s"]').val(r.s);
				        				$('[name="o"]').val(r.o);
				        				$('[name="a"]').val(r.a);
				        				$('[name="p"]').val(r.p);
				        				$('#modalUmum').css('display','block');
				        				$('#msg').html(r.msg);
				        				$('#exampleModal2').modal();
				        				reset_timer();
				        			}
				        			else
				        			{
				        				$('#modalUmum').css('display','block');
				        				$('#msg').html(r.msg);
				        				$('#exampleModal2').modal();
				        			}
				        		}
				        	});
			        	}
			        	else
			        	{
			        		alert('Data tidak boleh kosong.');
			        	}
					}
					else if(res.match('clear')=='clear')
					{
						$('#formsimpan_pemeriksaan [name="s"]').val('');
			        	$('#formsimpan_pemeriksaan [name="o"]').val('');
			        	$('#formsimpan_pemeriksaan [name="a"]').val('');
			        	$('#formsimpan_pemeriksaan [name="p"]').val('');
					}
					else if(res.match('stop')=='stop')
					{
						recognizer.stop();
					}
					else
					{
						alert('Perintah ini '+res+' tidak bisa di laksanakan.');
					}
					
				}
			}

			document.querySelector("#rect").addEventListener("click",function(){
				try {
		            recognizer.start();
		          } catch(ex) {
		          	alert("error: "+ex.message);
		          }
			});

			document.querySelector("#rectstop").addEventListener("click",function(){
				try {
		            recognizer.stop();
		          } catch(ex) {
		          	alert("error: "+ex.message);
		          }
			});
		}
	}

	function voicediagnosa()
	{
		window.SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition || null;
		if (window.SpeechRecognition === null) {
			document.getElementById('unsupported').classList.remove('hidden');
		}else {
		    var recognizer = new window.SpeechRecognition();
		    var transcription = $('#voicetotextsdiag');
			//Para o reconhecedor de voz, não parar de ouvir, mesmo que tenha pausas no usuario
			recognizer.continuous = true;
			recognizer.onresult = function(event){
				transcription.textContent = "";
				for (var i = event.resultIndex; i < event.results.length; i++) {
					if(event.results[i].isFinal){
						transcription.textContent = event.results[i][0].transcript;
					}else{
		            	transcription.textContent += event.results[i][0].transcript;
					}
				}
				
				var res;
				
				if(transcription.textContent!='')
				{
					res = transcription.textContent;
					$.ajax({
		        		url : "<?php echo site_url($this->uri->segment(1).'/getDiagnosavoice');?>",
		            	type : "GET",
		            	data:
			            {
			                term: res.replace('+',''),
			            },
		            	success: function(data){
		            		$('#recordsdiagnosa').html(data);
		            		$('#voicetotextsdiag').val(res);
		            		$('#resdiagnosa .pilihdiagnosa').click(function(){
					        	var id = $(this).data('id');
					        	var keys = $(this).data('keys');
					        	var nama = $(this).data('nama');
					        	$("#list-datadignosa").append('\n\
			                    <tr id="'+keys+'">\n\
			                        <td class="tr_mod"><input type="hidden" name="kd_penyakit[]" value='+id+'><h6>'+nama+'</h6></td>\n\
			                        <td style="cursor:pointer;" class="delete" data-id="'+keys+'"><h6><i class="fa fa-delete"> Hapus</i></h6></td>\n\
			                    </tr>');

			                    $('#resdiagnosa').remove();
					        });
		            	}
		        	});
				}
			}
		}

		document.querySelector("#rectdiag").addEventListener("click",function(){
			try {
	            recognizer.start();
	          } catch(ex) {
	          	alert("error: "+ex.message);
	          }
		});

		document.querySelector("#rectstopdiag").addEventListener("click",function(){
			try {
	            recognizer.stop();
	          } catch(ex) {
	          	alert("error: "+ex.message);
	          }
		});
	}

	/*function voicelaboratorium()
	{
		window.SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition || null;
		if (window.SpeechRecognition === null) {
			document.getElementById('unsupported').classList.remove('hidden');
		}else {
		    var recognizer = new window.SpeechRecognition();
		    var transcription = document.getElementById("voicetotextslabs");
			//Para o reconhecedor de voz, não parar de ouvir, mesmo que tenha pausas no usuario
			recognizer.continuous = true;
			recognizer.onresult = function(event){
				transcription.textContent = "";
				for (var i = event.resultIndex; i < event.results.length; i++) {
					if(event.results[i].isFinal){
						transcription.textContent = event.results[i][0].transcript;
					}else{
		            	transcription.textContent += event.results[i][0].transcript;
					}
				}
				
				var res;
				
				if(transcription.textContent!='')
				{
					res = transcription.textContent;
					$.ajax({
		        		url : "<?php echo site_url($this->uri->segment(1).'/getLaboratoriumVoice');?>",
		            	type : "GET",
		            	data:
			            {
			                term: res.replace('+',''),
			            },
		            	success: function(data){
		            		$('#recordslaboratorium').html(data);
		            		$('#voicetotextslabs').val(res);
		            		$('#reslaboratorium .pilihlaboratorium').click(function(){
					        	var id = $(this).data('id');
					        	var keys = $(this).data('keys');
					        	var nama = $(this).data('nama');
					        	$("#labs").append('\n\
				                    <tr id="'+id+'">\n\
				                        <td class="tr_mod"><input type="hidden" name="kd_jenis_prw_labs[]" value='+id+'><h6>'+nama+'</h6></td>\n\
				                        <td style="cursor:pointer;" class="delete" data-id="'+keys+'"><h6><i class="fa fa-delete"> Hapus</i></h6></td>\n\
				                    </tr>');

			                    $('#reslaboratorium').remove();
					        });
		            	}
		        	});
				}
			}
		}

		document.querySelector("#rectlabs").addEventListener("click",function(){
			try {
	            recognizer.start();
	          } catch(ex) {
	          	alert("error: "+ex.message);
	          }
		});

		document.querySelector("#rectstoplabs").addEventListener("click",function(){
			try {
	            recognizer.stop();
	          } catch(ex) {
	          	alert("error: "+ex.message);
	          }
		});
	}*/

    function startTimer(selector){
	    var ini     = selector
	    if(ini.val()!='' && ini.val()!=null){
	        var split   = ini.val().split(':');
	        var detik   = split[2];
	        var menit   = split[1];
	        var jam     = split[0];
	        
	        detik++;
	        if(detik == 60){
	            menit++;
	            detik = '00';
	            if(menit == 60){
	                jam++;
	                menit = '00';
	            }
	        }
	        
	        if(detik.toString().length == 1){
	           detik = '0'+detik ;
	        }
	        if(menit.toString().length == 1){
	           menit = '0'+menit ;
	        }
	        if(jam.toString().length == 1){
	           jam = '0'+jam ;
	        }
	        
	        ini.val(jam+':'+menit+':'+detik);
	    }
	}

	function reset_timer()
	{
		$('#timerDurasiDiagnosa').val('00:00:00');
		$('#timerDurasiSoap').val('00:00:00');
		$('#timerDurasiObat').val('00:00:00');
		$('#timerDurasiLab').val('00:00:00');
		$('#timerDurasiRad').val('00:00:00');
		$('#timerDurasiTindakan').val('00:00:00');
	}

    function clock() {
      var now = new Date();
      var secs = ('0' + now.getSeconds()).slice(-2);
      var mins = ('0' + now.getMinutes()).slice(-2);
      var hr = now.getHours();
      var Time = hr + ":" + mins + ":" + secs;
      document.getElementById("watch").innerHTML = 'Jam : '+Time;
      requestAnimationFrame(clock);
    }
	requestAnimationFrame(clock);

	document.querySelector('#myInput').addEventListener('keyup', filterTable, false);

	var kedipan = 750; 
	var ele = document.getElementById('ralert')
	if(ele)
	{
		var dumet = setInterval(function () {
		    ele.style.visibility = (ele.style.visibility == 'hidden' ? '' : 'hidden');
		}, kedipan);
	}
</script>