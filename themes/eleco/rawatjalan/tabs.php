<div class="container h-100 py-2">
						<ul class="nav nav-tabs border-0" id="myTab" role="tablist">
						  <li class="nav-item">
						    <a class="nav-link active border border-primary border-bottom-0" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Formulir SOAP</a>
						  </li>
						  <li class="nav-item">
						    <a class="nav-link text-bold" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Formulir Diagnosa</a>
						  </li>
						  <li class="nav-item">
						    <a class="nav-link text-bold" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Formulir Resep Obat Pasien</a>
						  </li>
						  <li class="nav-item">
						    <a class="nav-link text-bold" id="labs-tab" data-toggle="tab" href="#labs" role="tab" aria-controls="labs" aria-selected="false">Formulir Permintaan Lab</a>
						  </li>
						  <li class="nav-item">
						    <a class="nav-link text-bold" id="rads-tab" data-toggle="tab" href="#rads" role="tab" aria-controls="rads" aria-selected="false">Formulir Permintaan Rad</a>
						  </li>
						  <li class="nav-item">
						    <a class="nav-link text-bold" id="tins-tab" data-toggle="tab" href="#tins" role="tab" aria-controls="tins" aria-selected="false">Formulir Tindakan Pasien</a>
						  </li>
						</ul>

						<div class="tab-content h-75">
						  <div class="tab-pane h-100 p-3 active border border-primary" id="home" role="tabpanel" aria-labelledby="home-tab">
						  	<?php $this->site->get_template('rawatjalan/soap');?>
						  </div>
						  <div class="tab-pane fade h-100 p-3 border border-warning" id="profile" role="tabpanel" aria-labelledby="profile-tab">
						  	<?php $this->site->get_template('rawatjalan/diagnosa');?>
						  </div>
						  <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
						  	<?php $this->site->get_template('rawatjalan/obat');?>
						  </div>
						  <div class="tab-pane fade" id="labs" role="tabpanel" aria-labelledby="labs-tab">
						  	<?php $this->site->get_template('rawatjalan/laboratorium');?>
						  </div>
						  <div class="tab-pane fade" id="rads" role="tabpanel" aria-labelledby="rads-tab">
						  	<?php $this->site->get_template('rawatjalan/radiologi');?>
						  </div>
						   <div class="tab-pane fade" id="tins" role="tabpanel" aria-labelledby="tins-tab">
						  	<?php $this->site->get_template('rawatjalan/tindakan');?>
						  </div>
						</div>
					</div>