<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/ui-lightness/jquery-ui.css" type="text/css" rel="stylesheet" />
<?php $this->site->get_template('inc/navbar-page.html'); ?>
<!-- tabs -->
	<div class="open-hours segments-page">
		<div class="container">
			<div class="row" style="padding: 5px;">
				<div class="col-12 mb-2">
					<a href="<?php echo base_url($this->uri->segment(1).'/index');?>" class="btn btn-primary col-12">Kembali</a>
				</div>
				<div class="col-12">
					<div class="row">
						<div class="col-8">
							<div class="card">
								<div class="card-header panel-heading no-border bg-primary" style="padding: 10px; background: blue;"><h4 class="text-white"><?php echo ($dtpasien[0]->jk == 'L') ? '<i class="fa fa-male text-dark"></i>' : '<i class="fa fa-female text-dark"></i>'; ?> <?php echo $dtpasien[0]->nm_pasien; ?></h4></div>
								<div class="col-md-12" style="padding: 5px 0;">
									<table class="table-responsive-md" style="padding: 0px; margin: 0px;">
										<tr style="padding: 0px; margin: 0px;">
											<td>&nbsp; - Nomor Antrian</td>
											<td>:</td>
											<td><b><?php echo $dtpasien[0]->no_reg; ?></b></td>
											<td>&nbsp; -</td>
											<td>Poliklinik</td>
											<td>:</td>
											<td><b><?php echo $dtpasien[0]->nm_poli; ?></b></td>
										</tr>
										<tr>
											<td>&nbsp; - No Rekamedis</td>
											<td>:</td>
											<td><b><?php echo $dtpasien[0]->no_rkm_medis; ?></b></td>
											<td>&nbsp; -</td>
											<td>Cara Bayar</td>
											<td>:</td>
											<td><b><?php echo $dtpasien[0]->png_jawab; ?></b></td>
										</tr>
										<tr>
											<td>&nbsp; - Nama Pasien</td>
											<td>:</td>
											<td><b><?php echo $dtpasien[0]->nm_pasien; ?></b></td>
											<td>&nbsp; -</td>
											<td>No Rawat</td>
											<td>:</td>
											<td><b><?php echo $noRawat; ?></b></td>
										</tr>
										<tr>
											<td>&nbsp; - Tanggal Lahir</td>
											<td>:</td>
											<td><?php echo '<b>'. date('d M Y', strtotime($dtpasien[0]->tgl_lahir)) .'</b>';?> | Umur (<b><?php echo $usia['year'] . ' Tahun, ' . $usia['month'] .' Bulan, '. $usia['day'] .' Hari'; ?></b>)</td>
											<td>&nbsp; -</td>
											<td>Bahasa Pasien</td>
											<td>:</td>
											<td><b><?php echo $dtpasien[0]->nama_bahasa; ?></b></td>
										</tr>
										<tr>
											<td>&nbsp; - Jenis Kelamin</td>
											<td>:</td>
											<td><b><?php echo ($dtpasien[0]->jk == 'L') ? '<h6><i class="fa fa-male text-dark"></i>  Laki-laki</h6>' : '<h6><i class="fa fa-female text-dark"></i> Perempuan</h6>'; ?></b></td>
											<td>
												<?php  
												echo ($pemeriksaan[0]->alergi=='Ada') ? '<span class="border border-danger bg-danger text-white p-1" id="ralert">Alergi</span>' : '';
												?>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>

						<div class="col-4">
							<div class="input-group">      
						        <input type="text" class="form-control form-control-small bg-light" style="border-radius: 0;" id="myInput" placeholder="Pencarian data antrian">
						    </div>
							<div class="bg-white" style="border-left:1px solid rgba(0,0,0,.125); border-right:1px solid rgba(0,0,0,.125);border-bottom:1px solid rgba(0,0,0,.125);">
								<div class="col-md-12" style="height: 118px;  max-height: 118px; overflow-y: scroll; padding: 5px 10px 0px 10px;">
									<?php  
									echo '<table id="myTable">';
										$no = 0;
										foreach ($antrian as $k => $v) {
											$no++;
											echo '<tr>';
												echo '<td '; 
												echo ($noRawat == $v->no_rawat) ? 'class="blue" ' : '';
												echo '>Antrian '.$no.'.</td>';
												echo '<td><a href="'.base_url($this->uri->segment(1).'/kajipasien/'.str_replace('/', '-', $v->no_rawat)).'/'.$v->tgl_registrasi.'/'.$v->no_rkm_medis.'" ';
												echo ($noRawat == $v->no_rawat) ? 'class="blue" ' : '';
												echo '>'.$v->no_reg.' - '. $this->instrument->cutName($v->nm_pasien) .' ('.$v->no_rkm_medis.')</a></td>';
											echo '</tr>';
										}
									echo '</table>';
									?>
								</div>
							</div>
						</div>

						<div class="col-12 mt-2">
							<div class="card mb-2">
								<div class="card-header panel-heading no-border bg-primary" style="padding: 10px; background: blue;"><h4 class="text-white"><i class="fa fa-file-text"></i> Cari Riwayat Pasien</h4></div>
								<div class="col-12">
									<div class="form-group mt-2">
										<h5>Pilih Tanggal</h5>
									<?php
					                echo $this->instrument->htmlSelectFromArray($driwayat, 'name="no_rawat" id="no_rawat" style="width:100%;" class="form-control search2"', true);
					                ?>
									</div>
									<div class="form-group">
										<input type="hidden" name="no_rkm_medis" id="no_rkm_medis" value="<?php echo $no_rkm_medis; ?>">
									</div>
								</div>
							</div>
						</div>

						<div class="col-12">
							<div class="card mb-2">
								<div class="card-header panel-heading no-border bg-primary" style="margin: 0px; padding: 0px;">
									<button id="myelement" class="button bg-primary" style="margin: 0px;"><h4 class="text-dark"><i class="fa fa-file-text"></i> Data Riwayat Pasien</h4></button>
									</div>

									<div id="another-element">
										<div class="row show_history" style="margin: 5px 0px; padding: 0px;">
											<div class="col-12">
												<div class="tabs-page">
													<div class="tabs b-shadow">
														<div class="nav nav-tabs justify-content-center" id="nav-tab" role="tablist">
															<a href="#nav-tabs1" class="nav-item nav-link active" id="nav-tabs1-tab" data-toggle="tab" role="tab" aria-controls="nav-tabs1" aria-selected="true">Poliklinik</a>
															<a href="#nav-tabs2" class="nav-item nav-link" id="nav-tabs2-tab" data-toggle="tab" role="tab" aria-controls="nav-tabs2" aria-selected="false">Rawatinap</a>
														</div>
													</div>
													<div class="tab-content b-shadow" id="nav-tabContent">
														<div class="tab-pane fade show active" id="nav-tabs1" role="tabpanel" aria-labelledby="nav-tabs1-tab">
															<span class="text-center p-2"><p class="text-justify"><h5>Untuk menampilkan data riwayat silahkan pilih tanggal yang tersedia pada formulir cari riwayat pasien.</h5></p></span>
														</div>

														<div class="tab-pane fade show" id="nav-tabs2" role="tabpanel" aria-labelledby="nav-tabs2-tab">
															<span class="text-center p-2"><p class="text-justify"><h5>Untuk menampilkan data riwayat silahkan pilih tanggal yang tersedia pada formulir cari riwayat pasien.</h5></p></span>
														</div>
													</div>
													
												</div>
											</div>
										</div>
									</div>
									
								</div>
							</div>
						</div>
					</div>

					<!-- <div class="col-12">
						<div class="card">
							<div class="card-header panel-heading no-border bg-primary" style=" padding: 10px; background: blue;"><h4 class="text-white"><i class="fa fa-user-md"></i> Formulir Tindakan Pasien</h4></div> -->
							<div class="col-12">
								<div class="row pb-2">
									<div class="col-12">
										<div class="card-header panel-heading no-border bg-primary" style=" padding: 7px;"><h4 class="text-white"><i class="fa fa-server"></i> Tindakan Diagnosa Pasien</h4></div>
										<div class="row">
											<div class="col-12">
												<div class="border bg-light p-2">
													<form method="post" id="formsimpan_diagnosa" action="#">
													<div class="showdiagnosa">
													<?php  if($diagnosa){ ?>
													<?php  
													echo '<table class="table table-striped table-responsive-md">';
														echo '<thead>';
															echo '<tr>';
																echo '<td>No</td>';
																echo '<td>No Rawat</td>';
																echo '<td>Kode Diagnosa</td>';
																echo '<td>Nama Diagnosa</td>';
																echo '<td>Prioritas</td>';
																echo '<td>Aksi</td>';
															echo '</tr>';
														echo '</thead>';
														echo '<tbody class="tabledianosa1">';
															$n = 0;
															foreach ($diagnosa as $k => $v)
															{
																$n++;
																echo '<input type="hidden" name="kd_penyakit[]" value="'.$v->kd_penyakit.'">';
																echo '<tr id="'.$v->kd_penyakit.'" data-kd_penyakit="'.$v->kd_penyakit.'" data-no_rawat="'.$v->no_rawat.'">';
																	echo '<td>'.$n.'</td>';
																	echo '<td>'.$v->no_rawat.'</td>';
																	echo '<td>'.$v->kd_penyakit.'</td>';
																	echo '<td>'.$v->nm_penyakit.'</td>';
																	echo '<td>'.$v->prioritas.'</td>';
																	echo '<td class="delete_diagnosa" style="cursor:pointer;"><button type="button" class="btn btn-danger btn-sm"> <i class="fa fa-delete"></i>Hapus</button></td>';
																echo '</tr>';
															}
														echo '</tbody>';
													echo '</table>';
													echo '<input type="hidden" name="diagnosa_order_list" id="diagnosa_order_list" />';
													?>
													<?php } ?>
													</div>
													<div class="form-group">
														<label><h5>Cari Data Diagnosis</h5></label>
														<input type="text" id="autocomplete" class="form-control" style="border-radius:0px;" placeholder="Pencarian minimal 3 karakter" />
													</div>	
													
													<table class="table table-striped table-responsive-md" id="my-list"><tbody></tbody></table>
													<button type="button" id="simpandiagnosa" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Simpan</button>
													</form>
												</div>
											</div>
										</div>
									</div>

									<div class="col-12">
										<div class="card-header panel-heading no-border bg-primary" style=" padding: 7px;"><h4 class="text-white"><i class="fa fa-medkit"></i> Tindakan Resep Pasien</h4></div>
										<div class="row">
											<div class="col-12" id="form_obat">
												<div class="border bg-light p-2">
													<div id="biling_pasien"></div>

													<form method="post" id="formsimpan_obat" action="#">
													<div class="row">
														<div class="col-6">
															<input type="button" style="cursor:pointer;" class="del btn btn-danger btn-sm" value="Delete Row">
														</div>
														<div class="col-6">
															<input type="button" class="add-row btn btn-primary btn-sm" value="Tambah Data">
														</div>
													</div>

													<div class="showresepobat">
													<?php if($resepobat) { ?>
													<table class="table table-striped table-responsive-md">
													<thead>
													<tr>
														<td width="10">No</td>
														<td>Nama Obat</td>
														<td>Jumlah</td>
														<td>Aturan Pakai</td>
														<td width="50">Aksi</td>
													</tr>
													</thead>
													<tbody>
													<?php 
													$nr =1; 
													foreach ($resepobat as $k => $v) {
														echo '<tr data-no_rawat="'.$v->no_rawat.'" data-kode_brng="'.$v->kode_brng.'" data-no_resep="'.$v->no_resep.'">';
															echo '<input type="hidden" name="kode_obat[]" value="'.$v->kode_brng.'">';
															echo '<input type="hidden" name="jlm_obat[]" value="'.$v->jml.'">';
															echo '<input type="hidden" name="aturan_pakai[]" value="'.$v->aturan_pakai.'">';
															echo '<td>'.$nr.'</td>';
															echo '<td>'.$v->nama_brng.'</td>';
															echo '<td>'.$v->jml.'</td>';
															echo '<td>'.$v->aturan_pakai.'</td>';
															echo '<td style="cursor:pointer;" class="deleteResepObat"><button type="button" class="btn btn-danger btn-sm"> Hapus</button></td>';
														echo '</tr>';
														$nr++;
													}
													?>
													</tbody> 
													</table>
													<?php } ?>
													</div>

													<table class="table table-bordered table-striped tblInputO">
														<thead>
															<tr>
																<th>Check</th>
																<th>Nama Obat</th>
																<th>Jumlah Obat</th>
																<!-- <th>Aturan Pakai</th> -->
															</tr>
														</thead>
														<tbody id="tb-resepobat">
															
														</tbody>
													</table>
													<!-- <button type="button" id="simpanobat" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Simpan</button>
													</form> -->
												</div>
											</div>
										</div>
									</div>

									<div class="col-12">
										<div class="card-header panel-heading no-border bg-primary" style=" padding: 7px;"><h4 class="text-white"><i class="fa fa-medkit"></i> Permintaan Laboratorium</h4></div>
										<div class="row">
											<div class="col-12">
												<div class="border bg-light p-2">
													<form method="post" id="formsimpan_laboratorium" action="#">
													<div class="showlabs">
													<?php if($reslabs) {?>
													<?php  
													echo '<table class="table table-striped table-responsive-md">';
													echo '<thead>';
														echo '<tr>';
															echo '<td width="10">No</td>';
															echo '<td>Nama Permintaan Lab</td>';
															echo '<td width="50">Aksi</td>';
														echo '</tr>';
													echo '</thead>';
													echo '<tbody class="permintaan_lab">';
													$nl = 1;
													foreach ($reslabs as $k => $v) 
													{
														echo '<tr data-no_rawat="'.$v->no_rawat.'" data-kd_jenis_prw="'.$v->kd_jenis_prw.'" data-noorder="'.$v->noorder.'">';
															echo '<input type="hidden" name="kd_jenis_prw[]" value="'.$v->kd_jenis_prw.'">';
															echo '<td>'.$nl.'</td>';
															echo '<td>'.$v->nm_perawatan.'</td>';
															echo '<td style="cursor:pointer;" class="deletelabs"><button type="button" class="btn btn-danger btn-sm"> Hapus</button></td>';
														echo '</tr>';
														$nl++;
													}
													echo '</tbody>';
													echo '</table>';
													?>
													<?php } ?>
													</div>

													<div class="form-group">
														<label><h5>Cari Data Lab</h5></label>
														<input type="text" id="laboratorium" class="form-control" style="border-radius:0px;" placeholder="Pencarian minimal 3 karakter" />
													</div>	

													<table class="table table-striped table-responsive-md" id="labs"><tbody></tbody></table>
													<button type="button" id="simpanlabs" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Simpan</button>
												</form>
												</div>
											</div>
										</div>
									</div>

									<div class="col-12">
										<div class="card-header panel-heading no-border bg-primary" style=" padding: 7px;"><h4 class="text-white"><i class="fa fa-medkit"></i> Permintaan Radiologi</h4></div>
										<div class="row">
											<div class="col-12">
												<div class="border bg-light p-2">
												<form method="post" id="formsimpan_radiologi" action="#">
													<div class="showrads">
													<?php if($resrads) { ?>
													<?php  
													echo '<table class="table table-striped table-responsive-md">';
														echo '<thead>';
															echo '<tr>';
															echo '<td width="10">No</td>';
															echo '<td>Nama Permintaan Radiologi</td>';
															echo '<td width="50">Aksi</td>';
															echo '</tr>';
														echo '</thead>';
														echo '<tbody class="permintaan_lab">';
															$nr = 1;
														foreach ($resrads as $k => $v) {
															echo '<tr data-no_rawat="'.$v->no_rawat.'" data-kd_jenis_prw="'.$v->kd_jenis_prw.'" data-noorder="'.$v->noorder.'">';
																echo '<input type="hidden" name="kd_jenis_prw[]" value="'.$v->kd_jenis_prw.'">';
																echo '<td>'.$nr.'</td>';
																echo '<td>'.$v->nm_perawatan.'</td>';
																echo '<td style="cursor:pointer;" class="deleterads"><button type="button" class="btn btn-danger btn-sm"> Hapus</button></td>';
															echo '</tr>';
															$nr++;
														}
														echo '</tbody>';
													echo '</table>';
													?>
													<?php } ?>
													</div>
													<div class="form-group">
														<label><h5>Cari Tindakan Radiologi</h5></label>
														<input type="text" id="radiologi" class="form-control" style="border-radius:0px;" placeholder="Pencarian minimal 3 karakter" />
													</div>	

													<table class="table table-striped table-responsive-md" id="rads"><tbody></tbody></table>

													<button type="button" id="simpanrads" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Simpan</button>
												</form>
												</div>
											</div>
										</div>
									</div>

									<!-- <div class="col-12 mt-2">
										<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
									</div> -->
								</div>
							</div>
						<!-- </div>
					</div> -->
				</div>
			</div>
		</div>
	</div>
	<!-- end tabs -->
	<div class="content" style="display: none;" id="modalUmum">
	<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModal2">
	    <div class="modal-dialog" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title">Batal Booking</h5>
	                <button class="close" data-dismiss="modal" aria-label="close">
	                    <span aria-hidden="true"><i class="fa fa-close"></i></span>
	                </button>
	            </div>
	            <div class="modal-body">
	            	<p id="msg"></p>		
	            </div>
	            <div class="modal-footer">
	                <button type="button" id="tutups" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
	                <a href="<?php echo base_url('home/view');?>" type="button" id="sukses" class="btn btn-secondary" style="display: none;">Kembali</a>
	            </div>
	        </div>
	    </div>
	</div>
</div>
<?php $this->site->get_template('inc/footer.html'); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.js" type="text/javascript"></script>
<script type="text/javascript">
	var link = '<?php echo base_url($this->uri->segment(1));?>';
	var NoRawat = '<?php echo $noRawat;?>';
	var NoRM = '<?php echo $no_rkm_medis;?>';
	var TglPerawatan = '<?php echo date('Y-m-d');?>';
	var status_poli = '<?php echo $dtpasien[0]->status_poli;?>';
	var kd_poli = '<?php echo $dtpasien[0]->kd_poli;?>';
	var kd_dokter = '<?php echo $this->session->userdata('id_user');?>';

	$(document).ready(function(){

		$('.search2').select2();

		$( "#myelement" ).click(function() {     
		    if($('#another-element:visible').length)
		    {
		   		$('#another-element').hide(1000);
		    }
		    else
		    {
		    	$('#another-element').show(1000);
		    }       
		});

		$('#no_rawat').change(function(){
			var no_rawat 		= $(this).val();
			var no_rkm_medis 	= $('#no_rkm_medis').val();
			if(no_rawat)
			{
				$.ajax({
					type	: 'post',
					url 	: link+'/getHistory',
					data 	: 'no_rawat='+no_rawat+'&no_rkm_medis='+no_rkm_medis,
					success : function(res)
					{
						var data_view = JSON.parse(res);
						$('#nav-tabs1').html(data_view.poliklinik);
						$('#nav-tabs2').html(data_view.rawatinap);
					}
				});
			}
			else
			{
				$('#nav-tabs1').html('<span class="text-center p-2"><p class="text-justify"><h5>Data tidak ditemukan.</h5></p></span>');
				$('#nav-tabs2').html('<span class="text-center p-2"><p class="text-justify"><h5>Data tidak ditemukan.</h5></p></span>');
			}
		});

		$("#autocomplete").autocomplete({
            source: "<?php echo site_url($this->uri->segment(1).'/getDiagnosa');?>",
            minLength: 3,
            focus: function( event, ui ) {
            	event.preventDefault();
            	$(this).val(ui.item.label);
            },
            select: function(event, ui) {
                //console.log(event);
                $(this).val('');//.blur();
                event.preventDefault(); // cancel default behavior which updates input field
                
                $("#my-list").append('\n\
                    <tr id="'+ui.item.keys+'">\n\
                        <td class="tr_mod"><input type="hidden" name="kd_penyakit[]" value='+ui.item.id+'><h6>'+ui.item.value+'</h6></td>\n\
                        <td style="cursor:pointer;" class="delete" data-id="'+ui.item.keys+'"><h6><i class="fa fa-delete"> Hapus</i></h6></td>\n\
                    </tr>');
            }
        });

        $('#my-list').on('click', '.delete', function(){
            var tr = $(this).data('id');
            $('#'+tr).remove();
        });

        $("#laboratorium").autocomplete({
            source: "<?php echo site_url($this->uri->segment(1).'/getLaboratorium');?>",
            minLength: 3,
            focus: function( event, ui ) {
            	event.preventDefault();
            	$(this).val(ui.item.label);
            },
            select: function(event, ui) {
                //console.log(event);
                $(this).val('');//.blur();
                event.preventDefault(); // cancel default behavior which updates input field
                
                $("#labs").append('\n\
                    <tr>\n\
                        <td class="tr_mod"><input type="hidden" name="kd_jenis_prw_labs[]" value='+ui.item.id+'><h6>'+ui.item.value+'</h6></td>\n\
                        <td style="cursor:pointer;" class="delete" data-id="'+ui.item.id+'"><h6><i class="fa fa-delete"> Hapus</i></h6></td>\n\
                    </tr>');
            }
        });

        $('#labs').on('click', '.delete', function(){
            var tr = $(this).data('id');
            $('#'+tr).remove();
        });

        $("#radiologi").autocomplete({
            source: "<?php echo site_url($this->uri->segment(1).'/getRadiologi');?>",
            minLength: 3,
            focus: function( event, ui ) {
            	event.preventDefault();
            	$(this).val(ui.item.label);
            },
            select: function(event, ui) {
                //console.log(event);
                $(this).val('');//.blur();
                event.preventDefault(); // cancel default behavior which updates input field
                
                $("#rads").append('\n\
                    <tr>\n\
                        <td class="tr_mod"><input type="hidden" name="kd_jenis_prw_rad[]" value='+ui.item.id+'><h6>'+ui.item.value+'</h6></td>\n\
                        <td style="cursor:pointer;" class="delete" data-id="'+ui.item.id+'"><h6><i class="fa fa-delete"> Hapus</i></h6></td>\n\
                    </tr>');
            }
        });

        $('#labs').on('click', '.delete', function(){
            var tr = $(this).data('id');
            $('#'+tr).remove();
        });

        $('.add-row').click(function(){
        	var datas = '<tr class="addtr">\n\
        				<td width="50"><input type="checkbox" class="form-control" style="width:20px;" name="record"></td>\n\
						<td>\n\
						<select name="kode_obat[]" class="kd_obat form-control" style="width:100%"></select>\n\
						</td>\n\
						<td width="100">\n\
						<input type="number" name="jlm_obat[]" list="jumlahobat" class="form-control jlm_obat">\n\
						</td>\n\
						<td width="200"><?php echo  $this->instrument->htmlSelectFromArray($aturan_pakai, 'name="aturan_pakai[]" id="kd_poli" class="form-control"', true);?>\n\
						</td>\n\
						</tr>';
			$('.tblInputO tbody').append(datas);

			$('.tblInputO tbody .kd_obat').select2({
				placeholder: 'Pilih obat',
				ajax: 
					{
						url: '<?php echo site_url($this->uri->segment(1).'/getObat');?>',
						dataType: 'json',
						delay: 250,
						processResults: function (data) 
						{
							return {
								results: data
							};
						},
						cache: true
					},
				templateResult: formatData,
				minimumInputLength: 3
	        });

	        $('.tblInputO tbody .jlm_obat').change(function(){
	        	var jlm_obat = $('[name="jlm_obat[]"]').serialize();
	        	var kode_obat = $('[name="kode_obat[]"]').serialize();
	        	$.ajax({
	        		type : 'post',
	        		url : link+'/getHargaObat',
	        		data : jlm_obat+'&'+kode_obat,
	        		success : function(res)
	        		{
	        			$('#biling_pasien').html(res);
	        		}
	        	});
	        });

	        $(".del").click(function(){
		        $(".tblInputO tbody").find('input[name="record"]').each(function(){
	            	if($(this).is(":checked")){
	            		$(this).parents("tr").remove();
	            		var jlm_obat = $('[name="jlm_obat[]"]').serialize();
			        	var kode_obat = $('[name="kode_obat[]"]').serialize();
			        	$.ajax({
			        		type : 'post',
			        		url : link+'/delHargaObat',
			        		data : jlm_obat+'&'+kode_obat,
			        		success : function(res)
			        		{
			        			$('#biling_pasien').html(res);
			        		}
			        	});
	                }
	            });
		    }); 
        });

        // Simpan data diagnosa
        $('#formsimpan_diagnosa').on('click','#simpandiagnosa', function(){
        	var diagnosa = $('[name="kd_penyakit[]"]').serialize();
        	$("#my-list tr").remove();
        	if(diagnosa!='')
        	{
        		$.ajax({
	        		type : 'post',
	        		url : link+'/savediagnosa',
	        		data : diagnosa+'&no_rawat='+NoRawat+'&status_penyakit='+status_poli,
	        		success : function(res)
	        		{
	        			var r = JSON.parse(res);
	        			if(r.error=='')
	        			{
	        				$('#formsimpan_diagnosa .showdiagnosa').html(r.view);
	        				delete_diagnosa();
	        				sortTableDiag2();
	        			}
	        			else
	        			{
	        				$('#formsimpan_diagnosa .showdiagnosa').html(r.view);
	        			}
	        		}
	        	});
        	}
        	else
        	{
        		alert('Data tidak boleh kosong.');
        	}
        });

        $( ".tabledianosa1" ).sortable({
	        delay: 150,
	        stop: function() {
	            var selectedData = new Array();
	            $('.tabledianosa1>tr').each(function() {
	                selectedData.push($(this).attr("id"));
	            });
	            updateOrder(selectedData,NoRawat);
	        }
	    });
	    // End

	    // Simpan data obat
	    $('#formsimpan_obat').on('click','#simpanobat', function(){
	    	var kode_obat = $('[name="kode_obat[]"]').serialize();
	    	var jlm_obat = $('[name="jlm_obat[]"]').serialize();
	    	var aturan_pakai = $('[name="aturan_pakai[]"]').serialize();
	    	$("#tb-resepobat tr").remove();
	    	if(kode_obat!='' && jlm_obat!='' && aturan_pakai!='')
	    	{
	    		$.ajax({
	        		type : 'post',
	        		url : link+'/saveResepObat',
	        		data : kode_obat+'&'+jlm_obat+'&'+aturan_pakai+'&no_rawat='+NoRawat+'&tgl_perawatan='+TglPerawatan+'&no_rkm_medis='+NoRM+'&kd_poli='+kd_poli+'&kd_dokter='+kd_dokter,
	        		success : function(res)
	        		{
	        			var r = JSON.parse(res);
	        			if(r.error=='')
	        			{
	        				$('#formsimpan_obat .showresepobat').html(r.view);
	        				//delete_resepobat();
	        			}
	        			else
	        			{
	        				$('#formsimpan_obat .showresepobat').html(r.view);
	        			}
	        		}
	        	});
	    	}	    	
	    });

	    // Simpan data Laboratorium
        $('#formsimpan_laboratorium').on('click','#simpanlabs', function(){
        	var labs = $('[name="kd_jenis_prw_labs[]"]').serialize();
        	$("#labs tr").remove();
        	if(labs!='')
        	{
        		$.ajax({
	        		type : 'post',
	        		url : link+'/savelabs',
	        		data : labs+'&no_rawat='+NoRawat+'&kd_dokter='+kd_dokter+'&tgl='+TglPerawatan,
	        		success : function(res)
	        		{
	        			//alert(res);
	        			var r = JSON.parse(res);
	        			if(r.error=='')
	        			{
	        				$('#formsimpan_laboratorium .showlabs').html(r.view);
	        				delete_labs();
	        			}
	        			else
	        			{
	        				$('#formsimpan_laboratorium .showlabs').html(r.view);
	        			}
	        		}
	        	});
        	}
        	else
        	{
        		alert('Data tidak boleh kosong.');
        	}
        });

        // Simpan data Radiologi
        $('#formsimpan_radiologi').on('click','#simpanrads', function(){
        	var rads = $('[name="kd_jenis_prw_rad[]"]').serialize();
        	$("#rads tr").remove();
        	if(rads!='')
        	{
        		$.ajax({
	        		type : 'post',
	        		url : link+'/saverads',
	        		data : rads+'&no_rawat='+NoRawat+'&kd_dokter='+kd_dokter+'&tgl='+TglPerawatan,
	        		success : function(res)
	        		{
	        			//alert(res);
	        			var r = JSON.parse(res);
	        			if(r.error=='')
	        			{
	        				$('#formsimpan_radiologi .showrads').html(r.view);
	        				delete_rads();
	        			}
	        			else
	        			{
	        				$('#formsimpan_laboratorium .showrads').html(r.view);
	        			}
	        		}
	        	});
        	}
        	else
        	{
        		alert('Data tidak boleh kosong.');
        	}
        });

        delete_diagnosa();
        delete_resepobat();
        delete_labs();
        delete_rads();
	});

	$(function(){
	    $( "#my-list" ).sortable();
	    $( "#my-list" ).disableSelection();
  	});

	function formatData (data) {
		var ret;
		if(data.stok <= 0)
    	{
    		ret = '<span class="text-primary">'+ data.text+'</span>';
    	}
    	else
    	{
    		ret = '<span>'+ data.text +'</span>';
    	}
        var $data = $(
        	ret
        );
        return $data;
    };

	function filterTable(event) {
	    var filter = event.target.value.toUpperCase();
	    var rows = document.querySelector("#myTable tbody").rows;
	    
	    for (var i = 0; i < rows.length; i++) {
	        var firstCol = rows[i].cells[0].textContent.toUpperCase();
	        var secondCol = rows[i].cells[1].textContent.toUpperCase();
	        if (firstCol.indexOf(filter) > -1 || secondCol.indexOf(filter) > -1) {
	            rows[i].style.display = "";
	        } else {
	            rows[i].style.display = "none";
	        }      
	    }
	}

	function updateOrder(data,norawat) {
        $.ajax({
            url:"<?php echo base_url($this->uri->segment(1));?>/updateDiagnosa",
            type:'post',
            data:'kd_penyakit='+data+'&no_rawat='+norawat,
            success:function(res){
            	var r = JSON.parse(res);
            	if(r.error=='')
            	{
            		//alert('perubahan Anda berhasil disimpan');
            		$('#formsimpan_diagnosa .showdiagnosa').html(r.view);
            		delete_diagnosa();
            		sortTableDiag2();
            	}
            }
        })
    }

    function delete_diagnosa()
    {
    	$('.delete_diagnosa').click(function(){
	    	var tr = $(this).parent();
	    	if(tr.data('kd_penyakit')!='')
	    	{
	    		$.ajax({
	    			type : 'post',
	    			url : link+'/deleteDiagnosa',
	    			data : 'kd_penyakit='+tr.data('kd_penyakit')+'&no_rawat='+tr.data('no_rawat'),
	    			success : function(res)
	    			{
	    				var r = JSON.parse(res);
	    				$('#formsimpan_diagnosa .showdiagnosa').html(r.view);
        				delete_diagnosa();
        				$( "#formsimpan_diagnosa .showdiagnosa .tabledianosa2" ).sortable({
					        delay: 150,
					        stop: function() {
					            var selectedData = new Array();
					            $('.tabledianosa2>tr').each(function() {
					                selectedData.push($(this).attr("id"));
					            });
					            updateOrder(selectedData,NoRawat);
					        }
					    });
	    			}
	    		});
	    	}
	    });
    }

    function delete_resepobat()
    {
    	$('.deleteResepObat').click(function(){
	    	var tr = $(this).parent();
	    	if(tr.data('no_rawat')!='' && tr.data('kode_brng')!='')
	    	{
	    		$.ajax({
	    			type : 'post',
	    			url : link+'/deleteResepobat',
	    			data : 'no_rawat='+tr.data('no_rawat')+'&kode_brng='+tr.data('kode_brng')+'&no_resep='+tr.data('no_resep'),
	    			success : function(res)
	    			{
	    				var r = JSON.parse(res);
	    				$('#formsimpan_obat .showresepobat').html(r.view);
	    				delete_resepobat();
	    			}
	    		});
	    	}
	    });
    }

    function delete_labs()
    {
    	$('.deletelabs').click(function(){
	    	var tr = $(this).parent();
	    	if(tr.data('kd_jenis_prw')!='')
	    	{
	    		$.ajax({
	    			type : 'post',
	    			url : link+'/deletelabs',
	    			data : 'kd_jenis_prw='+tr.data('kd_jenis_prw')+'&no_rawat='+tr.data('no_rawat')+'&noorder='+tr.data('noorder'),
	    			success : function(res)
	    			{
	    				var r = JSON.parse(res);
	    				$('#formsimpan_laboratorium .showlabs').html(r.view);
	    				delete_labs();
	    			}
	    		});
	    	}
	    	else
	    	{
	    		alert('Mohon maaf anda tidak bisa menghapus.')
	    	}
	    });
    }

    function delete_rads()
    {
    	$('.deleterads').click(function(){
	    	var tr = $(this).parent();
	    	if(tr.data('kd_jenis_prw')!='')
	    	{
	    		$.ajax({
	    			type : 'post',
	    			url : link+'/deleterads',
	    			data : 'kd_jenis_prw='+tr.data('kd_jenis_prw')+'&no_rawat='+tr.data('no_rawat')+'&noorder='+tr.data('noorder'),
	    			success : function(res)
	    			{
	    				var r = JSON.parse(res);
	    				$('#formsimpan_radiologi .showrads').html(r.view);
	    				delete_rads();
	    			}
	    		});
	    	}
	    	else
	    	{
	    		alert('Mohon maaf anda tidak bisa menghapus.')
	    	}
	    });
    }

    function sortTableDiag2()
    {
    	$( ".tabledianosa2" ).sortable({
	        delay: 150,
	        stop: function() {
	            var selectedData = new Array();
	            $('.tabledianosa2>tr').each(function() {
	                selectedData.push($(this).attr("id"));
	            });
	            updateOrder(selectedData,NoRawat);
	        }
	    });
    }

    window.onbeforeunload = function() {
	    return 'sads';
	}

	function saveFormData() {
		var data = $('#tb-resepobat :input').serialize();
	    console.log('saved');
	}

	document.querySelector('#myInput').addEventListener('keyup', filterTable, false);

	var kedipan = 750; 
	var dumet = setInterval(function () {
	    var ele = document.getElementById('ralert');
	    ele.style.visibility = (ele.style.visibility == 'hidden' ? '' : 'hidden');
	}, kedipan);
</script>
<?php $this->site->get_template('inc/endhtml.html'); ?>