<!-- <div class="col-12" style="margin-top: 10px;">
	<div class="card">
			<div class="card-header panel-heading no-border bg-primary" style="padding: 10px; background: blue;"><h4 class="text-white">Data Pemeriksaan Perawat</h4></div> -->
			<div class="border bg-light p-2" id="datapemeriksaan">
				<table class="" style="padding: 0px; margin: 0px; width:100%;">
					<tr style="padding: 0px; margin: 0px;">
						<td width="200">&nbsp; - Suhu Tubuh</td>
						<td width="10">:</td>
						<td><b><?php echo (@$pemeriksaan[0]->suhu_tubuh) ? $pemeriksaan[0]->suhu_tubuh.'&deg;C' : ''; ?></b></td>
						<td width="20" style="">&nbsp; -</td>
						<td width="100">Tensi</td>
						<td width="10">:</td>
						<td><b><?php echo (@$pemeriksaan[0]->tensi) ? $pemeriksaan[0]->tensi.' mm Hg' : ''; ?></b></td>
					</tr>
					<tr>
						<td>&nbsp; - Nadi</td>
						<td>:</td>
						<td><b><?php echo (@$pemeriksaan[0]->nadi) ? $pemeriksaan[0]->nadi.'/Menit' : ''; ?></b></td>
						<td>&nbsp; -</td>
						<td>Respirasi</td>
						<td>:</td>
						<td><b><?php echo (@$pemeriksaan[0]->respirasi) ? $pemeriksaan[0]->respirasi : ''; ?></b></td>
					</tr>
					<tr>
						<td>&nbsp; - Tinggi</td>
						<td>:</td>
						<td><b><?php echo (@$pemeriksaan[0]->tinggi) ? $pemeriksaan[0]->tinggi : ''; ?></b></td>
						<td>&nbsp; -</td>
						<td>Berat</td>
						<td>:</td>
						<td><b><?php echo (@$pemeriksaan[0]->berat) ? $pemeriksaan[0]->berat .'Kg' : ''; ?></b></td>
					</tr>
					<tr>
						<td>&nbsp; - gcs</td>
						<td>:</td>
						<td><?php (@$pemeriksaan[0]->gcs) ? $pemeriksaan[0]->gcs : ''; ?></b></td>
						<td>&nbsp; -</td>
						<td>Imun Ke</td>
						<td>:</td>
						<td><b><?php echo (@$pemeriksaan[0]->imun_ke) ? $pemeriksaan[0]->imun_ke : ''; ?></b></td>
					</tr>
					<tr>
						<td>&nbsp; - Rtl</td>
						<td>:</td>
						<td><b><?php echo (@$pemeriksaan[0]->rtl) ? $pemeriksaan[0]->rtl : ''; ?></b></td>
						<td>&nbsp; -</td>
						<td>Alergi</td>
						<td>:</td>
						<td><?php echo (@$pemeriksaan[0]->alergi) ? @$pemeriksaan[0]->alergi : ''; ?></td>
					</tr>
					<tr>
						<td>&nbsp; - Keluhan</td>
						<td>:</td>
						<td><b><?php echo isset($pemeriksaan[0]->keluhan) ? $pemeriksaan[0]->keluhan : ''; ?></b></td>
						<td>&nbsp; -</td>
						<td>Pemeriksaan</td>
						<td>:</td>
						<td><b><?php echo isset($pemeriksaan[0]->pemeriksaan) ? $pemeriksaan[0]->pemeriksaan : ''; ?></b></td>
					</tr>
					<tr>
						<td class="p-2" colspan="7">
							<button class="btn btn-primary btn-sm edit-pemeriksaan">Edit</button>
						</td>
					</tr>
				</table>
			</div>
<!-- 	</div>
</div> -->