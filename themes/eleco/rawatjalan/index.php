<?php $this->site->get_template('inc/navbar-page.html'); ?>
<div class="table-page segments-page">
		<div class="container">
			<div class="wrap-title">
				<h5><?php echo strtoupper($titled); ?></h5>
			</div>

			<div class="row">
				<div class="col-12">
					<form method="post" action="<?php echo base_url($this->uri->segment(1).'/index');?>" >
						<div class="card">
							<div class="card-header panel-heading no-border bg-primary" style="padding: 10px; background: blue;"><h4 class="text-white"><i class="fa fa-calendar"></i> Fasilitas pencarian pasien berdasarkan tanggal.</h4></div>
							<div class="content no-mb p-2">
								<div class="row">
									<div class="col-md-5">
										<div class="form-group">
											<input type="date" name="search" class="form-control" value="<?php echo $date;?>">
										</div>
									</div>
									<div class="col-md-5">
										<div class="form-group">
											<?php
									            echo $this->instrument->htmlSelectFromArray($kd_poli, 'name="kd_polif" id="kd_polif" style="width:100%;" class="form-control search2"',true,$defaultkdpoli);
									        ?>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<button type="submit" class="btn btn-primary col-12" id="proses"><i class="fa fa-search"></i> Proses Cari</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>

			<div class="wrap-content b-shadow">
				<div class="card">
						<div class="card-header panel-heading no-border bg-primary mb-3" style="padding: 10px; background: blue;"><h4 class="text-white"><i class="fa fa-calendar"></i> Pasien terdaftar tanggal <?php echo date('d  M Y', strtotime($date));?></h4></div>
					<table class="table table-striped table-responsive display" style="width:100%" id="example">
						<thead>
							<tr>
								<th>Nomor Antrian</th>
								<th>No Rm</th>
								<th>Nama Pasien</th>
								<th>JK</th>
								<th>Tanggal Lahir</th>
								<th>Usia</th>
								<th>Poliklinik</th>
								<th>Dokter</th>
								<th>Cara Bayar</th>
								<th>Tanggal Registrasi</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							foreach ($row as $k => $v) { 
							?>
							<tr style="cursor:pointer;" <?php //echo $this->instrument->showValRec($v); ?>>
								<td class="tr_mod"><a href="<?php echo base_url($this->uri->segment(1).'/kajipasien/'.str_replace('/', '-', $v->no_rawat)).'/'.$v->tgl_registrasi.'/'.$v->no_rkm_medis;?>" style="color:blue;"><?php echo $v->no_reg; ?></a></td>
								<td class="tr_mod"><a href="<?php echo base_url($this->uri->segment(1).'/kajipasien/'.str_replace('/', '-', $v->no_rawat)).'/'.$v->tgl_registrasi.'/'.$v->no_rkm_medis;?>" style="color:blue;"><?php echo $v->no_rkm_medis; ?></a></td>
								<td class="tr_mod"><?php echo $v->nm_pasien; ?></td>
								<td class="tr_mod"><?php echo ($v->jk=='L') ? 'Laki-laki' : 'Perempuan'; ?></td>
								<td class="tr_mod"><?php echo $v->tgl_lahir; ?></td>
								<td class="tr_mod"><?php echo $v->umurdaftar; ?></td>
								<td class="tr_mod"><?php echo $v->nm_poli; ?></td>
								<td class="tr_mod"><?php echo $this->session->userdata('nm_dokter'); ?></td>
								<td class="tr_mod"><?php echo $v->png_jawab; ?></td>
								<td class="tr_mod"><?php echo $v->tgl_registrasi; ?></td>
								<td class="tr_mod"><?php echo $v->stts; ?></td>
								
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
</div>
<div class="content" style="display: none;" id="modalUmum">
	<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModal2">
	    <div class="modal-dialog" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title">Batal Booking</h5>
	                <button class="close" data-dismiss="modal" aria-label="close">
	                    <span aria-hidden="true"><i class="fa fa-close"></i></span>
	                </button>
	            </div>
	            <div class="modal-body">
	            	<p id="msg"></p>		
	            </div>
	            <div class="modal-footer">
	                <button type="button" id="tutups" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
	                <a href="<?php echo base_url('home/view');?>" type="button" id="sukses" class="btn btn-secondary" style="display: none;">Kembali</a>
	            </div>
	        </div>
	    </div>
	</div>
</div>
<?php $this->site->get_template('inc/footer.html'); ?>
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable();
});
</script>
<?php $this->site->get_template('inc/endhtml.html'); ?>