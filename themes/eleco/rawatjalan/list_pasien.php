<div class="col-4">
	<div class="input-group">      
        <input type="text" class="form-control form-control-small bg-light" style="border-radius: 0; height: 30px;" id="myInput" placeholder="Pencarian data antrian">
    </div>
	<div class="bg-white" style="border-left:1px solid rgba(0,0,0,.125); border-right:1px solid rgba(0,0,0,.125);border-bottom:1px solid rgba(0,0,0,.125);">
		<div class="col-md-12" style="height: 113px;  max-height: 118px; overflow-y: scroll; padding: 5px;">
			<?php  
			echo '<table id="myTable">';
				$no = 0;
				foreach ($antrian as $k => $v) {
					$no++;
					echo '<tr>';
						echo '<td '; 
						echo ($noRawat == $v->no_rawat) ? 'class="blue" ' : '';
						echo '>Antrian '.$no.'.</td>';
						echo '<td><a href="'.base_url($this->uri->segment(1).'/kajipasien/'.str_replace('/', '-', $v->no_rawat)).'/'.$v->tgl_registrasi.'/'.$v->no_rkm_medis.'" ';
						echo ($noRawat == $v->no_rawat) ? 'class="blue" ' : '';
						echo '>'.$v->no_reg.' - '. $this->instrument->cutName($v->nm_pasien) .' ('.$v->no_rkm_medis.')</a></td>';
					echo '</tr>';
				}
			echo '</table>';
			?>
		</div>
	</div>
</div>