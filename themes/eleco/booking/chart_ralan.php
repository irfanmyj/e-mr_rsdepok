<?php $this->site->get_template('inc/navbar-page.html'); ?>
<div class="table-page segments-page">
		<div class="container">
			<div class="wrap-title">
				<h5><?php echo strtoupper($title); ?></h5>
			</div>

			<div class="content">
				<div class="row">
					<div class="col-12">
						<div class="content total_daftar_bulan" id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
					</div>

					<div class="col-12"><hr></div>
					
					<div class="col-12">
						<div class="content total_daftar_bulan" id="container1" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
					</div>

					<!-- <div class="col-12"><hr></div>

					<div class="col-12">
						<div class="content total_daftar_bulan" id="container1" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
					</div> -->
				</div>

				
			</div>
		</div>
</div>
<?php $this->site->get_template('inc/footer.html'); ?>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable();
    	Highcharts.chart('container', {
		    chart: {
		        type: 'line'
		    },
		    title: {
		        text: 'JUMLAH PENGUNJUNG BULANAN PADA TAHUN <?php echo date('Y');?> BERDASARKAN CARA BAYAR'
		    },
		    subtitle: {
		        text: 'RSUD KOTA DEPOK'
		    },
		    xAxis: {
		        categories: Highcharts.getOptions().lang.shortMonths
		    },
		    yAxis: {
		        title: {
		            text: 'Jumlah Pendaftaran'
		        }
		    },
		    plotOptions: {
		        line: {
		            dataLabels: {
		                enabled: true
		            },
		            enableMouseTracking: false
		        }
		    },
		    series: [{
		        name: 'JAMKESNAS',
		        data: [<?php echo join($chart1['BPJ'], ','); ?>]
		    },{
		        name: 'BPJS KETENAGAKERJAAN',
		        data: [<?php echo join($chart1['JKK'], ','); ?>]
		    },{
		        name: 'MCU DPRD KOTA DEPOK',
		        data: [<?php echo join($chart1['MCD'], ','); ?>]
		    },{
		        name: 'NON KUOTA PBI',
		        data: [<?php echo join($chart1['NON'], ','); ?>]
		    },{
		        name: 'ORANG TERLANTAR',
		        data: [<?php echo join($chart1['OTG'], ','); ?>]
		    },{
		        name: 'RSUD KOTA DEPOK',
		        data: [<?php echo join($chart1['RSD'], ','); ?>]
		    },{
		        name: 'UMUM',
		        data: [<?php echo join($chart1['UMU'], ','); ?>]
		    }]
		});
	
	});
</script>
<?php $this->site->get_template('inc/endhtml.html'); ?>