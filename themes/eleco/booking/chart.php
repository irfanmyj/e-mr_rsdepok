<?php $this->site->get_template('inc/navbar-page.html'); ?>
<div class="table-page segments-page">
		<div class="container">
			<div class="wrap-title">
				<h5><?php echo strtoupper($title); ?></h5>
			</div>

			<div class="content">
				<div class="row">
					<div class="col-12">
						<div class="content total_tahun" id="containertahun" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
					</div>

					<div class="col-12"><hr></div>

					<div class="col-12">
						<div class="content total_daftar_bulan" id="container1" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
					</div>

					<div class="col-12"><hr></div>
					<div class="col-12">
						<div class="content total_daftar_bulan" id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
					</div>
				</div>

				
			</div>
		</div>
</div>
<?php $this->site->get_template('inc/footer.html'); ?>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable();
    Highcharts.chart('container', {
		    chart: {
		        type: 'line'
		    },
		    title: {
		        text: 'JUMLAH BOOKING BULANAN PADA TAHUN <?php echo date('Y');?> BERDASARKAN CARA BAYAR'
		    },
		    subtitle: {
		        text: 'RSUD KOTA DEPOK'
		    },
		    xAxis: {
		        categories: Highcharts.getOptions().lang.shortMonths
		    },
		    yAxis: {
		        title: {
		            text: 'Jumlah Pendaftaran Booking Pasien'
		        }
		    },
		    plotOptions: {
		        line: {
		            dataLabels: {
		                enabled: true
		            },
		            enableMouseTracking: false
		        }
		    },
		    series: [{
		        name: 'BPJS',
		        data: [<?php echo join($bpj['BPJ'], ','); ?>]
		    }, {
		        name: 'UMUM',
		        data: [<?php echo join($umu['UMU'], ','); ?>]
		    }]
		});

    Highcharts.chart('container1', {
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: 'DATA PASIEN BULANAN PADA TAHUN <?php echo date('Y');?> BERDASARKAN STATUS BOOKING'
	    },
	    subtitle: {
	        text: ''
	    },
	    xAxis: {
	        categories: Highcharts.getOptions().lang.shortMonths,
	        crosshair: true
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'JUMLAH'
	        }
	    },
	    tooltip: {
	        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	            '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
	        footerFormat: '</table>',
	        shared: true,
	        useHTML: true
	    },
	    plotOptions: {
	        column: {
	            pointPadding: 0,
	            borderWidth: 0
	        }
	    },
	    series: [{
	        name: 'Terdaftar',
	        data: [<?php echo join($terdaftar['Terdaftar'], ','); ?>]

	    }, {
	        name: 'Belum',
	        data: [<?php echo join($belum['Belum'], ','); ?>]

	    }, {
	        name: 'Batal',
	        data: [<?php echo join($batal['Batal'], ','); ?>]

	    }, {
	        name: 'Dokter Berhalangan',
	        data: [<?php echo join($dokter_berhalangan['DokterBerhalangan'], ','); ?>]

	    }]
	});

	Highcharts.chart('containertahun', {

		    title: {
		        text: 'DATA PASIEN TAHUN 2018 - <?php echo date('Y');?> BERDASARKAN STATUS BOOKING'
		    },

		    subtitle: {
		        text: ''
		    },

		    yAxis: {
		        title: {
		            text: 'Jumlah Pasien Booking'
		        }
		    },
		    legend: {
		        layout: 'vertical',
		        align: 'right',
		        verticalAlign: 'middle'
		    },

		    plotOptions: {
		        series: {
		            label: {
		                connectorAllowed: false
		            },
		            pointStart: 2018
		        }
		    },

		    series: [{
		        name: 'Belum',
		        data: [<?php echo join($chart3['Belum'], ','); ?>]
		    }, {
		        name: 'Terdaftar',
		        data: [<?php echo join($chart3['Terdaftar'], ','); ?>]
		    }, {
		        name: 'Dokter Berhalangan',
		        data: [<?php echo join($chart3['DokterBerhalangan'], ','); ?>]
		    }, {
		        name: 'Batal',
		        data: [<?php echo join($chart3['Batal'], ','); ?>]
		    }],

		    responsive: {
		        rules: [{
		            condition: {
		                maxWidth: 500
		            },
		            chartOptions: {
		                legend: {
		                    layout: 'horizontal',
		                    align: 'center',
		                    verticalAlign: 'bottom'
		                }
		            }
		        }]
		    }

		});
});
</script>
<?php $this->site->get_template('inc/endhtml.html'); ?>