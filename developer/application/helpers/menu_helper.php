<?php defined('BASEPATH') OR exit('No direct script access allowed');

	function menu_top()
	{
		try {
			$CI =& get_instance();
			/*
			$CI->config->load('menu');
			$menu_top = $CI->config->item('menu');
			*/
			$CI->load->model('Modules_model');
			$where = array('category'=>'top', 'status'=>'Y');
			$get = $CI->Modules_model->get('','*',$where,'','','sort','','')->result_array();
			$result = buildTree($get,0);
			
			/*
			$uri = array();
			$uri = $CI->uri->segment(1);
			*/

			$html  = '';
			$html .= '<ul class="nav navbar-nav">';
			foreach($result as $k => $v)
			{
				if(@$v['children'])
				{
					$html .= '<li class="nav-item dropdown pos-stc">';
						$html .= '<a class="nav-link dropdown-toggle" data-toggle="dropdown">';
						$html .= '<span class="nav-text"><i class="fa '.$v['icon'].'"></i> '.$v['name_module'].'</span>';
						$html .= '</a>';
							$html .= '<div class="dropdown-menu pull-down p-a w-full text-color text-primary-hover">';
							$html .= '<div class="row">';
							$html .= '<div class="col-md-12">';
							//$html .= '<div class="row">';
							//$html .= '<div class="col-md-12">';
							$html .= '<input type="text" class="form-control myInput" placeholder="Silahkan menggunakan form ini untuk mencari menu yang anda butuhkan.">';
							//$html .= '</div>';
							//$html .= '</div>';
							$html .= '</div>';
							foreach ($v['children'] as $kc1 => $vc1) 
							{
								if(@$vc1['children'])
								{
										$html .= '<div class="col-md-4">';
										$html .= '<div class="_800 text-u-c m-y-sm text-dark">'.$vc1['name_module'].'';
											$html .= ' <span class="label label-sm success"> ';
												$html .= count($vc1['children']);
											$html .= '</span>';
										$html .= '</div>';

										$html .= '<div class="row">';
										
										foreach ($vc1['children'] as $kc2 => $vc2) 
										{
											$html .= '<div class="col-sm-6">';
											$html .= '<ul class="nav flex-column l-h-2x myUL">';
											$html .= '<li>';
											$html .= '<a href='.base_url($vc2['link']).'>';
												$html .= '<span class=""><i class="fa fa-angle-double-right" style="color:#2a2b3c; font-size:16px;"></i> '.$vc2['name_module'].'</span>';
											$html .= '</a>';
											$html .= '</li>';
											$html .= '</ul>';
											$html .= '</div>';
										}
										
										$html .= '</div>';
									$html .= '</div>';
								}
								else
								{
									$html .= '<div class="col-sm-4">';
										$html .= '<div class="_600 text-u-c m-y-sm">'.$vc1['name_module'].'</div>';
										$html .= '<div class="row">';
											$html .= '<div class="col-sm-6">';
											$html .= '<ul class="nav flex-column l-h-2x myUL">';
												$html .= '<li>';
												$html .= '<a href="'.base_url($vc1['link']).'">';
													$html .= '<span>'.$vc1['name_module'].'</span>';
												$html .= '</a>';
												$html .= '</li>';
											$html .= '</ul>';
											$html .= '</div>';
										$html .= '</div>';
									$html .= '</div>';
								}
							}
							$html .= '</div>';
							$html .= '</div>';

					$html .= '</li>';
				}
				else
				{
					$html .= '<li class="nav-item">';
						$html .= '<a href="'.base_url($v['link']).'" class="nav-link active">';
							$html .= '<span class="nav-text"><i class="fa '.$v['icon'].'"></i> '.$v['name_module'].'</span>';
						$html .= '</a>';
					$html .= '</li>';
				}
				
			}
			$html .= '</ul>';
			return $html;
		} catch (Exception $e) {
			echo 'Error : '. $e->getMessages();
		}
	}

	function buildTree($elements=array(), $parentId = 0) {
	    $branch = array();

	    foreach ($elements as $element) {
	    	//print_r($element);
	        if ($element['parent_id'] == $parentId) {
	            $children = buildTree($elements, $element['module_id']);
	            if ($children) {
	                $element['children'] = $children;
	            }
	            $branch[] = $element;
	        }
	    }

	    return $branch;
	}

	function array_count($array = array(),$deep){
        $return = 0;
        switch($deep){
            case 1:
                count($array);
            break;
            case 2:
                foreach($array as $k1=>$v1){
                    $return += count($array[$k1]);
                }
            break;
            case 3:
                foreach($array as $k1=>$v1){
                    foreach($v1 as $k2=>$v2){
                        $return += count($array[$k1][$k2]);
                    }
                }
            break;
            case 4:
                foreach($array as $k1=>$v1){
                    foreach($v1 as $k2=>$v2){
                        foreach($v2 as $k3=>$v3){
                            $return += count($array[$k1][$k2][$k3]);
                        }
                    }
                }
            break;
            case 5:
                foreach($array as $k1=>$v1){
                    foreach($v1 as $k2=>$v2){
                        foreach($v2 as $k3=>$v3){
                            foreach($v3 as $k4=>$v4){
                                $return += count($array[$k1][$k2][$k3][$k4]);
                            }
                        }
                    }
                }
            break;
        }
        
        return $return;
    }

    function getNumberLab($data,$tgl='',$sts='')
    {
    	if($sts == 'labs')
    	{
    		$lastNumber = substr($data[0]->noorder, 0, 4);
			$get_next_number = sprintf('%04s', ($lastNumber + 1));
			$get_date = str_replace('-', '',$tgl);
			$next_no_order = 'PL'.$get_date.''.$get_next_number;
			return $next_no_order;
    	}
    	else if($sts == 'obat')
    	{
    		$lastNumber = substr($data[0]->no_resep, 0, 4);
            $next_no_resep = sprintf('%04s', ($lastNumber + 1));
            $get_date = str_replace('-', '',$tgl);
            return $get_date.$next_no_resep;
    	}
    	else if($sts == 'rad')
    	{
    		$lastNumber = substr($data[0]->noorder, 0, 4);
			$get_next_number = sprintf('%04s', ($lastNumber + 1));
			$get_date = str_replace('-', '',$tgl);
			$next_no_order = 'PR'.$get_date.''.$get_next_number;
			return $next_no_order;
    	}
    	
    }

    function cekAturanPakai($data1='',$data2='')
    {
    	if(!empty($data1) && !empty($data2))
		{
			$aturan = $data1 .', '. $data2;
		}else if(!empty($data1) && empty($data2))
		{
			$aturan = $data1;
		}else if(empty($data1) && !empty($data2))
		{
			$aturan = $data2;
		}else
		{
			$aturan = '';
		}

		return $aturan;
    }

    function getKdPoli($data,$time)
    {
    	$seskdpoli = $data;
    	if(count($data) == 1)
    	{
    		if($time > 06 && $time <= 22)
			{
				$kdpoli = $seskdpoli[0]->kd_poli;
			}
			else if($time >= 23 && $time < 24)
			{
				$kdpoli = $seskdpoli[1]->kd_poli;
			}
			else
			{
				$kdpoli = $seskdpoli[1]->kd_poli;
			}
		}
		else if(count($data) > 1)
		{
			$kdpoli = $seskdpoli[0]->kd_poli;
		}
    	else
    	{
    		$kdpoli = '';
    	}
    	//print_r($kdpoli);
		return $kdpoli;
    }