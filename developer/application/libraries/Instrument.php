<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Instrument {

	public function show_profiling($status='',$config='')
	{
		$CI =& get_instance();
		if($status == 'N') { 
			$CI->output->enable_profiler(FALSE);
		}
		else
		{		
			foreach (json_decode($config) as $key => $value) {
				$datas[$key] = ($value == 'Y') ? TRUE : FALSE; 
			}
			
			$CI->output->set_profiler_sections($datas);
			$CI->output->enable_profiler(TRUE);
		}
	}

	public function pagination($url='',$total_rows='',$per_page='')
	{
		$CI =& get_instance();
		$CI->load->library('pagination');

		$num_links					= $total_rows / $per_page;
		$config['base_url'] 		= $url;
		$config['total_rows'] 		= ($total_rows) ? $total_rows : 1;
		$config['per_page'] 		= ($per_page) ? $per_page : 0;
		$config["uri_segment"] 		= 3;
        $config["num_links"] 		= 3;
        //$config['use_page_numbers'] = TRUE;
		// Membuat Style pagination untuk BootStrap v4
      	$config['first_link']       = 'First&nbsp;';
        $config['last_link']        = '&nbsp;Last';
        $config['next_link']        = '&nbsp;Next';
        $config['prev_link']        = 'Prev&nbsp;';
        $config['full_tag_open']    = '<span class="label label-lg black pos-rlt m-r-xs"><b class="arrow right b-black"></b><span class="">';
        $config['full_tag_close']   = '<b class="arrow left b-black"></b></span> </span>';   
        $config['cur_tag_open']     = '<b class="text-blue">';
        $config['cur_tag_close']    = '</b>';    

		$CI->pagination->initialize($config);
	}

	public function showValRec($ar) 
	{
	    $ret = array();
	    if (@count($ar) > 0) {
	        foreach ($ar as $k => $v) {
	            $ret[] = 'data-' . $k . '="' . $v . '"';
	        }
	    }

	    return implode(' ', $ret);
	}

	public function generateReturn($location, $json = '', $returnJson = false) {
	    if ($returnJson) {
	    	//$this->isMobile() || 
	        //header('content-type:json/text');
	        echo $json ? json_encode($json) : json_encode($_SESSION);
	        exit();
	    } else {
	        header('location:' . $location);
	    }
	}

	public function datasToArray($data='',$key1='',$key2='',$key3='',$key4='',$key5='')
	{
	    $ls = array();
	    if($data)
	    {
	        if(!empty($key1) && !empty($key2) && !empty($key3) && !empty($key4))
	        {
	            foreach ($data as $key => $value) {
	                $ls[$value->$key1] = trim($value->$key2) .'  ('. trim($value->$key3).' - '. trim($value->$key4) .')';
	            }
	        }
	        else if(!empty($key1) && !empty($key2) && !empty($key3))
	        {
	            foreach ($data as $key => $value) {
	                $ls[$value->$key1] = trim($value->$key2) .'  ('. trim($value->$key3).')';
	            }
	        }
	        else if(!empty($key1) && !empty($key2))
	        {
	            foreach ($data as $key => $value) {
	                $ls[$value->$key1] = trim($value->$key2);
	            }
	        }else if(!empty($key1))
	        {
	            foreach ($data as $key => $value) {
	                $ls[] = trim($value->$key1);
	            }
	        }
	    }

	    return $ls;
	}

	function createDateRangeArray($strDateFrom, $strDateTo) {
        $aryRange = array();

        $iDateFrom = mktime(1, 0, 0, substr($strDateFrom, 5, 2), substr($strDateFrom, 8, 2), substr($strDateFrom, 0, 4));
        $iDateTo   = mktime(1, 0, 0, substr($strDateTo, 5, 2), substr($strDateTo, 8, 2), substr($strDateTo, 0, 4));

        if ($iDateTo >= $iDateFrom) {
            array_push($aryRange, date('Y-m-d', $iDateFrom)); // first entry
            while ($iDateFrom < $iDateTo) {
                $iDateFrom += 86400; // add 24 hours
                array_push($aryRange, date('Y-m-d', $iDateFrom));
            }
        }
        return $aryRange;
    }

    function getWeekDay($week, $bulan, $tahun) {
        $ret_day = array();
        $wk      = 1;

        for ($dt = 1; $dt <= date('t', strtotime($tahun . '-' . $bulan . '-01')); $dt++) {
            $dte = strtotime($tahun . '-' . $bulan . '-' . $dt);

            if ($week == $wk) {//jika week cocok
                $ret_day[] = date('d-m-Y', $dte);
            }

            if (date("N", $dte) == 7) {//if sunday
                $wk++;
            }
        }

        if (count($ret_day) < 7) {
            $max = count($ret_day);
            for ($ikd = 1; $ikd <= 7 - $max; $ikd++) {
                $ret_day[] = '';
            }
        }

        return $ret_day;

    }

    /**
     * Returns the amount of weeks into the month a date is
     * @param $date a YYYY-MM-DD formatted date
     * @param $rollover The day on which the week rolls over
     */
    function getWeeks($date, $rollover) {
        $cut    = substr($date, 0, 8);
        $daylen = 86400;

        $timestamp = strtotime($date);
        $first     = strtotime($cut . "00");
        $elapsed   = ($timestamp - $first) / $daylen;

        $weeks = 1;

        for ($i = 1; $i <= $elapsed; $i++) {
            $dayfind      = $cut . (strlen($i) < 2 ? '0' . $i : $i);
            $daytimestamp = strtotime($dayfind);

            $day = strtolower(date("l", $daytimestamp));

            if ($day == strtolower($rollover))
                $weeks ++;
        }

        return $weeks;
    }

    function splitRecursiveArray($array = array(), $key = '', $val = '') {
        $newArray = array();
        if ($key != '' || $val != '') {
            $keys = $key ? explode(',', $key) : array();
            $jumk = count($keys);
            $vals = $val ? explode(',', $val) : array();
            $jumv = count($vals);

            if (is_array($array)) {
                foreach ($array as $vk) {
                    $value = array();
                    if ($jumv > 1) {
                        foreach ($vals as $vv) {
                            $value[(string) $vv] = $vk[(string) $vv];
                        }
                    } elseif ($jumv == 1) {
                        $value = $vk[(string) $vals[0]];
                    } else {
                        $value = $vk;
                    }
                    switch ($jumk) {
                        case 1:
                            $newArray[$vk[(string) $keys[0]]] = $value;
                            break;
                        case 2:
                            $newArray[$vk[(string) $keys[0]]][$vk[(string) $keys[1]]] = $value;
                            break;
                        case 3:
                            $newArray[$vk[(string) $keys[0]]][$vk[(string) $keys[1]]][$vk[(string) $keys[2]]] = $value;
                            break;
                        case 4:
                            $newArray[$vk[(string) $keys[0]]][$vk[(string) $keys[1]]][$vk[(string) $keys[2]]][$vk[(string) $keys[3]]] = $value;
                            break;
                        case 5:
                            $newArray[$vk[(string) $keys[0]]][$vk[(string) $keys[1]]][$vk[(string) $keys[2]]][$vk[(string) $keys[3]]][$vk[(string) $keys[4]]] = $value;
                            break;
                        case 6:
                            $newArray[$vk[(string) $keys[0]]][$vk[(string) $keys[1]]][$vk[(string) $keys[2]]][$vk[(string) $keys[3]]][$vk[(string) $keys[4]]][$vk[(string) $keys[5]]] = $value;
                            break;
                        case 7:
                            $newArray[$vk[(string) $keys[0]]][$vk[(string) $keys[1]]][$vk[(string) $keys[2]]][$vk[(string) $keys[3]]][$vk[(string) $keys[4]]][$vk[(string) $keys[5]]][$vk[(string) $keys[6]]] = $value;
                            break;
                        case 8:
                            $newArray[$vk[(string) $keys[0]]][$vk[(string) $keys[1]]][$vk[(string) $keys[2]]][$vk[(string) $keys[3]]][$vk[(string) $keys[4]]][$vk[(string) $keys[5]]][$vk[(string) $keys[6]]][$vk[(string) $keys[7]]] = $value;
                            break;
                        case 9:
                            $newArray[$vk[(string) $keys[0]]][$vk[(string) $keys[1]]][$vk[(string) $keys[2]]][$vk[(string) $keys[3]]][$vk[(string) $keys[4]]][$vk[(string) $keys[5]]][$vk[(string) $keys[6]]][$vk[(string) $keys[7]]][$vk[(string) $keys[8]]] = $value;
                            break;
                        case 10:
                            $newArray[$vk[(string) $keys[0]]][$vk[(string) $keys[1]]][$vk[(string) $keys[2]]][$vk[(string) $keys[3]]][$vk[(string) $keys[4]]][$vk[(string) $keys[5]]][$vk[(string) $keys[6]]][$vk[(string) $keys[7]]][$vk[(string) $keys[8]]][$vk[(string) $keys[9]]] = $value;
                            break;
                        default:
                            $newArray[] = $value;
                            break;
                    }
                }
            }
        }             

        return $newArray;
    }

    function select($array, $attribute, $blank = true, $selected = '', $showKey = false, $arrayNonActive = '') {
        //$html  = "\r\n";
        $html = '<select ' . $attribute . '>';
        if (!empty($blank)) {
            if ($blank === true) {
                $html .= '<option value="">-- -- --</option>';
            } else {
                $html .= '<option value="">' . $blank . '</option>';
            }
        }
        if (is_array($array)) {
            if (count($array) > 0) {
                foreach ($array as $k => $v) {
                    $html .= '<option value="' . $k . '"';
                    if (is_array($selected)) {
                        if (in_array($k, $selected)) {
                            $html .= ' selected="selected"';
                        }
                    } else {
                        if ((string) $k == $selected && strlen($k) == strlen($selected)) {
                            $html .= ' selected="selected"';
                        }
                    }
                    if (is_array($arrayNonActive)) {
                        if (in_array($k, $arrayNonActive)) {
                            $html .= ' style="background-color:#eeeeee;color:#ff0000;"';
                        }
                    }
                    $html .= '>';
                    if ($showKey == true) {
                        $html .= $k . ' - ';
                    }
                    $html .= $v . '</option>';
                }
            } else {
                $html .= '<option value="">-- --</option>';
            }
        } else {
            $html .= '<option value="">-- --</option>';
        }
        $html .= '</select>';
        return $html;
    }

    function _entity($datas)
    {
        if(is_array($datas) && count($datas) > 0)
        {
            $retArray = array();
            foreach ($datas as $key => $value) {
                $retArray[$key] = ($value) ? htmlentities($value,ENT_QUOTES) : ''; 
            }
        }

        return $retArray;
    }

    function _post($request, $entity = true)
    {
        $CI =& get_instance();
        $datas = $CI->input->post();
        $retArray = array();

        if(is_array($request) && count($request) > 0)
        {
            foreach ($request as $key) {
                if (!$entity) {
                    $retArray[$key] = isset($datas[$key]) ? $datas[$key] : '';
                } else {
                    $retArray[$key] = isset($datas[$key]) ? ($entity ? htmlentities($datas[$key], ENT_QUOTES) : $datas[$key]) : '';
                }
            }
        }
        else
        {
            foreach ($datas as $key => $value) {
                $retArray[$key] = isset($value) ? htmlentities($value, ENT_QUOTES) : '';
            }
        }

        return $retArray;
    }

    function AddDate($date='',$numDay,$time='')
    {
        $tNow   = date_create($date);
        date_add($tNow, date_interval_create_from_date_string($numDay .' '. $time));
        $res  = date_format($tNow, 'Y-m-d');
        return $res;
    }

    function countDate($dNow,$dNext)
    {
        $convert_totime     = abs(strtotime($dNow) - strtotime($dNext));
        $res_day            = $convert_totime/86400;
        return $res_day;
    }

    function getAge($d1, $d2, $shows=array())
    {
        $d1 = (is_string($d1) ? strtotime($d1) : $d1);
        $d2 = (is_string($d2) ? strtotime($d2) : $d2);
        $diff_secs = abs($d1 - $d2);
        $base_year = min(date("Y", $d1), date("Y", $d2));
        $diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);
        
        $show['year']      = date("Y", $diff) - $base_year;
        $show['month']     = date("n", $diff) - 1;
        $show['day']       = date("j", $diff) - 1;
        $show['hour']      = date("G", $diff);
        $show['minute']    = (int) date("i", $diff);

        if(is_array($shows))
        {
            foreach ($shows as $k) {
                $res[$k] = $show[$k];
            }
        }
        return $res;
    }

    function indoDays($date){
        $getDays=date('D',strtotime($date));
        $day = array(
            'Sun' => 'AKHAD',
            'Mon' => 'SENIN',
            'Tue' => 'SELASA',
            'Wed' => 'RABU',
            'Thu' => 'KAMIS',
            'Fri' => 'JUMAT',
            'Sat' => 'SABTU'
        );
        return $showDay=$day[$getDays];
    }

    function cutText($text, $length, $mode = 1)
    {
        if ($mode != 1)
        {
            $char = $text{$length - 1};
            switch($mode)
            {
                case 2: 
                    while($char != ' ') {
                        $char = $text{--$length};
                    }
                case 3:
                    while($char != ' ') {
                        $char = $text{++$length};
                    }
            }
        }
        return substr($text, 0, $length);
    }

    function cutName($data)
    {
        if($data)
        {
            $nm = explode(' ', $data);
        }

        return $nm[0];
    }

    function array_count($array = array(),$deep){
        $return = 0;
        switch($deep){
            case 1:
                count($array);
            break;
            case 2:
                foreach($array as $k1=>$v1){
                    $return += count($array[$k1]);
                }
            break;
            case 3:
                foreach($array as $k1=>$v1){
                    foreach($v1 as $k2=>$v2){
                        $return += count($array[$k1][$k2]);
                    }
                }
            break;
            case 4:
                foreach($array as $k1=>$v1){
                    foreach($v1 as $k2=>$v2){
                        foreach($v2 as $k3=>$v3){
                            $return += count($array[$k1][$k2][$k3]);
                        }
                    }
                }
            break;
            case 5:
                foreach($array as $k1=>$v1){
                    foreach($v1 as $k2=>$v2){
                        foreach($v2 as $k3=>$v3){
                            foreach($v3 as $k4=>$v4){
                                $return += count($array[$k1][$k2][$k3][$k4]);
                            }
                        }
                    }
                }
            break;
        }
        
        return $return;
    }

    function htmlSelectFromArray($array, $attribute, $blank = true, $selected = '', $showKey = false, $arrayNonActive = '') {
        //$html  = "\r\n";
        $html = '<select ' . $attribute . '>';
        if (!empty($blank)) {
            if ($blank === true) {
                $html .= '<option value="">-- -- --</option>';
            } else {
                $html .= '<option value="">' . $blank . '</option>';
            }
        }
        if (is_array($array)) {
            if (count($array) > 0) {
                foreach ($array as $k => $v) {
                    $html .= '<option value="' . $k . '"';
                    if (is_array($selected)) {
                        if (in_array($k, $selected)) {
                            $html .= ' selected="selected"';
                        }
                    } else {
                        if ((string) $k == $selected && strlen($k) == strlen($selected)) {
                            $html .= ' selected="selected"';
                        }
                    }
                    if (is_array($arrayNonActive)) {
                        if (in_array($k, $arrayNonActive)) {
                            $html .= ' style="background-color:#eeeeee;color:#ff0000;"';
                        }
                    }
                    $html .= '>';
                    if ($showKey == true) {
                        $html .= $k . ' - ';
                    }
                    $html .= $v . '</option>';
                }
            } else {
                $html .= '<option value="">-- --</option>';
            }
        } else {
            $html .= '<option value="">-- --</option>';
        }
        $html .= '</select>';
        return $html;
    }

}