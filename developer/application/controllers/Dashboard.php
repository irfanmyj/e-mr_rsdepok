<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends backend_controller {

	public function __construct(){
		parent::__construct();
		$this->load->model(array('Dashboard_model'));
		$this->site->is_logged_in();
	}

	public function index()
	{

		$data = array(
			'title'		=> 'Halaman Dashboard',
			'file'		=> 'home',
			'folder'	=> 'page'
		);
		
		$this->site->view('inc',$data);
	}
}
