<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Provinsi extends backend_controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('Provinsi_model'));
	}
	
	public function index()
	{
		try {
			global $Cf;
			$post 	= $this->input->post();
			$search	= isset($post['search']) ? $post['search'] : '';
			$limit 	= isset($post['limit']) ? $post['limit'] : $Cf->default_limit;
			$offset = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

			$where = '';
			if($search)
			{
				$where = 'name like \'%'.$search.'%\'';
			} 

			$count 	= $this->Provinsi_model->count('',$where);
			$get 	= $this->Provinsi_model->get('','',$where,'','','id',$limit,$offset)->result();

			$this->instrument->pagination(site_url($this->uri->segment(1).'/index'),$count,$limit);
			$view = array(
				'title' 	=> 'Master Time To Pay',
				'row' 		=> $get,
				'total_rows'=> $count,
				'search'	=> $search,
				'limit'		=> $Cf->limit_rows,
				'limit_selected'=> $limit,
				'status'	=> $Cf->status,
				'folder' 	=> 'master',
				'breadcrumb'=> $this->mybreadcrumb->render(),
				'file' 		=> 'masterprovinsi'
			);
			$this->site->view('inc',$view);	
		} catch (Exception $e) {
			$msg = 'Error : '. $e->getMessage();
			return $msg;
		}
	}

	public function save()
	{
		try {
			global $Cf;
			$fields = array(
				'name'
			);
			$datas = $this->instrument->_post($fields);
			$rules = $this->Provinsi_model->rules;
			$this->form_validation->set_rules($rules);

			if($this->input->post('id'))
			{
				if($this->form_validation->run() == FALSE)
				{
					$_SESSION['error'] = '1';
					$_SESSION['msg']   = 'Data <b> TIDAK </b> berhasil diperbarui, silakan coba lagi nanti.';
				}
				else
				{
					$_SESSION['error'] = '';
					$_SESSION['msg']   = 'Data telah berhasil diperbarui.';

					$this->Provinsi_model->update('',$datas,array('id'=>$this->input->post('id')));
				}
			}
			else
			{
				if($this->form_validation->run() == FALSE)
				{
					$_SESSION['error'] = '1';
					$_SESSION['msg']   = 'Data <b> TIDAK </b> berhasil disimpan, silakan coba lagi nanti.';
				}
				else
				{
					$_SESSION['error'] = '';
					$_SESSION['msg']   = 'Data telah berhasil simpan.';

					// Get SORT
					$get_sort = $this->Provinsi_model->get('','max(id) as id','','','','',1,'')->result();
					$datas['id'] = $get_sort[0]->id+1;
					$this->Provinsi_model->insert('',$datas,FALSE);
				}
			}

		} catch (Exception $e) {
			$_SESSION['error'] = '1';
			$_SESSION['msg'] = $e->getMessage();
		}

		$this->instrument->generateReturn(base_url($this->uri->segment(1).'/index'));

	}

	public function delete($data='')
	{
		if(strpos($data, 'k') > 0)
		{
			$idls = explode('k', $data);
			if(is_array($idls))
			{
				foreach ($idls as $idv)
				{
					$det = $this->Provinsi_model->get('','name','id=\''.$idv.'\'');
					if($this->Provinsi_model->delete('',array('id'=>$idv)))
					{
						$_SESSION['error'] = '';
		            	$_SESSION['msg']   = 'Data telah berhasil dihapus.';
					}
					else
					{
						$_SESSION['error'] = '1';
		        		$_SESSION['msg']   = 'Data <b> TIDAK </b> berhasil dihapus, silakan coba lagi nanti.';
						//$this->Get_model->createHistory('Delete bahasa dengan nama : '.$det[0]->bahasa.'',$this->session->userdata('ID'));
					}
				}
			}	
		}
		else
		{
			if($this->Provinsi_model->delete('',array('id'=>$data)))
			{
				$_SESSION['error'] = '';
		        $_SESSION['msg']   = 'Data telah berhasil dihapus.';
			}
			else
			{
				$_SESSION['error'] = '1';
		       	$_SESSION['msg']   = 'Data <b> TIDAK </b> berhasil dihapus, silakan coba lagi nanti.';
			}
		}
		
		$this->instrument->generateReturn(base_url($this->uri->segment(1).'/index'));
	}
}
