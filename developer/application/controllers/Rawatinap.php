<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Rawatinap extends backend_controller {

	public function __construct(){
		parent::__construct();
		$this->load->model(array('Rawatinap_model','Rawatjalan_model','Antrianranap_model'));
		$this->site->is_logged_in();
	}

	public function index()
	{
		global $Cf;
		$keys 	= array('search');
		$datas 	= $this->instrument->_post($keys);
		$search	= isset($datas['search']) 	? $datas['search'] : '';
		$limit 	= isset($post['limit']) 	? $post['limit'] : '';
		$offset = ($this->uri->segment(3)) 	? $this->uri->segment(3) : 0;
		$dNow 	= date('Y-m-d');
		$dTitle = ($search) ? $search : $dNow;

		$where = array(
			'dpjp_ranap.kd_dokter' => $this->session->userdata('id_user'),
			'kamar_inap.stts_pulang' => '-'
		);

		if($search)
		{
			$where = '
				dpjp_ranap.kd_dokter = \''.$this->session->userdata('id_user').'\'
				and kamar_inap.tgl_masuk= \''.date('Y-m-d',strtotime($dTitle)).'\'
			';
		}

		$tbjoin = array(
			'pasien' 	 => array(
				'metode' => 'INNER',
				'relasi' => 'pasien.no_rkm_medis=reg_periksa.no_rkm_medis'
			),
			'kamar_inap' => array( // nama tabel
				'metode' => 'INNER', // metode join
				'relasi' => 'kamar_inap.no_rawat=reg_periksa.no_rawat' // filed relasi tabel antar tabel
			),
			'kamar' => array(
				'metode' => 'INNER',
				'relasi' => 'kamar.kd_kamar=kamar_inap.kd_kamar'
			),
			'bangsal' => array(
				'metode' => 'INNER',
				'relasi' => 'bangsal.kd_bangsal=kamar.kd_bangsal'
			),
			'penjab' => array(
				'metode' => 'INNER',
				'relasi' => 'penjab.kd_pj=reg_periksa.kd_pj'
			),
			'dpjp_ranap' => array(
				'metode' => 'INNER',
				'relasi' => 'dpjp_ranap.no_rawat=reg_periksa.no_rawat'
			)
		);

		$field = '
			pasien.nm_pasien,
			pasien.jk,
			pasien.tgl_lahir,
        	reg_periksa.tgl_registrasi,
        	reg_periksa.no_rkm_medis,
        	reg_periksa.umurdaftar,
        	bangsal.nm_bangsal,
        	kamar_inap.kd_kamar,
        	kamar_inap.tgl_masuk,
        	penjab.png_jawab,
        	reg_periksa.no_rawat
		';

		//$count_booking 	= $this->Booking_model->countJoin('',$tbjoin,$where);
		$res_rawatinap 	= $this->Rawatinap_model->getJoin('',$tbjoin,$field,$where,'','','',$limit,$offset)->result();

		/*echo '<pre>';
		print_r($res_rawatinap);
		exit();*/
		$data = array(
			'title'		=> 'Jumlah Data Pasien Rawatinap',
			'titled'	=> 'Jumlah Data Pasien Rawatinap Tanggal '. date('d M Y',strtotime($dTitle)),
			'date'		=> $dTitle,
			'row' 		=> $res_rawatinap,
			'file'		=> 'index',
			'folder'	=> 'rawatinap'
		);
		
		$this->site->view('inc',$data);
	}

	public function kajipasien($no_rawat,$dateNow,$noRm)
	{
		$this->load->model(array('Masteraturanpakai_model','Diagnosapasien_model','Resepobat_model','Permintaanlab_model','Permintaanradiologi_model'));
		$noRawat 	= isset($no_rawat) ? str_replace('-', '/', $no_rawat) : '';
		$dtNow 		= date('Y-m-d');
		$dNow		= isset($dateNow) ? date('Y-m-d',strtotime($dateNow)) : date('Y-m-d');
		$no_rkm_medis= isset($noRm) ? $noRm : '';

		/*
		Start Data Pasien
		Mengambil data pribadi pasien
		*/
		$where = array(
			'reg_periksa.no_rawat' => $noRawat
		);

		$res_pasien = $this->Rawatinap_model->getPaseinRajalAll($where);

		// End data pasien

		/*
		Start Antrian
		Mengambil data untuk urutan antrian
		*/
		if($dNow == $dtNow)
		{
			$where2 = array(
				'stts!=' => 'Sudah',
				'tgl_registrasi'=>$dNow,
				'kd_dokter'=>$this->session->userdata('id_user')
			);
		}
		else
		{
			$where2 = array(
				'dpjp_ranap.kd_dokter'=>$this->session->userdata('id_user'),
				'reg_periksa.status_lanjut'=>'Ranap',
				'kamar_inap.tgl_keluar'=>'0000-00-00'
			);
		}
		
		
		$res_antrian = $this->Antrianranap_model->getPaseinRanap($where2);

		// End Antrian

		/*
		Mengambil data no rawat dan tanggal registrasi
		*/
		
		$where3 = array(
			'reg_periksa.no_rkm_medis'=>$no_rkm_medis,
			'reg_periksa.stts!='=>'Batal'
		);
		$ls = array();
		$res_option = $this->Rawatinap_model->getPaseinRajal3($where3);
		//$data_option = $this->instrument->splitRecursiveArray($res_option,'no_rawat','tgl_registrasi');

		foreach ($res_option as $k => $v) {
            $ls[$v['no_rawat']] = date('d M Y', strtotime($v['tgl_registrasi'])) .' ('. $this->instrument->indoDays($v['tgl_registrasi']).') - '. $v['status_lanjut'];
        }

        $aturan_pakai1 = $this->Masteraturanpakai_model->get('','aturan')->result_array();
        $aturan_pakai = $this->instrument->splitRecursiveArray($aturan_pakai1,'aturan','aturan');

        // Diagnosa
        $ResDiagnosa = $this->Diagnosapasien_model->getDiagnosa(array('diagnosa_pasien.no_rawat' => $noRawat));

        // Resep Obat
        $fieldObt = '
        	resep_obat.no_resep,
			resep_obat.no_rawat,
			resep_dokter.kode_brng,
			resep_dokter.jml,
			resep_dokter.aturan_pakai,
			databarang.nama_brng
		';

		$tbjoinObt = array(
			'resep_dokter' => array(
				'metode' => 'inner',
				'relasi' => 'resep_dokter.no_resep=resep_obat.no_resep'
			),
			'databarang' => array(
				'metode' => 'inner',
				'relasi' => 'databarang.kode_brng=resep_dokter.kode_brng'
			)
		);

		$whereObt = array(
			'resep_obat.no_rawat' => $noRawat
		);

        $ResResepObat = $this->Resepobat_model->getJoin('',$tbjoinObt,$fieldObt,$whereObt,'','','')->result();

        // Get Lab
        $fieldLabs = '
        	permintaan_lab.noorder,
			permintaan_lab.no_rawat,
			permintaan_pemeriksaan_lab.kd_jenis_prw,
			jns_perawatan_lab.nm_perawatan
		';

		$tbjoinLabs = array(
			'permintaan_pemeriksaan_lab' => array(
				'metode' => 'inner',
				'relasi' => 'permintaan_pemeriksaan_lab.noorder=permintaan_lab.noorder'
			),
			'jns_perawatan_lab' => array(
				'metode' => 'inner',
				'relasi' => 'jns_perawatan_lab.kd_jenis_prw=permintaan_pemeriksaan_lab.kd_jenis_prw'
			)
		);

		$whereLabs = array(
			'permintaan_lab.no_rawat' => $noRawat
		);

        $ResLabs = $this->Permintaanlab_model->getJoin('',$tbjoinLabs,$fieldLabs,$whereLabs)->result();

        // Get Rad
         $fieldRad = '
        	permintaan_radiologi.noorder,
			permintaan_radiologi.no_rawat,
			permintaan_pemeriksaan_radiologi.kd_jenis_prw,
			jns_perawatan_radiologi.nm_perawatan
		';

		$tbjoinRad = array(
			'permintaan_pemeriksaan_radiologi' => array(
				'metode' => 'inner',
				'relasi' => 'permintaan_pemeriksaan_radiologi.noorder=permintaan_radiologi.noorder'
			),
			'jns_perawatan_radiologi' => array(
				'metode' => 'inner',
				'relasi' => 'jns_perawatan_radiologi.kd_jenis_prw=permintaan_pemeriksaan_radiologi.kd_jenis_prw'
			)
		);

		$whereRad = array(
			'permintaan_radiologi.no_rawat' => $noRawat
		);

        $resrads = $this->Permintaanradiologi_model->getJoin('',$tbjoinRad,$fieldRad,$whereRad)->result();
		
		/*print_r($resrads);
		exit();*/
		$data = array(
			'title' 	=> 'Halaman Informasi Tindakan',
			'titled' 	=> 'Form Pengkajian Pasien',
			'noRawat' 	=> $noRawat,
			'dtpasien' 	=> $res_pasien,
			'antrian' 	=> $res_antrian,
			'driwayat' 	=> $ls,
			'no_rkm_medis'=> $no_rkm_medis,
			'aturan_pakai'=> $aturan_pakai,
			'diagnosa' => $ResDiagnosa,
			'resepobat' => $ResResepObat,
			'reslabs' => $ResLabs,
			'resrads' => $resrads,
			'usia' 		=> $this->instrument->getAge(date('Y-m-d'),$res_pasien[0]->tgl_lahir,array('year','month','day')),
			'file' 		=> 'kajipasien',
			'folder' 	=> 'rawatinap'
		);
		$this->site->view('inc',$data);
	}

	public function getHistory()
	{
		$this->load->model(array('Aturanpakai_model','Laboratorium_model','Diagnosapasien_model','Detailpemberianobat_model','Hasilradiologi_model'));
		$post 			= $this->input->post();
		$no_rkm_medis 	= isset($post['no_rkm_medis']) ? $post['no_rkm_medis'] : '';
		$no_rawat 		= isset($post['no_rawat']) ? $post['no_rawat'] : '';

		/*
		Start Show Data Poliklinik
		*/
		$where = array(
			'reg_periksa.no_rawat' => $no_rawat,
			'reg_periksa.no_rkm_medis' => $no_rkm_medis,
			'reg_periksa.status_lanjut' => 'Ralan'
		);

		$where1 = array(
			'diagnosa_pasien.no_rawat' => $no_rawat,
			'diagnosa_pasien.status' => 'Ralan'
		);

		$field1 = '
			diagnosa_pasien.kd_penyakit,
			diagnosa_pasien.prioritas,
			penyakit.nm_penyakit
		';

		$tbjoin1 = array(
			'penyakit' => array(
				'metode' => 'inner',
				'relasi' => 'penyakit.kd_penyakit=diagnosa_pasien.kd_penyakit'
			)
		);

		$res_diagnosa 	= $this->Diagnosapasien_model->getJoin('',$tbjoin1,$field1,$where1,'','','diagnosa_pasien.prioritas asc')->result();
		// End Diagnosa

		$where2 = array(
			'detail_pemberian_obat.no_rawat' => $no_rawat,
			'detail_pemberian_obat.status' => 'Ralan'
		);

		$field2 = '
			detail_pemberian_obat.kode_brng,
			detail_pemberian_obat.jml,
			databarang.nama_brng
		';

		$tbjoin2 = array(
			'databarang' => array(
				'metode' => 'inner',
				'relasi' => 'databarang.kode_brng=detail_pemberian_obat.kode_brng'
			)
		);

		$res_aturan_pakai = $this->Aturanpakai_model->get('','kode_brng,aturan',array('no_rawat'=>$no_rawat),'','','')->result_array();
		$aturan_pakai = $this->instrument->splitRecursiveArray($res_aturan_pakai,'kode_brng','aturan');
		$res_obat 	= $this->Detailpemberianobat_model->getJoin('',$tbjoin2,$field2,$where2)->result();
		// End Obat

		$field3 = '
			hasil
		';

		$res_rad 	= $this->Hasilradiologi_model->get('',$field3,array(
			'no_rawat' => $no_rawat
		))->result();
		// End Radiologi

		$where4 = array(
			'detail_periksa_lab.no_rawat' => $no_rawat
		);

		$field4 = '
			detail_periksa_lab.kd_jenis_prw,
			detail_periksa_lab.nilai,
			detail_periksa_lab.nilai_rujukan,
			detail_periksa_lab.keterangan,
			jns_perawatan_lab.nm_perawatan
		';

		$tbjoin4 = array(
			'jns_perawatan_lab' => array(
				'metode' => 'inner',
				'relasi' => 'jns_perawatan_lab.kd_jenis_prw=detail_periksa_lab.kd_jenis_prw'
			)
		);
		$res_lab 	= $this->Laboratorium_model->getJoin('',$tbjoin4,$field4,$where4)->result();
		// End Laboratorium


		$field5 = '
			reg_periksa.no_rawat,
			reg_periksa.kd_poli,
			reg_periksa.tgl_registrasi,
			poliklinik.nm_poli,
			pemeriksaan_ralan.keluhan,
			pemeriksaan_ralan.pemeriksaan,
			dokter.nm_dokter
		';

		$tbjoin5 = array(
			'poliklinik' => array(
				'metode' => 'inner',
				'relasi' => 'poliklinik.kd_poli=reg_periksa.kd_poli'
			),
			'dokter' => array(
				'metode' => 'inner',
				'relasi' => 'dokter.kd_dokter=reg_periksa.kd_dokter'
			),
			'pemeriksaan_ralan' => array(
				'metode' => 'left',
				'relasi' => 'pemeriksaan_ralan.no_rawat=reg_periksa.no_rawat'
			)
		);
		$res_dataumum 	= $this->Rawatjalan_model->getJoin('',$tbjoin5,$field5,$where)->result();

		$html = '<div class="col-12 mt-3">';
			$html .= '<div class="card-header panel-heading no-border bg-primary" style="padding: 5px; background: blue;"><h5 class="text-white"><i class="fa fa-book"></i> Data Pasien</h5></div>';
			$html .= '<div class="row">';
					$html .= '<div class="col-12">';
						$html .= '<table class="table table-striped table-bordered table-responsive-md">';
							$html .= '<tr>';
								$html .= '<td align="center" width="10">1.</td>';
								$html .= '<td width="150">Tanggal Periksa</td>';
								$html .= '<td>'.date('d M Y',strtotime($res_dataumum[0]->tgl_registrasi)) .' | Hari : '. $this->instrument->indoDays($res_dataumum[0]->tgl_registrasi) .'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td align="center" width="10">2.</td>';
								$html .= '<td width="150">No Rawat</td>';
								$html .= '<td>'.$res_dataumum[0]->no_rawat.'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td align="center" width="10">3.</td>';
								$html .= '<td width="150">Nama Dokter</td>';
								$html .= '<td>'.$res_dataumum[0]->nm_poli.'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td align="center" width="10">4.</td>';
								$html .= '<td width="150">Nama Poli</td>';
								$html .= '<td>'.$res_dataumum[0]->nm_dokter.'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td align="center" width="10">5.</td>';
								$html .= '<td width="150">Keluhan</td>';
								$html .= '<td>'.$res_dataumum[0]->keluhan.'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td align="center" width="10">6.</td>';
								$html .= '<td width="150">Pemeriksaan</td>';
								$html .= '<td>'.$res_dataumum[0]->pemeriksaan.'</td>';
							$html .= '</tr>';
						$html .= '</table>';
					$html .= '</div>';
				$html .= '</div>';
		$html .= '</div>';

		$html .= '<div class="col-12">';
			$html .= '<div class="card-header panel-heading no-border bg-primary" style="padding: 5px; background: blue;"><h5 class="text-white"><i class="fa fa-book"></i> Data Riwayat Diagnosis</h5></div>';
			$html .= '<div class="row">';
				$html .= '<div class="col-12">';
					$html .= '<table class="table table-striped table-bordered table-responsive-md">';
						$html .= '<thead>';
							$html .= '<tr>';
								$html .= '<th>No</th>';
								$html .= '<th>Kode Diagnosa</th>';
								$html .= '<th>Nama Diagnosa</th>';
								$html .= '<th>Prioritas</th>';
							$html .= '</tr>';
						$html .= '</thead>';
						$html .= '<tbody>';
						$no=0;
							foreach ($res_diagnosa as $k => $v) {
								$no++;
								$html .= '<tr>';
									$html .= '<td align="center">'.$no.'</td>';
									$html .= '<td>'.$v->kd_penyakit.'</td>';
									$html .= '<td>'.$v->nm_penyakit.'</td>';
									$html .= '<td>';
									$html .= ($v->prioritas ==1) ? 'Primer' : 'Sekunder';
									$html .= '</td>';
								$html .= '</tr>';
							}
						$html .= '</tbody>'; 
					$html .= '</table>';
				$html .= '</div>';
			$html .= '</div>';
		$html .= '</div>';

		$html .= '<div class="col-12">';
			$html .= '<div class="card-header panel-heading no-border bg-primary" style="padding: 5px; background: blue;"><h5 class="text-white"><i class="fa fa-book"></i> Data Riwayat Pemberian Obat</h5></div>';
				$html .= '<div class="row">';
					$html .= '<div class="col-12">';
						$html .= '<table class="table table-striped table-bordered table-responsive-md">';
							$html .= '<thead>';
								$html .= '<tr>';
									$html .= '<th width="10">No</th>';
									$html .= '<th>Nama Obat</th>';
									$html .= '<th width="10">Jml</th>';
									$html .= '<th>Aturan Pakai</th>';
								$html .= '</tr>';
							$html .= '</thead>';
							$html .= '<tbody>';
							$no=0;
								foreach ($res_obat as $k => $v) {
									$no++;
									$html .= '<tr>';
										$html .= '<td align="center">'.$no.'</td>';
										$html .= '<td>'.$v->nama_brng.'</td>';
										$html .= '<td>'.$v->jml.'</td>';
										$html .= '<td>';
										$html .= (@$aturan_pakai[$v->kode_brng]) ? @$aturan_pakai[$v->kode_brng] : '-';
										$html .= '</td>';
									$html .= '</tr>';
								}
							$html .= '</tbody>'; 
						$html .= '</table>';
					$html .= '</div>';
				$html .= '</div>';
		$html .= '</div>';

		$html .= '<div class="col-12">';
			$html .= '<div class="card-header panel-heading no-border bg-primary" style="padding: 5px; background: blue;"><h5 class="text-white"><i class="fa fa-book"></i> Data Riwayat Laboratorium</h5></div>';
				$html .= '<div class="row">';
					$html .= '<div class="col-12">';
						$html .= '<table class="table table-striped table-bordered table-responsive-md">';
							$html .= '<thead>';
								$html .= '<tr>';
									$html .= '<th width="10">No</th>';
									$html .= '<th>Nama Pemeriksaan</th>';
									$html .= '<th>Hasil Laboratorium</th>';
								$html .= '</tr>';
							$html .= '</thead>';
							$html .= '<tbody>';
							$no=0;
								foreach ($res_lab as $k => $v) {
									$no++;
									$html .= '<tr>';
										$html .= '<td align="center">'.$no.'</td>';
										$html .= '<td>'.$v->nm_perawatan.'</td>';
										$html .= '<td>';
										$html .= ($v->nilai) ? $v->nilai : $v->nilai_rujukan;
										$html .= '</td>';
									$html .= '</tr>';
								}
							$html .= '</tbody>'; 
						$html .= '</table>';
					$html .= '</div>';
				$html .= '</div>';
		$html .= '</div>';

		$html .= '<div class="col-12">';
			$html .= '<div class="card-header panel-heading no-border bg-primary" style="padding: 5px; background: blue;"><h5 class="text-white"><i class="fa fa-book"></i> Data Riwayat Radiologi</h5></div>';
				$html .= '<div class="row">';
					$html .= '<div class="col-12">';
						$html .= '<table class="table table-striped table-bordered table-responsive-md">';
							$html .= '<thead>';
								$html .= '<tr>';
									$html .= '<th width="10">No</th>';
									$html .= '<th>Hasil Radiologi</th>';
								$html .= '</tr>';
							$html .= '</thead>';
							$html .= '<tbody>';
							$no=0;
								foreach ($res_rad as $k => $v) {
									$no++;
									$html .= '<tr>';
										$html .= '<td align="center">'.$no.'</td>';
										$html .= '<td>'.$v->hasil.'</td>';
									$html .= '</tr>';
								}
							$html .= '</tbody>'; 
						$html .= '</table>';
					$html .= '</div>';
				$html .= '</div>';
		$html .= '</div>';
		/*
		End Show Data Poliklinik
		*/

		/*
		Start Show Data Rawatinap
		*/
		$where6 = array(
			'reg_periksa.no_rawat' => $no_rawat,
			'reg_periksa.no_rkm_medis' => $no_rkm_medis,
			'reg_periksa.status_lanjut' => 'Ranap'
		);

		$field6 = '
			reg_periksa.no_rawat,
			reg_periksa.tgl_registrasi,
			pemeriksaan_ranap.keluhan,
			pemeriksaan_ranap.pemeriksaan,
			bangsal.nm_bangsal
		';

		$tbjoin6 = array(
			'pemeriksaan_ranap' => array(
				'metode' => 'inner',
				'relasi' => 'pemeriksaan_ranap.no_rawat=reg_periksa.no_rawat'
			),
			'kamar_inap' => array(
				'metode' => 'inner',
				'relasi' => 'kamar_inap.no_rawat=reg_periksa.no_rawat'
			),
			'kamar' => array(
				'metode' => 'inner',
				'relasi' => 'kamar.kd_kamar=kamar_inap.kd_kamar'
			),
			'bangsal' => array(
				'metode' => 'inner',
				'relasi' => 'bangsal.kd_bangsal=kamar.kd_bangsal'
			)
		);
		$res_dataumum2 	= $this->Rawatjalan_model->getJoin('',$tbjoin6,$field6,$where6)->result();
		if($res_dataumum2)
		{
			$html1 = '<div class="col-12 mt-3">';
				$html1 .= '<div class="card-header panel-heading no-border bg-primary" style="padding: 5px; background: blue;"><h5 class="text-white"><i class="fa fa-book"></i> Data Pasien</h5></div>';
				$html1 .= '<div class="row">';
						$html1 .= '<div class="col-12">';
							$html1 .= '<table class="table table-striped table-bordered table-responsive-md">';
								$html1 .= '<tr>';
									$html1 .= '<td align="center" width="10">1.</td>';
									$html1 .= '<td width="150">Tanggal Periksa</td>';
									$html1 .= '<td>'.date('d M Y',strtotime($res_dataumum2[0]->tgl_registrasi)) .' | Hari : '. $this->instrument->indoDays($res_dataumum2[0]->tgl_registrasi) .'</td>';
								$html1 .= '</tr>';
								$html1 .= '<tr>';
									$html1 .= '<td align="center" width="10">2.</td>';
									$html1 .= '<td width="150">No Rawat</td>';
									$html1 .= '<td>'.$res_dataumum2[0]->no_rawat.'</td>';
								$html1 .= '</tr>';
								$html1 .= '<tr>';
									$html1 .= '<td align="center" width="10">5.</td>';
									$html1 .= '<td width="150">Nama Bangsal</td>';
									$html1 .= '<td>'.$res_dataumum2[0]->nm_bangsal.'</td>';
								$html1 .= '</tr>';
								$html1 .= '<tr>';
									$html1 .= '<td align="center" width="10">5.</td>';
									$html1 .= '<td width="150">Keluhan</td>';
									$html1 .= '<td>'.$res_dataumum2[0]->keluhan.'</td>';
								$html1 .= '</tr>';
								$html1 .= '<tr>';
									$html1 .= '<td align="center" width="10">6.</td>';
									$html1 .= '<td width="150">Pemeriksaan</td>';
									$html1 .= '<td>'.$res_dataumum2[0]->pemeriksaan.'</td>';
								$html1 .= '</tr>';
							$html1 .= '</table>';
						$html1 .= '</div>';
					$html1 .= '</div>';
			$html1 .= '</div>';
		}


		$view = array(
			'poliklinik' => $html,
			'rawatinap' => isset($html1) ? $html1 : '<p>Riwayat data pasien rawatinap tidak ditemukan.</p>'
		);

		echo json_encode($view);
	}
}