<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Info extends backend_controller {

	public function __construct(){
		parent::__construct();
		$this->load->model(array('Password_model','Dokter_model','Tes_model'));
		$this->site->is_logged_in();
	}

	public function index()
	{
		$get = $this->Dokter_model->get('','*',array('kd_dokter'=>$this->session->userdata('id_user')))->result();

		$data = array(
			'title'		=> 'Halaman Dashboard',
			'title'		=> 'Halaman Informasi Dokter',
			'titled'	=> 'Halaman Informasi Dokter',
			'res'		=> $get,
			'folder'	=> 'dokter',
			'file'		=> 'index'
		);

		$this->site->view('inc',$data);
	}

	public function save()
	{
		try {
			$post = $this->input->post();
			$fields = array(
				'password2',
				'email'
			);
			$datas = $this->instrument->_post($fields);
			/*print_r($datas);
			exit();*/
			if($datas['password2'])
			{
				$data['password'] = sha1($datas['password2'].$this->session->userdata('id_user'));
				//$data['password2'] = sha1($datas['password2'].$this->session->userdata('id_user'));
				//$data['sts_online'] = 'Y';
				$email['email'] = isset($datas['email']) ? $datas['email'] : '';
				
				if($this->Password_model->update('',$data,"id_user = AES_ENCRYPT('{$this->session->userdata('id_user')}','nur')"))
				{
					$this->Dokter_model->update('',$email,array('kd_dokter'=>$this->session->userdata('id_user')));
					$error = '';
					throw new Exception('Data password anda telah berhasil diperbarui.');
				}
				else
				{
					$error = 1;
					throw new Exception('Data <b> TIDAK </b> berhasil diperbarui.');
				}
			}
			else
			{
				$error = 1;
				throw new Exception('Data <b> PASSWORD </b> tidak boleh kosong.');
			}

		} catch (Exception $e) {
			$_SESSION['error'] = $error;
			$_SESSION['msg'] = $e->getMessage();
		}
		$this->instrument->generateReturn(base_url($this->uri->segment(1).'/index'));
	}

	public function cekemail()
	{
		$post = $this->input->post();
		$this->load->library('Smtp');
		$res = $this->smtp->validate(array($post['email']));
		if($res)
		{
			echo 'Email anda sudah terdaftar.';
		}
		else
		{
			echo 'Mohon maaf email anda tidak terdaftar di GMAIL';
		}
	}

	public function chart()
	{
		$this->load->model(array('Booking_model'));
		$res_cara_bayar = $this->Booking_model->get('','sum(limit_reg) as total, kd_pj,MONTH(tanggal_periksa) as bulan',array('YEAR(tanggal_periksa)'=>date('Y'),'kd_dokter'=>$this->session->userdata('id_user')),'booking_registrasi.kd_pj,MONTH(tanggal_periksa)')->result();

		foreach ($res_cara_bayar as $k => $v) {
			$datas1[$v->bulan][$v->kd_pj] = $v->total;
		}

		for($a=1; $a<=(date('n')-1); $a++)
		{
			$UMU['UMU'][$a] = isset($datas1[$a]['UMU']) ? $datas1[$a]['UMU'] : 0;
			$BPJ['BPJ'][$a] = isset($datas1[$a]['BPJ']) ? $datas1[$a]['BPJ'] : 0;
		}


		$res_status = $this->Booking_model->get('','
			sum(limit_reg) as total, 
			REPLACE(status," ","") as status,
			MONTH(tanggal_periksa) as bulan',
			array('YEAR(tanggal_periksa)'=>date('Y'),'kd_dokter'=>$this->session->userdata('id_user')),
			'booking_registrasi.status,MONTH(booking_registrasi.tanggal_periksa)')
		->result();

		foreach ($res_status as $k => $v) {
			$datas2[$v->bulan][$v->status] = $v->total;
		}

		for($b=1; $b<=(date('n')-1); $b++)
		{
			$belum['Belum'][$b] = isset($datas2[$b]['Belum']) ? $datas2[$b]['Belum'] : 0;
			$terdaftar['Terdaftar'][$b] = isset($datas2[$b]['Terdaftar']) ? $datas2[$b]['Terdaftar'] : 0;
			$batal['Batal'][$b] = isset($datas2[$b]['Batal']) ? $datas2[$b]['Batal'] : 0;
			$dokter_berhalangan['DokterBerhalangan'][$b] = isset($datas2[$b]['DokterBerhalangan']) ? $datas2[$b]['DokterBerhalangan'] : 0;
		}

		$res_kd_poli = $this->Booking_model->get('',
			'sum(limit_reg) as total, 
			REPLACE(status," ","") as status,
			YEAR(tanggal_periksa) as tahun',
			array('kd_dokter'=>$this->session->userdata('id_user')),
			'booking_registrasi.status,YEAR(tanggal_periksa)')->result();

		foreach ($res_kd_poli as $k => $v) {
			$datas3[$v->tahun][$v->status] = $v->total;
		}

		for($c=2018; $c<=date('Y'); $c++)
		{
			$belum1['Belum'][$c] = isset($datas3[$c]['Belum']) ? $datas3[$c]['Belum'] : 0;
			$terdaftar1['Terdaftar'][$c] = isset($datas3[$c]['Terdaftar']) ? $datas3[$c]['Terdaftar'] : 0;
			$batal1['Batal'][$c] = isset($datas3[$c]['Batal']) ? $datas3[$c]['Batal'] : 0;
			$dokter_berhalangan1['DokterBerhalangan'][$c] = isset($datas3[$c]['DokterBerhalangan']) ? $datas3[$c]['DokterBerhalangan'] : 0;
		}
		$chart3 = $belum1 + $terdaftar1 + $batal1 + $dokter_berhalangan1;

		/*echo '<pre>';
		echo date('n');
		print_r($datas2);
		exit();*/
		$data = array(
			'title'		=> 'Data Statistik',
			'bpj'		=> $BPJ,
			'umu'		=> $UMU,
			'belum'		=> $belum,
			'terdaftar'	=> $terdaftar,
			'batal'		=> $batal,
			'dokter_berhalangan'=> $dokter_berhalangan,
			'chart3'		=> $chart3,
			'file'		=> 'chart',
			'folder'	=> 'booking'
		);

		$this->site->view('inc',$data);	
	}

	public function ralan()
	{
		$this->load->model(array('Rawatjalan_model','Jadwaldokter_model'));

		$get_jawdal = $this->Jadwaldokter_model->get('','kd_poli',array('kd_dokter'=>$this->session->userdata('id_user')),'kd_poli')->result();

		$where = array(
			'YEAR(tgl_registrasi)'=>date('Y'),
			'kd_dokter'=>$this->session->userdata('id_user'),
			'kd_poli' => $get_jawdal[0]->kd_poli,
			'status_lanjut' => 'Ralan'
		);

		$res_cara_bayar = $this->Rawatjalan_model->get(
			'',
			'count(no_rawat) as total, kd_pj,MONTH(tgl_registrasi) as bulan',
			$where,
			'MONTH(tgl_registrasi),kd_pj'
		)->result();

		foreach ($res_cara_bayar as $k => $v) {
			$datas1[$v->bulan][$v->kd_pj] = $v->total;
		}

		for($a=1; $a<=(date('n')-1); $a++)
		{
			$chart_cara_bayar['BPJ'][$a] = isset($datas1[$a]['BPJ']) ? $datas1[$a]['BPJ'] : 0;
			$chart_cara_bayar['JKK'][$a] = isset($datas1[$a]['JKK']) ? $datas1[$a]['JKK'] : 0;
			$chart_cara_bayar['NON'][$a] = isset($datas1[$a]['NON']) ? $datas1[$a]['NON'] : 0;
			$chart_cara_bayar['BKP'][$a] = isset($datas1[$a]['BKP']) ? $datas1[$a]['BKP'] : 0;
			$chart_cara_bayar['MCD'][$a] = isset($datas1[$a]['MCD']) ? $datas1[$a]['MCD'] : 0;
			$chart_cara_bayar['OTG'][$a] = isset($datas1[$a]['OTG']) ? $datas1[$a]['OTG'] : 0;
			$chart_cara_bayar['RSD'][$a] = isset($datas1[$a]['RSD']) ? $datas1[$a]['RSD'] : 0;
			$chart_cara_bayar['UMU'][$a] = isset($datas1[$a]['UMU']) ? $datas1[$a]['UMU'] : 0;
		}

		/*echo '<pre>';
		print_r($chart_cara_bayar);
		exit();*/
		$data = array(
			'title'		=> 'Data Statistik Rawat Jalan',
			'chart1'	=> $chart_cara_bayar,
			'file'		=> 'chart_ralan',
			'folder'	=> 'booking'
		);

		$this->site->view('inc',$data);	
	}

	public function tes()
	{
		$where = array(
			'DATE(date) >=' => '2019-06-16',
			'DATE(date) <=' => '2019-06-18',
			'divisi' => 'RAJAL'
		);
		$this->load->model(array('Tes_model'));
		$res = $this->Tes_model->get('','SUM(nilai) as a,divisi,cat',$where,'cat')->result();
		echo '<pre>';
		print_r($res);
	}
}
