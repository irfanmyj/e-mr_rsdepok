<?php defined('BASEPATH') OR exit('No direct script access allowed');
// Cek apakah sudah diterapakan untuk penyimpanan timer setiap module form

class Rawatjalan extends backend_controller {

	public function __construct(){
		parent::__construct();
		$this->load->model(array('Rawatjalan_model'));
		$this->site->is_logged_in();
	}

	public function index()
	{
		global $Cf;
		$keys 	= array('search','kd_polif');
		$datas 	= $this->instrument->_post($keys);
		$search	= isset($datas['search']) 	? $datas['search'] : '';
		$kdpolif= isset($datas['kd_polif']) 	? $datas['kd_polif'] : '';
		$limit 	= isset($post['limit']) 	? $post['limit'] : '';
		$offset = ($this->uri->segment(3)) 	? $this->uri->segment(3) : 0;
		$dNow 	= date('Y-m-d');
		$dTitle = ($search) ? $search : $dNow;
		$time 	= date('H');
		$kdpoli = getKdPoli(@$this->session->userdata('kd_poli'),$time);

		//print_r($kdpoli);
		//exit();
		$where = array(
			'reg_periksa.kd_dokter' => $this->session->userdata('id_user'),
			'reg_periksa.status_lanjut' => 'Ralan',
			'reg_periksa.tgl_registrasi' => date('Y-m-d',strtotime($dTitle)),
			'reg_periksa.kd_poli' => $kdpoli
		);

		if(!empty($search) && !empty($kdpolif))
		{
			$where = '
				reg_periksa.kd_dokter = \''.$this->session->userdata('id_user').'\'
				and reg_periksa.status_lanjut = "Ralan"
				and reg_periksa.tgl_registrasi= \''.date('Y-m-d',strtotime($dTitle)).'\'
				and reg_periksa.kd_poli=\''.$kdpolif.'\'
			';
		}
		else if(!empty($search) && empty($kdpolif))
		{
			$where = '
				reg_periksa.kd_dokter = \''.$this->session->userdata('id_user').'\'
				and reg_periksa.status_lanjut = "Ralan"
				and reg_periksa.tgl_registrasi= \''.date('Y-m-d',strtotime($dTitle)).'\'
			';
		}

		$dtkdpol = $this->session->userdata('kd_poli');
		foreach ($dtkdpol as $k => $v) {
            $ls[$v->kd_poli] = $v->kd_poli;
        }
		
		$res_booking 	= $this->Rawatjalan_model->getPaseinRajal($where,$limit,$offset);
		$data = array(
			'title'		=> 'Jumlah Data Pasien Booking',
			'titled'	=> 'Jumlah Data Pasien Booking Tanggal '. date('d M Y',strtotime($dTitle)),
			'date'		=> $dTitle,
			'row' 		=> $res_booking,
			'kd_poli' 	=> !empty($ls) ? $ls : '',
			'defaultkdpoli' => $kdpoli,
			'file'		=> 'index',
			'folder'	=> 'rawatjalan'
		);
		
		$this->site->view('inc',$data);
	}

	public function kajipasien($no_rawat,$dateNow,$noRm)
	{
		$this->load->model(array('Masteraturanpakai_model','Diagnosapasien_model','Resepobat_model','Permintaanlab_model','Permintaanradiologi_model','Pemeriksaanralan_model','Detailpemberianobat_model','Catatanperawatan_model'));
		$noRawat 	= isset($no_rawat) ? str_replace('-', '/', $no_rawat) : '';
		$dtNow 		= date('Y-m-d');
		$dNow		= isset($dateNow) ? date('Y-m-d',strtotime($dateNow)) : date('Y-m-d');
		$no_rkm_medis= isset($noRm) ? $noRm : '';
		$time 	= date('H');
		$kdpoli = getKdPoli($this->session->userdata('kd_poli'),$time);

		/*
		Start Data Pasien
		Mengambil data pribadi pasien
		*/
		$where = array(
			'reg_periksa.no_rawat' => $noRawat
		);

		$res_pasien = $this->Rawatjalan_model->getPaseinRajalAll($where);

		// End data pasien

		/*
		Start Antrian
		Mengambil data untuk urutan antrian
		*/
		if($dNow == $dtNow)
		{
			$where2 = array(
				/*'stts!=' => 'Sudah',*/
				'tgl_registrasi'=>$dNow,
				'kd_dokter'=>$this->session->userdata('id_user'),
				'kd_poli' => $kdpoli
			);
		}
		else
		{
			$where2 = array(
				'tgl_registrasi'=>$dNow,
				'kd_dokter'=>$this->session->userdata('id_user'),
				'kd_poli' => $kdpoli
			);
		}
		
		
		$res_antrian = $this->Rawatjalan_model->getPaseinRajal2($where2);

		// End Antrian
		/*
		Mengambil data no rawat dan tanggal registrasi
		*/
		
		$where3 = array('
			reg_periksa.no_rkm_medis'=>$no_rkm_medis,
			'reg_periksa.status_lanjut'=>'Ralan',
			'reg_periksa.stts!='=>'Batal'
		);
		$ls = array();
		$res_option = $this->Rawatjalan_model->getPaseinRajal3($where3);
		$data_option = $this->instrument->splitRecursiveArray($res_option,'no_rawat','tgl_registrasi');

		foreach ($res_option as $k => $v) {
            $ls[$v['no_rawat']] = date('d M Y', strtotime($v['tgl_registrasi'])) .' ('. $this->instrument->indoDays($v['tgl_registrasi']).') - '. $v['nm_poli'];
        }

        $aturan_pakai1 = $this->Masteraturanpakai_model->get('','aturan')->result_array();
        $aturan_pakai = $this->instrument->splitRecursiveArray($aturan_pakai1,'aturan','aturan');

        // Diagnosa
        $ResDiagnosa = $this->showTableDiagnosa($noRawat);

        // Obat
        $ResResepObat = $this->showTableResepObat1($noRawat);
        
        // Get Lab
        $ResLabs = $this->showTableLab($noRawat);

        // Get Rad
        $resrads = $this->showTableRad($noRawat);

        // Get Tindakan
        $restindakan = $this->showTableRiwayatTindakan($noRawat);

        // Data Pemeriksaan Perawat
        $respemeriksaan = $this->Pemeriksaanralan_model->get('','*',array('no_rawat'=>$noRawat))->result();
       
        // Catatn dokter SOAP
        $soap = $this->Catatanperawatan_model->get('','*',array('no_rawat'=>$noRawat))->result();       
        $expsoap = explode('\n', @$soap[0]->catatan);
        foreach($expsoap as $k => $v)
        {	
        	$frist = substr($v,0,1);
        	$d[strtolower($frist)] = $v;
        }
       	
        $res_norawat = $this->Rawatjalan_model->getPaseinRajal4($where3);
       	
		$data = array(
			'title' 	=> 'Halaman Informasi Tindakan',
			'titled' 	=> 'Form Pengkajian Pasien',
			'noRawat' 	=> $noRawat,
			'noRawatTerakhir' 	=> (count($res_norawat) > 1) ? $res_norawat[1]['no_rawat'] : $res_norawat[0]['no_rawat'],
			'dtpasien' 	=> $res_pasien,
			'antrian' 	=> $res_antrian,
			'driwayat' 	=> $ls,
			'no_rkm_medis'=> $no_rkm_medis,
			'aturan_pakai'=> $aturan_pakai,
			'diagnosa' => $ResDiagnosa,
			'resepobat' => $ResResepObat,
			'reslabs' => $ResLabs,
			'resrads' => $resrads,
			'pemeriksaan' => $respemeriksaan,
			'tindakan' => $restindakan,
			'soap' => $d,
			'usia' 		=> $this->instrument->getAge(date('Y-m-d'),$res_pasien[0]->tgl_lahir,array('year','month','day')),
			'file' 		=> 'kajipasien',
			'folder' 	=> 'rawatjalan'
		);
		$this->site->view('inc',$data);
	}

	public function getAntrianPasien()
	{
		$where2 = array(
			'tgl_registrasi'=>date('Y-m-d'),
			'kd_dokter'=>$this->session->userdata('id_user')
		);

		$res_antrian = $this->Rawatjalan_model->getAntrianPasien($where2)->result();
		echo json_encode($res_antrian);
	}

	public function getHistory()
	{
		$this->load->model(array('Aturanpakai_model','Laboratorium_model','Diagnosapasien_model','Detailpemberianobat_model','Hasilradiologi_model'));
		$post 			= $this->input->post();
		$no_rkm_medis 	= isset($post['no_rkm_medis']) ? $post['no_rkm_medis'] : '';
		$no_rawat 		= isset($post['no_rawat']) ? $post['no_rawat'] : '';

		/*
		Start Show Data Poliklinik
		*/
		
		// End Laboratorium
		$res_dataumum 	= $this->Rawatjalan_model->getHistoryRegperiksa(array(
			'reg_periksa.no_rawat' => $no_rawat,
			'reg_periksa.no_rkm_medis' => $no_rkm_medis,
			'reg_periksa.status_lanjut' => 'Ralan'
		));

		$html = '<div class="container h-100 py-2">';
			$html .= '<ul class="nav nav-tabs border-0" id="myTab2" role="tablist">';
				$html .= '<li class="nav-item">';
					$html .= '<a class="nav-link active border border-success border-bottom-0" id="riwayat_default-tab" data-toggle="tab" href="#riwayat_default" role="tab" aria-controls="riwayat_default" aria-selected="true">Data Pasien</a>';
				$html .= '</li>';
				$html .= '<li class="nav-item">';
					$html .= '<a class="nav-link border border-success border-bottom-0" id="riwayat_pemeriksaan-tab" data-toggle="tab" href="#riwayat_pemeriksaan" role="tab" aria-controls="riwayat_pemeriksaan" aria-selected="true">Pemeriksaan Perawat</a>';
				$html .= '</li>';
				$html .= '<li class="nav-item">';
					$html .= '<a class="nav-link border border-success border-bottom-0" id="riwayat_soap-tab" data-toggle="tab" href="#riwayat_soap" role="tab" aria-controls="riwayat_soap" aria-selected="true">Catatan Soap</a>';
				$html .= '</li>';
				$html .= '<li class="nav-item">';
					$html .= '<a class="nav-link border border-success border-bottom-0" id="riwayat_diagnosa-tab" data-toggle="tab" href="#riwayat_diagnosa" role="tab" aria-controls="riwayat_diagnosa" aria-selected="true">Diagnosa</a>';
				$html .= '</li>';
				$html .= '<li class="nav-item">';
					$html .= '<a class="nav-link border border-success border-bottom-0" id="riwayat_resep-tab" data-toggle="tab" href="#riwayat_resep" role="tab" aria-controls="riwayat_resep" aria-selected="true">Resep Obat</a>';
				$html .= '</li>';
				$html .= '<li class="nav-item">';
					$html .= '<a class="nav-link border border-success border-bottom-0" id="riwayat_laboratorium-tab" data-toggle="tab" href="#riwayat_laboratorium" role="tab" aria-controls="riwayat_laboratorium" aria-selected="true">Laboratorium</a>';
				$html .= '</li>';
				$html .= '<li class="nav-item">';
					$html .= '<a class="nav-link border border-success border-bottom-0" id="riwayat_radiologi-tab" data-toggle="tab" href="#riwayat_radiologi" role="tab" aria-controls="riwayat_radiologi" aria-selected="true">Radiologi</a>';
				$html .= '</li>';
				$html .= '<li class="nav-item">';
					$html .= '<a class="nav-link border border-success border-bottom-0" id="riwayat_tindakan-tab" data-toggle="tab" href="#riwayat_tindakan" role="tab" aria-controls="riwayat_tindakan" aria-selected="true">Tindakan</a>';
				$html .= '</li>';
			$html .= '</ul>';

			$html .= '<div class="tab-content h-75 bg-white">';
				$html .= '<div class="tab-pane h-100 p-3 active border border-primary" id="riwayat_default" role="tabpanel" aria-labelledby="riwayat_default-tab">';
					$html .= '<div class="row">';
						$html .= '<table class="table table-responsive-md">';
								$html .= '<tr>';
									$html .= '<td align="center" width="10">1.</td>';
									$html .= '<td width="150">Tanggal Periksa</td>';
									$html .= '<td width="10">:</td>';
									$html .= '<td>'.date('d M Y',strtotime($res_dataumum[0]->tgl_registrasi)) .' | Hari : '. $this->instrument->indoDays($res_dataumum[0]->tgl_registrasi) .'</td>';
									$html .= '<td align="center" width="10">3.</td>';
									$html .= '<td width="150">No Rawat</td>';
									$html .= '<td width="10">:</td>';
									$html .= '<td>'.$res_dataumum[0]->no_rawat.'</td>';
								$html .= '</tr>';
								$html .= '<tr>';
									$html .= '<td align="center" width="10">2.</td>';
									$html .= '<td width="150">Nama Dokter</td>';
									$html .= '<td width="10">:</td>';
									$html .= '<td>'.$res_dataumum[0]->nm_dokter.'</td>';
									$html .= '<td align="center" width="10">4.</td>';
									$html .= '<td width="150">Nama Poli</td>';
									$html .= '<td width="10">:</td>';
									$html .= '<td>'.$res_dataumum[0]->nm_poli.'</td>';
								$html .= '</tr>';
						$html .= '</table>';
					$html .= '</div>';
				$html .= '</div>';

				$html .= '<div class="tab-pane h-100 p-3 border border-primary" id="riwayat_pemeriksaan" role="tabpanel" aria-labelledby="riwayat_pemeriksaan-tab">';
					$html .= '<div class="row">';
						$html .= $this->tablePemeriksaan($no_rawat);
					$html .= '</table>';
					$html .= '</div>';
				$html .= '</div>';

				$html .= '<div class="tab-pane h-100 p-3 border border-primary" id="riwayat_soap" role="tabpanel" aria-labelledby="riwayat_soap-tab">';
					$html .= '<div class="row">';
						$html .= $this->tableSoap($no_rawat);
					$html .= '</table>';
					$html .= '</div>';
				$html .= '</div>';

				$html .= '<div class="tab-pane h-100 p-3 border border-primary" id="riwayat_diagnosa" role="tabpanel" aria-labelledby="riwayat_diagnosa-tab">';
					$html .= '<div class="row">';
						$html .= $this->showTableDiagnosaRiwayat($no_rawat);
					$html .= '</table>';
					$html .= '</div>';
				$html .= '</div>';

				$html .= '<div class="tab-pane h-100 p-3 border border-primary" id="riwayat_resep" role="tabpanel" aria-labelledby="riwayat_resep-tab">';
					$html .= '<div class="row">';
						$html .= $this->riwayatTableResepObat1($no_rawat);
					$html .= '</table>';
					$html .= '</div>';
				$html .= '</div>';

				$html .= '<div class="tab-pane h-100 p-3 border border-primary" id="riwayat_laboratorium" role="tabpanel" aria-labelledby="riwayat_laboratorium-tab">';
					$html .= '<div class="row">';
						$html .= $this->showTableLabRiwayat($no_rawat);
					$html .= '</table>';
					$html .= '</div>';
				$html .= '</div>';

				$html .= '<div class="tab-pane h-100 p-3 border border-primary" id="riwayat_radiologi" role="tabpanel" aria-labelledby="riwayat_radiologi-tab">';
					$html .= '<div class="row">';
						$html .= $this->showTableRadRiwayat($no_rawat);
					$html .= '</table>';
					$html .= '</div>';
				$html .= '</div>';

				$html .= '<div class="tab-pane h-100 p-3 border border-primary" id="riwayat_tindakan" role="tabpanel" aria-labelledby="riwayat_tindakan-tab">';
					$html .= '<div class="row">';
						$html .= $this->showTableRiwayatTindakan($no_rawat);
					$html .= '</table>';
					$html .= '</div>';
				$html .= '</div>';
			$html .= '</div>';
		$html .= '</div>';
		/*
		End Show Data Poliklinik
		*/

		/*
		Start Show Data Rawatinap
		*/
		$where6 = array(
			'reg_periksa.no_rawat' => $no_rawat,
			'reg_periksa.no_rkm_medis' => $no_rkm_medis,
			'reg_periksa.status_lanjut' => 'Ranap'
		);

		$field6 = '
			reg_periksa.no_rawat,
			reg_periksa.tgl_registrasi,
			pemeriksaan_ranap.keluhan,
			pemeriksaan_ranap.pemeriksaan,
			bangsal.nm_bangsal
		';

		$tbjoin6 = array(
			'pemeriksaan_ranap' => array(
				'metode' => 'inner',
				'relasi' => 'pemeriksaan_ranap.no_rawat=reg_periksa.no_rawat'
			),
			'kamar_inap' => array(
				'metode' => 'inner',
				'relasi' => 'kamar_inap.no_rawat=reg_periksa.no_rawat'
			),
			'kamar' => array(
				'metode' => 'inner',
				'relasi' => 'kamar.kd_kamar=kamar_inap.kd_kamar'
			),
			'bangsal' => array(
				'metode' => 'inner',
				'relasi' => 'bangsal.kd_bangsal=kamar.kd_bangsal'
			)
		);
		$res_dataumum2 	= $this->Rawatjalan_model->getJoin('',$tbjoin6,$field6,$where6)->result();
		if($res_dataumum2)
		{
			$html1 = '<div class="col-12 mt-3">';
				$html1 .= '<div class="card-header panel-heading no-border bg-primary" style="padding: 5px; background: blue;"><h5 class="text-white"><i class="fa fa-book"></i> Data Pasien</h5></div>';
				$html1 .= '<div class="row">';
						$html1 .= '<div class="col-12">';
							$html1 .= '<table class="table table-striped table-bordered table-responsive-md">';
								$html1 .= '<tr>';
									$html1 .= '<td align="center" width="10">1.</td>';
									$html1 .= '<td width="150">Tanggal Periksa</td>';
									$html1 .= '<td>'.date('d M Y',strtotime($res_dataumum2[0]->tgl_registrasi)) .' | Hari : '. $this->instrument->indoDays($res_dataumum2[0]->tgl_registrasi) .'</td>';
								$html1 .= '</tr>';
								$html1 .= '<tr>';
									$html1 .= '<td align="center" width="10">2.</td>';
									$html1 .= '<td width="150">No Rawat</td>';
									$html1 .= '<td>'.$res_dataumum2[0]->no_rawat.'</td>';
								$html1 .= '</tr>';
								$html1 .= '<tr>';
									$html1 .= '<td align="center" width="10">5.</td>';
									$html1 .= '<td width="150">Nama Bangsal</td>';
									$html1 .= '<td>'.$res_dataumum2[0]->nm_bangsal.'</td>';
								$html1 .= '</tr>';
								$html1 .= '<tr>';
									$html1 .= '<td align="center" width="10">5.</td>';
									$html1 .= '<td width="150">Keluhan</td>';
									$html1 .= '<td>'.$res_dataumum2[0]->keluhan.'</td>';
								$html1 .= '</tr>';
								$html1 .= '<tr>';
									$html1 .= '<td align="center" width="10">6.</td>';
									$html1 .= '<td width="150">Pemeriksaan</td>';
									$html1 .= '<td>'.$res_dataumum2[0]->pemeriksaan.'</td>';
								$html1 .= '</tr>';
							$html1 .= '</table>';
						$html1 .= '</div>';
					$html1 .= '</div>';
			$html1 .= '</div>';
		}


		$view = array(
			'poliklinik' => $html,
			'rawatinap' => isset($html1) ? $html1 : '<p class="text">Riwayat data pasien rawatinap tidak ditemukan.</p>'
		);

		echo json_encode($view);
	}



	/*
	Module Diagnosa
	*/
	public function savediagnosa()
	{
		$post = $this->input->post();
		$this->load->model(array('Diagnosapasien_model'));

		if(isset($post['kd_penyakit']))
		{
			$cek = $this->Diagnosapasien_model->get('','count(*) as jml',array('no_rawat'=>$post['no_rawat']))->result();
			$no = 1;
			foreach ($post['kd_penyakit'] as $key => $value) {
				$diagnosa[] = array(
					'no_rawat' => $post['no_rawat'],
					'kd_penyakit' => $value,
					'prioritas' => $no,
					'status_penyakit' => $post['status_penyakit'],
					'status' => 'Ralan'
				);
				$no++;
			}
			
			if($cek[0]->jml == 0)
			{
				if(!$this->Diagnosapasien_model->insert('',$diagnosa,true))
				{
					$analisis = $this->saveanalisis($post,'',2,'insert');
					$this->Analisispengguna_model->insert('',$analisis);
					$html = $this->showTableDiagnosa($post['no_rawat']);
					$error = '';
				}
				else
				{
					$error = 1;
					$html = '<span class="text-danger"><h4>Data diagnosa gagal disimpan</h4></span>';
				}
			}
			else
			{
				$this->Diagnosapasien_model->delete('',array('no_rawat'=>$post['no_rawat']));
				if(!$this->Diagnosapasien_model->insert('',$diagnosa,true))
				{
					$analisis = $this->saveanalisis($post,'',2,'update');
					$this->Analisispengguna_model->insert('',$analisis);
					$html = $this->showTableDiagnosa($post['no_rawat']);
					$error = '';
				}
				else
				{
					$error = 1;
					$html = '<span class="text-danger"><h4>Data diagnosa gagal disimpan</h4></span>';
				}
			}

			$datas = array(
				'view' => $html,
				'error' => $error
			);
			
			echo json_encode($datas);
		}
		
	}

	public function updateDiagnosa()
	{
		$this->load->model(array('Diagnosapasien_model'));
		$post = $this->input->post();
		$kd_penyakit = explode(',', $post['kd_penyakit']);
		$i = 1;
		foreach ($kd_penyakit as $k => $v) {
			$data['prioritas'] = $i; 
			$this->Diagnosapasien_model->update('',$data,array('no_rawat'=>$post['no_rawat'],'kd_penyakit'=>$v));
			$i++;
		}


		$error = '';
		$datas = array(
			'view' => $this->showTableDiagnosa($post['no_rawat']),
			'error' => $error
		); 

		echo json_encode($datas);
	}

	public function deleteDiagnosa()
	{
		$this->load->model(array('Diagnosapasien_model'));
		$post = $this->input->post();
		if($post['kd_penyakit'])
		{
			if($this->Diagnosapasien_model->delete('',array('no_rawat'=>$post['no_rawat'],'kd_penyakit'=>$post['kd_penyakit'])))
			{
				$html = $this->showTableDiagnosa($post['no_rawat']);
				$error = '';
			}
			else
			{
				$html = '<span class="text-danger">Maaf data tidak bisa dihapus.</span>';
				$error = 1;
			}

			$datas = array(
				'view' => $this->showTableDiagnosa($post['no_rawat']),
				'error' => $error
			); 

			echo json_encode($datas);
		}
	}

	private function showTableDiagnosa($no_rawat)
	{
		$field = '
			diagnosa_pasien.no_rawat,
			diagnosa_pasien.kd_penyakit,
			diagnosa_pasien.prioritas,
			diagnosa_pasien.status_penyakit,
			diagnosa_pasien.status,
			penyakit.nm_penyakit
		';

		$tbjoin = array(
			'penyakit' => array(
				'metode' => 'inner',
				'relasi' => 'penyakit.kd_penyakit=diagnosa_pasien.kd_penyakit'
			)
		);

		$where = array(
			'diagnosa_pasien.no_rawat' => $no_rawat
		);

		$res = $this->Diagnosapasien_model->getJoin('',$tbjoin,$field,$where,'','','diagnosa_pasien.prioritas ASC')->result();

		if($res)
		{
			$html = '<table class="table table-striped table-bordered table-responsive-md">';
				$html .= '<thead class="bg-success text-white">';
					$html .= '<tr>';
						$html .= '<td>No</td>';
						$html .= '<td>No Rawat</td>';
						$html .= '<td>Kode Diagnosa</td>';
						$html .= '<td>Nama Diagnosa</td>';
						$html .= '<td>Prioritas</td>';
						$html .= '<td>Aksi</td>';
					$html .= '</tr>';
				$html .= '</thead>';
				$html .= '<tbody class="tablediagnosa2">';
					$n = 1;
					foreach ($res as $k => $v) {
						$html .= '<tr id="'.$v->kd_penyakit.'" data-kd_penyakit="'.$v->kd_penyakit.'" data-no_rawat="'.$v->no_rawat.'">';
							$html .= '<td><input type="hidden" name="kd_penyakit[]" value="'.$v->kd_penyakit.'">'.$n.'</td>';
							$html .= '<td>'.$v->no_rawat.'</td>';
							$html .= '<td>'.$v->kd_penyakit.'</td>';
							$html .= '<td>'.$v->nm_penyakit.'</td>';
							$html .= '<td>'.$v->prioritas.'</td>';
							$html .= '<td style="cursor:pointer;" class="delete_diagnosa"><button type="button" class="btn btn-danger btn-sm" id="delete_diagnosa"> <i class="fa fa-delete"></>Hapus</button></td>';
						$html .= '</tr>';
						$n++;
					}
				$html .= '</tbody>';
			$html .= '</table>';
		}
		else
		{
			$html = '';
		}

		return $html;
	}

	private function showTableDiagnosaRiwayat($no_rawat)
	{
		$field = '
			diagnosa_pasien.no_rawat,
			diagnosa_pasien.kd_penyakit,
			diagnosa_pasien.prioritas,
			diagnosa_pasien.status_penyakit,
			diagnosa_pasien.status,
			penyakit.nm_penyakit
		';

		$tbjoin = array(
			'penyakit' => array(
				'metode' => 'inner',
				'relasi' => 'penyakit.kd_penyakit=diagnosa_pasien.kd_penyakit'
			)
		);

		$where = array(
			'diagnosa_pasien.no_rawat' => $no_rawat
		);

		$res = $this->Diagnosapasien_model->getJoin('',$tbjoin,$field,$where,'','','diagnosa_pasien.prioritas ASC')->result();

		if($res)
		{
			$html = '<table class="table table-striped table-bordered table-responsive-md">';
				$html .= '<thead class="bg-success text-white">';
					$html .= '<tr>';
						$html .= '<td>No</td>';
						$html .= '<td>No Rawat</td>';
						$html .= '<td>Kode Diagnosa</td>';
						$html .= '<td>Nama Diagnosa</td>';
						$html .= '<td>Prioritas</td>';
						//$html .= '<td>Aksi</td>';
					$html .= '</tr>';
				$html .= '</thead>';
				$html .= '<tbody class="tabledianosa2">';
					$n = 1;
					foreach ($res as $k => $v) {
						$html .= '<tr id="'.$v->kd_penyakit.'" data-kd_penyakit="'.$v->kd_penyakit.'" data-no_rawat="'.$v->no_rawat.'">';
							$html .= '<td><input type="hidden" name="kd_penyakit[]" value="'.$v->kd_penyakit.'">'.$n.'</td>';
							$html .= '<td>'.$v->no_rawat.'</td>';
							$html .= '<td>'.$v->kd_penyakit.'</td>';
							$html .= '<td>'.$v->nm_penyakit.'</td>';
							$html .= '<td>'.$v->prioritas.'</td>';
							//$html .= '<td style="cursor:pointer;" class="delete_diagnosa"><button type="button" class="btn btn-danger btn-sm" id="delete_diagnosa"> <i class="fa fa-delete"></>Hapus</button></td>';
						$html .= '</tr>';
						$n++;
					}
				$html .= '</tbody>';
			$html .= '</table>';
		}
		else
		{
			$html = '<p class="text-bold text-center">Data tidak tersedia.</p>';
		}

		return $html;
	}

	public function getDiagnosa()
	{
		$this->load->model(array('Diagnosapenyakit_model'));
		$keys 	= $_GET['term'];
		$count  = strlen($keys);
		if($count == 3)
		{
			$awal = substr($keys, 0,3);
			$akhir = substr($keys, 3);
			$search = $awal.'.'.$akhir;
		}
		else
		{
			if($count >= 4)
			{
				$search = $keys;
			}
		}
		
		/*print_r($keys);
		exit();*/
		if(isset($keys))
		{
			$where = 'kd_penyakit like \'%'.strtoupper($search).'%\' OR nm_penyakit like \'%'.strtoupper($search).'%\' OR ciri_ciri like \'%'.strtoupper($search).'%\'';
			$res = $this->Diagnosapenyakit_model->get('','kd_penyakit,nm_penyakit',$where,'','','kd_penyakit ASC')->result();
			if(count($res) > 0)
			{
				$res_diagnosa = array();
				$no=0;
				foreach($res as $key => $value)
				{
					$no++;
					$res_diagnosa[] = array(
						'keys' => str_replace(".", '', $value->kd_penyakit),
						'id' => $value->kd_penyakit,
						'value' => '('.$value->kd_penyakit.') '. $value->nm_penyakit
					);
				}
			}
			else
			{
				$res_diagnosa[] = array(
					'keys' => 'No',
					'id' => 'No',
					'value' => 'Data tidak ditemukan.'
				);
			}	
			
		}
		echo json_encode($res_diagnosa);
	}

	public function getDiagnosavoice()
	{
		$this->load->model(array('Diagnosapenyakit_model'));
		$keys 	= $_GET['term'];
		$count  = strlen($keys);
		if($count == 3)
		{
			$awal = substr($keys, 0,3);
			$akhir = substr($keys, 3);
			$search = $awal.'.'.$akhir;
		}
		else
		{
			if($count >= 4)
			{
				$search = $keys;
			}
		}
		
		/*print_r($keys);
		exit();*/
		if(isset($keys))
		{
			$where = 'kd_penyakit like \'%'.strtoupper($search).'%\' OR nm_penyakit like \'%'.strtoupper($search).'%\' OR ciri_ciri like \'%'.strtoupper($search).'%\'';
			$res = $this->Diagnosapenyakit_model->get('','kd_penyakit,nm_penyakit',$where,'','','kd_penyakit ASC')->result();
			if(count($res) > 0)
			{
				$res_diagnosa = array();
				$no=0;
				foreach($res as $key => $value)
				{
					$no++;
					$res_diagnosa[] = array(
						'keys' => str_replace(".", '', $value->kd_penyakit),
						'id' => $value->kd_penyakit,
						'value' => '('.$value->kd_penyakit.') '. $value->nm_penyakit
					);
				}
			}
			else
			{
				$res_diagnosa[] = array(
					'keys' => 'No',
					'id' => 'No',
					'value' => 'Data tidak ditemukan.'
				);
			}	
			
			$no = 1;
			echo '<table class="table table-bordered" id="resdiagnosa">';
				echo '<thead>
					<tr>
						<td>No</td>
						<td>Kata Kunci</td>
						<td>Nama</td>
					</tr>
				</thead>';
				echo '<tbody>';
				foreach($res_diagnosa as $key => $value) {
					echo '<tr style="cursor:pointer" data-id="'.$value['id'].'" data-keys="'.$value['keys'].'" data-nama="'.$value['value'].'" class="pilihdiagnosa">';
						echo '<td class="tr_mod">'.($no++).'</td>';
						echo '<td class="tr_mod">'.$value['keys'].'</td>';
						echo '<td class="tr_mod">'.$value['value'].'</td>';
					echo '</tr>';
				}	
				echo '</tbody>';	
			echo '</table>';	
		}
	}

	public function history_diagnosa_last(){
		$this->load->model('Diagnosapasien_model');
		$post = $this->input->post();
		$field = '
			diagnosa_pasien.no_rawat,
			diagnosa_pasien.kd_penyakit,
			diagnosa_pasien.prioritas,
			diagnosa_pasien.status_penyakit,
			diagnosa_pasien.status,
			penyakit.nm_penyakit
		';

		$tbjoin = array(
			'penyakit' => array(
				'metode' => 'inner',
				'relasi' => 'penyakit.kd_penyakit=diagnosa_pasien.kd_penyakit'
			)
		);

		$where = array(
			'diagnosa_pasien.no_rawat' => $post['no_rawat']
		);

		$res = $this->Diagnosapasien_model->getJoin('',$tbjoin,$field,$where,'','','diagnosa_pasien.prioritas ASC')->result();
		echo '<div class="border bg-light col-11 pt-2">';
		echo '<div class="form-group">';
			echo '<label><h5>Data Riwayat Diagnosis Masa Lalu</h5></label>';
		echo '</div>';
		echo '<hr>';
		echo '<table class="table table-striped table-responsive-md" id="list-datadignosa-riwayat">';
		echo '<tbody>';
		foreach ($res as $k => $v) {
			echo '<tr id="'.str_replace('.','',$v->kd_penyakit).'" style="cursor:pointer;" class="pilihdiagnosa" data-id="'.$v->kd_penyakit.'" data-keys="'.str_replace('.','',$v->kd_penyakit).'" data-nama="'.'('.$v->kd_penyakit.') '.$v->nm_penyakit.'">';
				echo '<td class="tr_mod"><h6>('.$v->kd_penyakit.') '.$v->nm_penyakit.'</h6></td>';
				echo '<td><h6><i class="fa fa-transfer"> Pindahkan</i></h6></td>';
			echo '</tr>';
		}	
		echo '</tbody>';
		echo '</table>';
		echo '</div>';
	}
	/*
	End Diagnosa Module
	*/

	/*
	Module Resep Obat
	*/
	public function saveResepObat()
	{
		$models = array(
			'Resepobat_model',
			'Resepdokter_model'
		);

		$this->load->model($models);
		$post = $this->input->post();
		$tgl = $post['tgl_perawatan'];
		$data = implode('","', $post['kode_obat']);
		$where = 'kode_brng IN ("'.$data.'")';

		/*print_r($where);
		exit();*/
		$res_noresep = $this->Resepobat_model->get('','no_resep',array('no_rawat'=>$post['no_rawat']),'','','',1)->result();
		$jmlresepdokter = count(@$post['kode_obat']);
		
	}

	public function saveResepObat1()
	{
		$models = array(
			'Resepobat_model',
			'Resepdokter_model'
		);

		$this->load->model($models);
		$post = $this->input->post();
		$tgl = $post['tgl_perawatan'];

		$res_noresep = $this->Resepobat_model->get('','no_resep',array('no_rawat'=>$post['no_rawat']),'','','',1)->result();
		$jmlresepdokter = count(@$post['kode_obat']);

		if(@$res_noresep[0]->no_resep)
		{
			if(isset($post['kode_obat']))
			{
				if($jmlresepdokter >= 2)
				{
					$jml_resep_dokter = $jmlresepdokter - 1;
					$no_resep = $res_noresep[0]->no_resep;

					for($rd=0; $rd<=$jml_resep_dokter; $rd++)
					{
						$aturan  = cekAturanPakai($post['aturan_pakai'][$rd],$post['keterangan'][$rd]);

						$resep_dokter[] = array(
							'no_resep' => $no_resep,
							'kode_brng' => @$post['kode_obat'][$rd],
							'jml' => @$post['jlm_obat'][$rd],
							'aturan_pakai' => $aturan
						);
					}

					$this->Resepdokter_model->delete('',array('no_resep'=>$no_resep));
					if(!$this->Resepdokter_model->insert('',$resep_dokter,TRUE))
					{
						$analisis = $this->saveanalisis($post,'',3,'update');
						$this->Analisispengguna_model->insert('',$analisis);
						$error = '';
						$html = $this->showTableResepObat1($post['no_rawat']);
					}	
					else
					{
						$error = 1;
						$html = '<span class="text-danger">Data gagal disimpan.</span>';
					}				
				}
				else if($jmlresepdokter == 1)
				{
					$jml_resep_dokter = $jmlresepdokter - 1;
					$no_resep = $res_noresep[0]->no_resep;

					for($rd=0; $rd<=$jml_resep_dokter; $rd++)
					{
						$aturan  = cekAturanPakai($post['aturan_pakai'][$rd],$post['keterangan'][$rd]);

						$resep_dokter[] = array(
							'no_resep' => $no_resep,
							'kode_brng' => @$post['kode_obat'][$rd],
							'jml' => @$post['jlm_obat'][$rd],
							'aturan_pakai' => $aturan
						);
					}

					$this->Resepdokter_model->delete('',array('no_resep'=>$no_resep));
					if(!$this->Resepdokter_model->insert('',$resep_dokter,TRUE))
					{
						$analisis = $this->saveanalisis($post,'',3,'update');
						$this->Analisispengguna_model->insert('',$analisis);
						$error = '';
						$html = $this->showTableResepObat1($post['no_rawat']);
					}	
					else
					{
						$error = 1;
						$html = '<span class="text-danger">Data gagal disimpan.</span>';
					}
				}
			}
			else
			{
				$error = 1;
				$html = '<span class="text-danger">Tidak ada data yang dikirim.</span>';
			}
		}
		else
		{
			
			//ifnull(MAX(CONVERT(RIGHT(no_resep,10),signed)),0)
			$get_number_resep = $this->Resepobat_model->get('','ifnull(MAX(CONVERT(RIGHT(no_resep,4),signed)),0) as no_resep',array('tgl_perawatan'=>$tgl))->result();
			$no_resep = getNumberLab($get_number_resep,$tgl,'obat');

			if(isset($post['kode_obat']))
			{
				if($jmlresepdokter >=2)
				{
					$jml_resep_dokter = $jmlresepdokter -1;
					$resep_obat = array(
						'no_resep' => $no_resep,
						'tgl_perawatan' => $tgl,
						'jam' => date('H:i:s'),
						'no_rawat' => $post['no_rawat'],
						'kd_dokter' => $post['kd_dokter'],
						'tgl_peresepan' => $tgl,
						'jam_peresepan' => date('H:i:s'),
						'status' => 'ralan'
					);
					$this->Resepobat_model->insert('',$resep_obat);

					//$resep_dokter = array();
					for($rd=0; $rd<=$jml_resep_dokter; $rd++)
					{
						$aturan  = cekAturanPakai($post['aturan_pakai'][$rd],$post['keterangan'][$rd]);

						$resep_dokter[] = array(
							'no_resep' => $no_resep,
							'kode_brng' => @$post['kode_obat'][$rd],
							'jml' => @$post['jlm_obat'][$rd],
							'aturan_pakai' => $aturan
						);
					}

					if(!$this->Resepdokter_model->insert('',$resep_dokter,TRUE))
					{
						$analisis = $this->saveanalisis($post,'',3,'insert');
						$this->Analisispengguna_model->insert('',$analisis);
						$error = '';
						$html = $this->showTableResepObat1($post['no_rawat']);
					}
					else
					{
						$error = 1;
						$html = '<span class="text-danger">Data gagal disimpan.</span>';
					}
				}
				else if($jmlresepdokter == 1)
				{
					$jml_resep_dokter = $jmlresepdokter -1;
										
					$resep_obat = array(
						'no_resep' => $no_resep,
						'tgl_perawatan' => $tgl,
						'jam' => date('H:i:s'),
						'no_rawat' => $post['no_rawat'],
						'kd_dokter' => $post['kd_dokter'],
						'tgl_peresepan' => $tgl,
						'jam_peresepan' => date('H:i:s'),
						'status' => 'ralan'
					);
					$this->Resepobat_model->insert('',$resep_obat);

					$resep_dokter = array();
					for($rd=0; $rd<=$jml_resep_dokter; $rd++)
					{
						$aturan  = cekAturanPakai($post['aturan_pakai'][$rd],$post['keterangan'][$rd]);

						$resep_dokter[] = array(
							'no_resep' => $no_resep,
							'kode_brng' => @$post['kode_obat'][$rd],
							'jml' => @$post['jlm_obat'][$rd],
							'aturan_pakai' => $aturan
						);
					}

					if(!$this->Resepdokter_model->insert('',$resep_dokter,TRUE))
					{
						$analisis = $this->saveanalisis($post,'',3,'insert');
						$this->Analisispengguna_model->insert('',$analisis);
						$error = '';
						$html = $this->showTableResepObat1($post['no_rawat']);
					}
					else
					{
						$error = 1;
						$html = '<span class="text-danger">Data gagal disimpan.</span>';
					}
				}
			}
			else
			{
				$error = 1;
				$html = '<span class="text-danger">Tidak ada data yang dikirim.</span>';
			}
		}
		
		$datas = array(
			'view' => $html,
			'error' => $error
		);
		echo json_encode($datas);
	}

	public function deleteResepobat()
	{
		$this->load->model(array('Detailpemberianobat_model','Aturanpakai_model'));
		$post = $this->input->post();
		//print_r($post);
		if($post['no_rawat'])
		{
			if($this->Detailpemberianobat_model->delete('',array('no_rawat'=>$post['no_rawat'],'kode_brng'=>$post['kode_brng'])))
			{
				$this->Aturanpakai_model->delete('',array('no_rawat'=>$post['no_rawat'],'kode_brng'=>$post['kode_brng']));
				$html = $this->showTableResepObat1($post['no_rawat']);
				$error = '';
			}
			else
			{
				$html = '<span class="text-danger">Maaf data tidak bisa dihapus.</span>';
				$error = 1;
			}

			$datas = array(
				'view' => $html,
				'error' => $error
			); 

			echo json_encode($datas);
		}
	}

	public function deleteResepobat1()
	{
		$this->load->model(array('Resepobat_model','Resepdokter_model'));
		$post = $this->input->post();
		$getResepDokter = $this->Resepdokter_model->get('','count(*) as total',array('no_resep'=>$post['no_resep']))->result();
		//print_r($post);
		if($getResepDokter[0]->total > 1)
		{
			//echo $getResepDokter[0]->total .' / lebih';
			if($post['no_rawat'])
			{
				if($this->Resepdokter_model->delete('',array('no_resep'=>$post['no_resep'],'kode_brng'=>$post['kode_brng'])))
				{
					//$this->Aturanpakai_model->delete('',array('no_rawat'=>$post['no_rawat'],'kode_brng'=>$post['kode_brng']));
					$html = $this->showTableResepObat1($post['no_rawat']);
					$error = '';
				}
				else
				{
					$html = '<span class="text-danger">Maaf data tidak bisa dihapus.</span>';
					$error = 1;
				}

				$datas = array(
					'view' => $html,
					'error' => $error
				); 

				echo json_encode($datas);
			}
		}
		else
		{
			//echo $getResepDokter[0]->total .' / kurang';
			if($post['no_rawat'])
			{
				if($this->Resepdokter_model->delete('',array('no_resep'=>$post['no_resep'],'kode_brng'=>$post['kode_brng'])))
				{
					$this->Resepobat_model->delete('',array('no_rawat'=>$post['no_rawat'],'no_resep'=>$post['no_resep']));
					$html = $this->showTableResepObat1($post['no_rawat']);
					$error = '';
				}
				else
				{
					$html = '<span class="text-danger">Maaf data tidak bisa dihapus.</span>';
					$error = 1;
				}

				$datas = array(
					'view' => $html,
					'error' => $error
				); 

				echo json_encode($datas);
			}			
		}
	}

	private function showTableResepObat($no_rawat)
	{
		$this->load->model(array('Detailpemberianobat_model'));
        $ResResepObat = $this->Detailpemberianobat_model->getDataObat($no_rawat);
        
		$html = '<table class="table table-striped table-bordered table-responsive-md">';
					$html .= '<thead>';
						$html .= '<tr>';
							$html .= '<td width="10">No</td>';
							$html .= '<td>Nama Obat</td>';
							//$html .= '<td>Harga</td>';
							$html .= '<td>Jumlah</td>';
							//$html .= '<td>Total</td>';
							$html .= '<td>Aturan Pakai</td>';
							$html .= '<td width="50">Aksi</td>';
						$html .= '</tr>';
					$html .= '</thead>';
					$html .= '<tbody class="tabledianosa2">';
						$n = 1;
						foreach ($ResResepObat as $k => $v) {
							$atruan = (!empty($v->aturan)) ? explode(',', $v->aturan) : '';
							$html .= '<tr data-no_rawat="'.$v->no_rawat.'" data-kode_brng="'.$v->kode_brng.'" data-tgl_perawatan="'.$v->tgl_perawatan.'">';
								$html .= '<input type="hidden" name="kode_obat[]" value="'.$v->kode_brng.'">';
								$html .= '<input type="hidden" name="jlm_obat[]" value="'.$v->jml.'">';
								$html .= '<input type="hidden" name="aturan_pakai[]" value="'.@$atruan[0].'">';
								$html .= '<input type="hidden" name="keterangan[]" value="'.@$atruan[1].'">';
								$html .= '<td>'.$n.'</td>';
								$html .= '<td>'.$v->nama_brng.'</td>';
								//$html .= '<td>'.$v->biaya_obat.'</td>';
								$html .= '<td>'.$v->jml.'</td>';
								//$html .= '<td>'.$v->total.'</td>';
								$html .= '<td>'.$v->aturan.'</td>';
								$html .= '<td style="cursor:pointer;" class="deleteResepObat"><button type="button" class="btn btn-danger btn-sm"> Hapus</button></td>';
							$html .= '</tr>';
							$n++;
						}
					$html .= '</tbody>';
				$html .= '</table>';

		return $html;
	}

	private function riwayatTableResepObat1($no_rawat)
	{
		$this->load->model(array('Resepobat_model'));
         $ResResepObat = $this->Resepobat_model->getDataObat(array(
			'resep_obat.no_rawat' => $no_rawat
		));

		if($ResResepObat)
		{
			$html = '<table class="table table-striped table-responsive-md">';
				$html .= '<thead>';
					$html .= '<tr>';
						$html .= '<td width="10">No</td>';
						$html .= '<td>Nama Obat</td>';
						//$html .= '<td>Harga</td>';
						$html .= '<td>Jumlah</td>';
						//$html .= '<td>Total</td>';
						$html .= '<td>Aturan Pakai</td>';
						//$html .= '<td width="50">Aksi</td>';
					$html .= '</tr>';
				$html .= '</thead>';
				$html .= '<tbody class="tabledianosa2">';
					$n = 1;
					foreach ($ResResepObat as $k => $v) {
						$atruan = (!empty($v->aturan_pakai)) ? explode(',', $v->aturan_pakai) : '';
						$html .= '<tr data-kode_brng="'.$v->kode_brng.'">';
							$html .= '<input type="hidden" name="kode_obat[]" value="'.$v->kode_brng.'">';
							$html .= '<input type="hidden" name="jlm_obat[]" value="'.$v->jml.'">';
							$html .= '<input type="hidden" name="aturan_pakai[]" value="'.@$atruan[0].'">';
							$html .= '<input type="hidden" name="keterangan[]" value="'.@$atruan[1].'">';
							$html .= '<td>'.$n.'</td>';
							$html .= '<td>'.$v->nama_brng.'</td>';
							//$html .= '<td>'.$v->biaya_obat.'</td>';
							$html .= '<td>'.$v->jml.'</td>';
							//$html .= '<td>'.$v->total.'</td>';
							$html .= '<td>'.$v->aturan_pakai.'</td>';
							//$html .= '<td style="cursor:pointer;" class="deleteResepObat"><button type="button" class="btn btn-danger btn-sm"> Hapus</button></td>';
						$html .= '</tr>';
						$n++;
					}
				$html .= '</tbody>';
		}
		else
		{
			$html = '<p class="text-bold text-center">Data tidak tersedia.</p>';
		}

		return $html;
	}

	public function getObat()
	{
		$this->load->model(array('Databarangobat_model'));
		$keys 	= $_GET['search'];
		if(isset($keys))
		{
			$tbjoin = array(
				'gudangbarang' => array(
					'metode' => 'inner',
					'relasi' => 'gudangbarang.kode_brng=databarang.kode_brng'
				)
			);

			$where = 'databarang.kode_brng like \'%'.strtoupper($keys).'%\' OR databarang.nama_brng like \'%'.strtoupper($keys).'%\' and gudangbarang.kd_bangsal="B0021"';
			$field = '
				databarang.kode_brng,
				databarang.nama_brng,
				gudangbarang.stok
			';

			$res = $this->Databarangobat_model->getJoin('',$tbjoin,$field,$where)->result();
			if(count($res) > 0)
			{
				$res_obat = array();
				$no=0;
				foreach($res as $key => $value)
				{
					$no++;
					$res_obat[] = array(
						'id' => $value->kode_brng,
						'text' => $value->nama_brng . ' - (Sisa Stok = '.$value->stok.')',
						'stok' => $value->stok
					);
				}
			}
			else
			{
				$res_obat[] = array(
					'id' => 'No',
					'text' => 'Data tidak ditemukan.',
					'stok' => '0'
				);
			}	
			echo json_encode($res_obat);			
		}
	}

	public function getHargaObat()
	{
		$this->load->model(array('Databarangobat_model','Gudangbarang_model','Detailpemberianobat_model','Aturanpakai_model'));
		$post = $this->input->post();
		
		$html = '';
		if($post)
		{
			$kode_obat 	= implode('","', $post['kode_obat']);
			$jml_obat 	= implode('","', $post['jlm_obat']);
			$where1 		= 'kode_brng IN ("'.$kode_obat.'")';
			$where2 	= 'kode_brng IN ("'.$kode_obat.'") and kd_bangsal="Far2"';
			$where3 	= 'no_rawat=\''.$post['no_rawat'].'\'';

			$resGb = $this->Gudangbarang_model->get('','kode_brng,stok',$where2,'kode_brng')->result();
			$res = $this->Databarangobat_model->get('','ralan,nama_brng,kode_brng',$where1,'kode_brng')->result();
			$resDo = $this->Detailpemberianobat_model->get('','*',$where3,'kode_brng')->result();

			$dtpost = array();
			$dtupdate = array();

			foreach ($post['jlm_obat'] as $key => $value) {
				$dtpost[$post['kode_obat'][$key]] = array(
					'jml_obat' => $value,
					'kode_obat' => $post['kode_obat'][$key],
					'aturan_pakai' => $post['aturan_pakai'][$key] . ', '. $post['keterangan'][$key]
				);
			}
			
			foreach ($res as $k1 => $v1) {
				$has[$v1->kode_brng] = array('harga' => $v1->ralan,'nama_brng'=>$v1->nama_brng);
			}

			foreach ($resGb as $k1 => $v1) {
				$stok[$v1->kode_brng] = $v1->stok;
			}

			foreach ($has as $k2 => $v2) {
				$has2[] = array(
					'tgl_perawatan' => $post['tgl_perawatan'],
					'jam' => date('H:i:s'),
					'no_rawat' => $post['no_rawat'],
					'kode_brng' => $dtpost[$k2]['kode_obat'],
					'h_beli' => $v2['harga'],
					'biaya_obat' => $v2['harga'],
					'jml' => $dtpost[$k2]['jml_obat'],
					'total' => $dtpost[$k2]['jml_obat']*$v2['harga'],
					'status' => 'Ralan',
					'kd_bangsal' => 'Far2'
				);
			}

			foreach ($has as $k2 => $v2) {
				$has3[] = array(
					'tgl_perawatan' => $post['tgl_perawatan'],
					'jam' => date('H:i:s'),
					'kode_brng' => $dtpost[$k2]['kode_obat'],
					'no_rawat' => $post['no_rawat'],
					'aturan' => $dtpost[$k2]['aturan_pakai']
				);
			}

			if($resDo)
			{
				$this->Detailpemberianobat_model->delete('',array('no_rawat'=>$post['no_rawat']));
				$this->Aturanpakai_model->delete('',array('no_rawat'=>$post['no_rawat']));
				$this->Detailpemberianobat_model->insert('',$has2,true);
				$this->Aturanpakai_model->insert('',$has3,true);
			}
			else
			{
				$this->Detailpemberianobat_model->insert('',$has2,true);
				$this->Aturanpakai_model->insert('',$has3,true);
			}
			


		}

		$datas = array(
			'view' => $this->showTableResepObat($post['no_rawat']),
			'error' => ''
		);
		echo json_encode($datas);
	}

	public function delHargaObat()
	{
		$this->load->model(array('Databarangobat_model','Gudangbarang_model'));
		$post = $this->input->post();

		echo '<pre>';
		print_r($post);
		/*$html = '';
		if($post)
		{
			$data = implode('","', $post['kode_obat']);
			$where = 'kode_brng IN ("'.$data.'")';
			$where2 = 'kode_brng IN ("'.$data.'") and kd_bangsal="Far2"';
			$res = $this->Databarangobat_model->get('','ralan,nama_brng,kode_brng',$where,'kode_brng')->result();
			$resGb = $this->Gudangbarang_model->get('','kode_brng,stok',$where2,'kode_brng')->result();
			
			$dtpost = array();
			$dtupdate = array();
			foreach ($post['jlm_obat'] as $key => $value) {
				$dtpost[$post['kode_obat'][$key]] = $value;
			}
			
			foreach ($res as $k1 => $v1) {
				$has[$v1->kode_brng] = array('harga' => $v1->ralan,'nama_brng'=>$v1->nama_brng);
			}

			foreach ($resGb as $k1 => $v1) {
				$stok[$v1->kode_brng] = $v1->stok;
				//$this->Gudangbarang_model->update('',array('stok'=>$v1->stok + $dtpost[$v1->kode_brng]),array('kode_brng'=>$v1->kode_brng,'kd_bangsal'=>'Far2'));
			}

			foreach ($has as $k2 => $v2) {
				$has1[] = array('kode_brng' => $k2,'jumlah' => $dtpost[$k2],'total' => $dtpost[$k2]*$v2['harga'],'nama_brng'=>$v2['nama_brng'],'stok'=>$stok[$k2],'sisa_stok' => $stok[$k2] - $dtpost[$k2]);
			}

			print_r($has1);
			exit();
			$html = '<table class="table table-striped table-bordered table-responsive-md">';
				$html .= '<thead class="thead-dark">';
					$html .= '<tr>';
						$html .= '<th width="10">No</th>';
						$html .= '<th>Nama Obat</th>';
						$html .= '<th>Jumlah</th>';
						$html .= '<th>Stok Awal</th>';
						$html .= '<th>Sisa Stok Obat</th>';
						$html .= '<th>Total</th>';
					$html .= '</tr>';
				$html .= '</thead>';
				$html .= '<tbody>';
					$n = 1;
					$tot = array();
					foreach ($has1 as $k => $v) {
						$tot[] = $v['total'];
						$html .= '<tr>';
							$html .= '<td>'.$n.'</td>';
							$html .= '<td>'.$v['nama_brng'].'</td>';
							$html .= '<td>'.$v['jumlah'].'</td>';
							$html .= '<td>'.$v['stok'].'</td>';
							$html .= '<td>'.$v['sisa_stok'].'</td>';
							$html .= '<td>Rp. '.number_format($v['total'],2,',','.').'</td>';
						$html .= '</tr>';
						$n++;
					}
					$html .= '<tr>';
						$html .= '<td colspan="5">Total Harga</td>';
						$html .= '<td>Rp. '.number_format(array_sum($tot),2,',','.').'</td>';
					$html .= '</tr>';
				$html .= '</tbody>';
			$html .= '</table>';
		}

		echo $html;*/
	}

	public function getHargaObatOld()
	{
		$this->load->model(array('Databarangobat_model','Gudangbarang_model'));
		$post = $this->input->post();
		$html = '';
		if($post)
		{
			$data = implode('","', $post['kode_obat']);
			$where = 'kode_brng IN ("'.$data.'")';
			$where2 = 'kode_brng IN ("'.$data.'") and kd_bangsal="Far2"';
			$res = $this->Databarangobat_model->get('','ralan,nama_brng,kode_brng',$where,'kode_brng')->result();
			$resGb = $this->Gudangbarang_model->get('','kode_brng,stok',$where2,'kode_brng')->result();
			
			$dtpost = array();
			$dtupdate = array();
			foreach ($post['jlm_obat'] as $key => $value) {
				$dtpost[$post['kode_obat'][$key]] = $value;
			}
			
			foreach ($res as $k1 => $v1) {
				$has[$v1->kode_brng] = array('harga' => $v1->ralan,'nama_brng'=>$v1->nama_brng);
			}

			foreach ($resGb as $k1 => $v1) {
				$stok[$v1->kode_brng] = $v1->stok;
				$this->Gudangbarang_model->update('',array('stok'=>$v1->stok - $dtpost[$v1->kode_brng]),array('kode_brng'=>$v1->kode_brng,'kd_bangsal'=>'Far2'));
			}

			foreach ($has as $k2 => $v2) {
				$has1[] = array('kode_brng' => $k2,'jumlah' => $dtpost[$k2],'total' => $dtpost[$k2]*$v2['harga'],'nama_brng'=>$v2['nama_brng'],'stok'=>$stok[$k2],'sisa_stok' => $stok[$k2] - $dtpost[$k2]);
			}

			/*print_r($has1);
			exit();*/
			$html = '<table class="table table-striped table-bordered table-responsive-md">';
				$html .= '<thead class="thead-dark">';
					$html .= '<tr>';
						$html .= '<th width="10">No</th>';
						$html .= '<th>Nama Obat</th>';
						$html .= '<th>Jumlah</th>';
						$html .= '<th>Stok Awal</th>';
						$html .= '<th>Sisa Stok Obat</th>';
						$html .= '<th>Total</th>';
					$html .= '</tr>';
				$html .= '</thead>';
				$html .= '<tbody>';
					$n = 1;
					$tot = array();
					foreach ($has1 as $k => $v) {
						$tot[] = $v['total'];
						$html .= '<tr>';
							$html .= '<td>'.$n.'</td>';
							$html .= '<td>'.$v['nama_brng'].'</td>';
							$html .= '<td>'.$v['jumlah'].'</td>';
							$html .= '<td>'.$v['stok'].'</td>';
							$html .= '<td>'.$v['sisa_stok'].'</td>';
							$html .= '<td>Rp. '.number_format($v['total'],2,',','.').'</td>';
						$html .= '</tr>';
						$n++;
					}
					$html .= '<tr>';
						$html .= '<td colspan="5">Total Harga</td>';
						$html .= '<td>Rp. '.number_format(array_sum($tot),2,',','.').'</td>';
					$html .= '</tr>';
				$html .= '</tbody>';
			$html .= '</table>';
		}

		echo $html;
	}

	public function getStatusObat()
	{
		$this->load->model(array('Databarangobat_model'));
		$post = $this->input->post();
		$tbjoin = array(
			'gudangbarang' => array(
				'metode' => 'inner',
				'relasi' => 'gudangbarang.kode_brng=databarang.kode_brng'
			)
		);

		$where = 'databarang.kode_brng="'.$post['kode_brng'].'" and gudangbarang.kd_bangsal="'.$post['kd_bangsal'].'"';
		$field = '
			databarang.kode_brng,
			databarang.nama_brng,
			gudangbarang.stok
		';

		$res = $this->Databarangobat_model->getJoin('',$tbjoin,$field,$where)->result();
		
		if($res[0]->stok == 0)
		{
			$html = $this->informasi_stok_obat($res);
			$msg = 'Informasi, bahwa obat ini tidak ada stok...';
		}
		else
		{
			$html = '';
			$msg = '';
		}

		echo json_encode(array('msg' => $msg,'status'=>$html));
	}

	private function informasi_stok_obat($res)
	{
		$html = '<table class="table table-bordered table-striped tblInputO">';
			$html .= '<thead>';
				$html .= '<tr>';
					$html .= '<th>No</th>';
					$html .= '<th>Kode Barang</th>';
					$html .= '<th>Nama Barang</th>';
					$html .= '<th>JumlahStok</th>';
				$html .= '</tr>';
			$html .= '</thead>';
			$html .= '<tbody>';
				$html .= '<tr>';
					$html .= '<td>1</td>';
					$html .= '<td>'.$res[0]->kode_brng.'</td>';
					$html .= '<td>'.$res[0]->nama_brng.'</td>';
					$html .= '<td>'.$res[0]->stok.'</td>';
				$html .= '</tr>';
			$html .= '</tbody>';
		$html .= '</table>';
		return $html;
	}

	private function showTableResepObat1($no_rawat)
	{
		$this->load->model(array('Resepobat_model'));
		

        $ResResepObat = $this->Resepobat_model->getDataObat(array(
			'resep_obat.no_rawat' => $no_rawat
		));

		if($ResResepObat)
		{
			$html = '<table class="table table-striped table-bordered table-responsive-md">';
				$html .= '<thead class="bg-success text-white">';
					$html .= '<tr>';
						$html .= '<td width="10">No</td>';
						$html .= '<td>Nama Obat</td>';
						$html .= '<td>Jumlah</td>';
						$html .= '<td>Aturan Pakai</td>';
						$html .= '<td width="50">Aksi</td>';
					$html .= '</tr>';
				$html .= '</thead>';
				$html .= '<tbody class="tabledianosa2">';
					$n = 1;
					foreach ($ResResepObat as $k => $v) {
						$atruan = (!empty($v->aturan_pakai)) ? explode(',', $v->aturan_pakai) : '';
						$html .= '<tr data-no_rawat="'.$v->no_rawat.'" data-kode_brng="'.$v->kode_brng.'" data-no_resep="'.$v->no_resep.'">';
							$html .= '<input type="hidden" name="kode_obat[]" value="'.$v->kode_brng.'">';
							$html .= '<input type="hidden" name="jlm_obat[]" value="'.$v->jml.'">';
							$html .= '<input type="hidden" name="aturan_pakai[]" value="'.@$atruan[0].'">';
							$html .= '<input type="hidden" name="keterangan[]" value="'.@$atruan[1].'">';
							$html .= '<td>'.$n.'</td>';
							$html .= '<td>'.$v->nama_brng.'</td>';
							$html .= '<td>'.$v->jml.'</td>';
							$html .= '<td>'.$v->aturan_pakai.'</td>';
							$html .= '<td style="cursor:pointer;" class="deleteResepObat"><button type="button" class="btn btn-danger btn-sm"> Hapus</button></td>';
						$html .= '</tr>';
						$n++;
					}
				$html .= '</tbody>';
			$html .= '</table>';
		}
		else
		{
			$html = '';
		}

		return $html;
	}

	public function history_obat_last()
	{
		$this->load->model(array('Resepobat_model','Masteraturanpakai_model'));
		$post = $this->input->post();
        $ResResepObat = $this->Resepobat_model->getDataObat(array(
			'resep_obat.no_rawat' => $post['no_rawat']
		));

        $aturan_pakai1 = $this->Masteraturanpakai_model->get('','aturan')->result_array();
        $aturan_pakai = $this->instrument->splitRecursiveArray($aturan_pakai1,'aturan','aturan');
        if($ResResepObat)
        {
        	echo '<div class="border bg-light col-12 mb-3 pt-2">';
			echo '<div class="form-group">';
				echo '<label><h5>Data Riwayat Resep Obat Masa Lalu</h5></label>';
			echo '</div>';
			echo '<hr>';
			echo '<input type="button" style="cursor:pointer;" class="delcr btn btn-danger btn-sm" value="Hapus">&nbsp;';
			echo '<input type="button" style="cursor:pointer;" class="copyr btn btn-primary btn-sm" value="Simpan">';
			echo '<table class="table table-striped table-responsive-md tblInput2 mt-3" id="list-dataobat-riwayat"><br>';
			echo '<tbody id="tb-resepobatcopy">';
			foreach ($ResResepObat as $k => $v) {
				$atruan = (!empty($v->aturan_pakai)) ? explode(',', $v->aturan_pakai) : $v->aturan_pakai;
				echo '<tr class="pilihobat">';
					echo '<td width="50"><input type="checkbox" class="form-control" style="width:20px;" name="records"></td>';
					echo '<td class="tr_mod"><input type="hidden" name="kode_obat[]" id="kode_obat" value="'.$v->kode_brng.'"><h6>'.$v->nama_brng.'</h6></td>';
					echo '<td class="tr_mod">Jumlah <br><input type="text" class="form-control col-2" name="jlm_obat[]" id="jlm_obat" value="'.$v->jml.'"></td>';
					echo '<td class="tr_mod">Keterangan <br><input type="text" id="keterangan" class="form-control" name="keterangan[]" id="keterangan" value="'.@$atruan[1].'"></td>';
					echo '<td class="tr_mod">Aturan Pakai <br>';
					echo $this->instrument->htmlSelectFromArray($aturan_pakai,'name="aturan_pakai[]" style="width:100px;" id="aturan_pakai" class="form-control"', false, @$atruan[0]);
					echo '</td>';
				echo '</tr>';
			}	
			echo '</tbody>';
			echo '</table>';
			echo '</div>';
        }
        else
        {
        	echo '<div class="border bg-light col-12 mb-3 pt-2">';
			echo '<div class="form-group">';
				echo '<label><h5>Data Riwayat Resep Obat Masa Lalu</h5></label>';
			echo '</div>';
			echo '<hr>';
			echo '<p><b>Data tidak tersedia.</b></p>';
			echo '</div>';
        }
	}
	/*
	End Module Obat
	*/


	/*
	Module Laboratorium
	*/
	public function savelabs()
	{
		$post = $this->input->post();
		$this->load->model(array('Permintaanpemeriksaanlab_model','Permintaanlab_model'));

		$res_noorder = $this->Permintaanlab_model->get('','noorder',array('no_rawat'=>$post['no_rawat']),'','','',1)->result();
		
		if(@$res_noorder[0]->noorder)
		{
			if(isset($post['kd_jenis_prw_labs']))
			{

				foreach ($post['kd_jenis_prw_labs'] as $key => $value) {
					$permintaan_pemeriksaan_lab[] = array(
						'noorder' => $res_noorder[0]->noorder,
						'kd_jenis_prw' => $value,
						'stts_bayar' => 'Belum'
					);
				}

				if(!$this->Permintaanpemeriksaanlab_model->insert('',$permintaan_pemeriksaan_lab,TRUE))
				{
					$analisis = $this->saveanalisis($post,'',4,'update');
					$this->Analisispengguna_model->insert('',$analisis);
					$error = '';
					$html = $this->showTableLab($post['no_rawat']);
				}
				else
				{
					$error = 1;
					$html = '<span class="text-danger">Data gagal disimpan.</span>';
				}

			}
			else
			{
				$error = 1;
				$html = '<span class="text-danger">Data tidak boleh kosong.</span>';
			}
		}
		else
		{
			if(isset($post['kd_jenis_prw_labs']))
			{
				$cek = $this->Permintaanlab_model->get('','count(*) as jml',array('no_rawat'=>$post['no_rawat']))->result();
				$get_number = $this->Permintaanlab_model->get('','ifnull(MAX(CONVERT(RIGHT(noorder,10),signed)),0) as noorder',array('tgl_permintaan'=>$post['tgl']))->result();
				$noorder = getNumberLab($get_number,$post['tgl'],'labs');

				$permintaan_lab = array(
					'noorder' => $noorder,
					'no_rawat' => $post['no_rawat'],
					'tgl_permintaan' => $post['tgl'],
					'jam_permintaan' => date('H:i:s'),
					'tgl_sampel' => '0000-00-00',
					'jam_sampel' => '00:00:00',
					'tgl_hasil' => '0000-00-00',
					'jam_hasil' => '00:00:00',
					'dokter_perujuk' => $post['kd_dokter'],
					'status' => 'ralan'
				);

				$this->Permintaanlab_model->insert('',$permintaan_lab,FALSE);

				foreach ($post['kd_jenis_prw_labs'] as $key => $value) {
					$permintaan_pemeriksaan_lab[] = array(
						'noorder' => $noorder,
						'kd_jenis_prw' => $value,
						'stts_bayar' => 'Belum'
					);
				}

				if(!$this->Permintaanpemeriksaanlab_model->insert('',$permintaan_pemeriksaan_lab,TRUE))
				{
					$analisis = $this->saveanalisis($post,'',4,'insert');
					$this->Analisispengguna_model->insert('',$analisis);
					$error = '';
					$html = $this->showTableLab($post['no_rawat']);
				}
				else
				{
					$error = 1;
					$html = '<span class="text-danger">Data gagal disimpan.</span>';
				}
			}
			else
			{
				$error = 1;
				$html = '<span class="text-danger">Data tidak boleh kosong.</span>';
			}
		}
		
		$datas = array(
			'view' => $html,
			'error' => $error
		);

		echo json_encode($datas);
	}

	public function deletelabs()
	{
		$this->load->model(array('Permintaanpemeriksaanlab_model','Permintaanlab_model'));
		$post = $this->input->post();
		$getLab = $this->Permintaanpemeriksaanlab_model->get('','count(*) as total',array('noorder'=>$post['noorder']))->result();
		if($getLab[0]->total > 1)
		{
			if($post['noorder'])
			{
				if($this->Permintaanpemeriksaanlab_model->delete('',array('noorder'=>$post['noorder'],'kd_jenis_prw'=>$post['kd_jenis_prw'])))
				{
					$html = $this->showTableLab($post['no_rawat']);
					$error = '';
				}
				else
				{
					$html = '<span class="text-danger">Maaf data tidak bisa dihapus.</span>';
					$error = 1;
				}

				$datas = array(
					'view' => $html,
					'error' => $error
				); 

				echo json_encode($datas);
			}
		}
		else
		{
			if($post['noorder'])
			{
				if($this->Permintaanpemeriksaanlab_model->delete('',array('noorder'=>$post['noorder'],'kd_jenis_prw'=>$post['kd_jenis_prw'])))
				{
					$this->Permintaanlab_model->delete('',array('noorder'=>$post['noorder'],'no_rawat'=>$post['no_rawat']));
					$html = $this->showTableLab($post['no_rawat']);
					$error = '';
				}
				else
				{
					$html = '<span class="text-danger">Maaf data tidak bisa dihapus.</span>';
					$error = 1;
				}

				$datas = array(
					'view' => $html,
					'error' => $error
				); 

				echo json_encode($datas);
			}
		}
	}

	private function showTableLab($no_rawat)
	{
		$this->load->model(array('Permintaanlab_model'));

        $reslabs = $this->Permintaanlab_model->getPermintaanLab(array(
			'permintaan_lab.no_rawat' => $no_rawat
		));
        
        if($reslabs)
        {
        	$html = '<table class="table table-bordered table-striped table-responsive-md">';
				$html .= '<thead class="bg-success text-white">';
					$html .= '<tr>';
						$html .= '<td width="10">No</td>';
						$html .= '<td>Nama Permintaan Lab</td>';
						$html .= '<td width="50">Aksi</td>';
					$html .= '</tr>';
				$html .= '</thead>';
				$html .= '<tbody class="permintaan_lab">';
					$n = 1;
					foreach ($reslabs as $k => $v) {
						$html .= '<tr data-no_rawat="'.$v->no_rawat.'" data-kd_jenis_prw="'.$v->kd_jenis_prw.'" data-noorder="'.$v->noorder.'">';
							$html .= '<input type="hidden" name="kd_jenis_prw[]" value="'.$v->kd_jenis_prw.'">';
							$html .= '<td>'.$n.'</td>';
							$html .= '<td>'.$v->nm_perawatan.'</td>';
							$html .= '<td style="cursor:pointer;" class="deletelabs"><button type="button" class="btn btn-danger btn-sm"> Hapus</button></td>';
						$html .= '</tr>';
						$n++;
					}
				$html .= '</tbody>';
			$html .= '</table>';
        }
        else
        {
        	$html = '';
        }
		

		return $html;
	}

	private function showTableLabRiwayat($no_rawat)
	{
		$this->load->model(array('Laboratorium_model'));
		$where = array(
			'detail_periksa_lab.no_rawat' => $no_rawat
		);
		$res = $this->Laboratorium_model->getHistoryDetailPeriksa($where);
		
		if($res)
		{
			$html = '<table class="table table-striped table-bordered table-responsive-md">';
				$html .= '<thead>';
					$html .= '<tr>';
						$html .= '<th width="10">No</th>';
						$html .= '<th>Nama Pemeriksaan</th>';
						$html .= '<th>Hasil Laboratorium</th>';
					$html .= '</tr>';
				$html .= '</thead>';
				$html .= '<tbody>';
				$no=0;
					foreach ($res as $k => $v) {
						$no++;
						$html .= '<tr>';
							$html .= '<td align="center">'.$no.'</td>';
							$html .= '<td>'.$v->nm_perawatan.'</td>';
							$html .= '<td>';
							$html .= ($v->nilai) ? $v->nilai : $v->nilai_rujukan;
							$html .= '</td>';
						$html .= '</tr>';
					}
				$html .= '</tbody>'; 
			$html .= '</table>';
		}
		else
		{
			$html = '<p class="text-bold text-center">Data tidak tersedia.</p>';
		}

		return $html;
	}

	public function getLaboratorium()
	{
		$this->load->model(array('Jnsperawatanlab_model'));
		$keys 	= $_GET['term'];
		if(isset($keys))
		{
			$where = 'kd_jenis_prw like \'%'.strtoupper($keys).'%\' OR nm_perawatan like \'%'.strtoupper($keys).'%\'';
			$res = $this->Jnsperawatanlab_model->get('','kd_jenis_prw,nm_perawatan,kelas',$where,'','','kd_jenis_prw ASC')->result();
			if(count($res) > 0)
			{
				$res_lab = array();
				$no=0;
				foreach($res as $key => $value)
				{
					$no++;
					$res_lab[] = array(
						'keys' => trim(str_replace(".", '', $value->kd_jenis_prw)),
						'id' => $value->kd_jenis_prw,
						'value' => $value->nm_perawatan . ' - (' . $value->kelas . ')'
					);
				}
			}
			else
			{
				$res_lab[] = array(
					'keys' => '',
					'id' => 'No',
					'value' => 'Data tidak ditemukan.'
				);
			}	
		}
		else
		{
			$res_lab[] = array(
				'keys' => '',
				'id' => 'No',
				'value' => 'Data tidak ditemukan.'
			);
		}

		echo json_encode($res_lab);			
	}

	public function getLaboratoriumVoice()
	{
		$this->load->model(array('Jnsperawatanlab_model'));
		$keys 	= str_replace('+',' ',$_GET['term']);
		$res_lab = array();
		
		if(isset($keys))
		{
			$where = 'kd_jenis_prw like \'%'.strtoupper($keys).'%\' OR nm_perawatan like \'%'.strtoupper($keys).'%\'';
			$res = $this->Jnsperawatanlab_model->get('','kd_jenis_prw,nm_perawatan,kelas',$where,'','','kd_jenis_prw ASC')->result();
			
			if(count($res) > 0)
			{
				
				$no=0;
				foreach($res as $key => $value)
				{
					$no++;
					$res_lab[] = array(
						'keys' => trim(str_replace(".", '', $value->kd_jenis_prw)),
						'id' => $value->kd_jenis_prw,
						'value' => $value->nm_perawatan . ' - (' . $value->kelas . ')'
					);
				}
			}	
		}

		print_r($res_lab);
		/*if($res_lab)
		{
			
		}else{
			echo '<p>Data tidak ditemukan.<p>';
		}	*/
			
	}

	public function history_laboratorium_last()
	{
		$this->load->model(array('Laboratorium_model'));
		$post = $this->input->post();
		$where = array(
			'detail_periksa_lab.no_rawat' => $post['no_rawat']
		);
		$res = $this->Laboratorium_model->getHistoryDetailPeriksa($where);

		echo '<div class="border bg-light col-11 pt-2">';
		echo '<div class="form-group">';
			echo '<label><h5>Data Riwayat Laboratorium Masa Lalu</h5></label>';
		echo '</div>';
		echo '<hr>';
		echo '<table class="table table-striped table-responsive-md" id="list-datalaboratorium-riwayat">';
		echo '<tbody>';
		foreach ($res as $k => $v) {
			echo '<tr id="'.str_replace('.','',$v->kd_jenis_prw).'" style="cursor:pointer;" class="pilihlaboratorium" data-id="'.$v->kd_jenis_prw.'" data-keys="'.str_replace('.','',$v->kd_jenis_prw).'" data-nama="'.$v->nm_perawatan.'">';
				echo '<td class="tr_mod"><h6>'.$v->nm_perawatan.'</h6></td>';
				echo '<td><h6><i class="fa fa-transfer"> Pindahkan</i></h6></td>';
			echo '</tr>';
		}	
		echo '</tbody>';
		echo '</table>';
		echo '</div>';
	}
	/*
	End module Laboratorium
	*/

	/*
	Module Radiologi
	*/
	public function saverads()
	{
		$post = $this->input->post();
		/*print_r($post);
		exit();*/
		$this->load->model(array('Permintaanpemeriksaanradiologi_model','Permintaanradiologi_model'));

		$res_noorder = $this->Permintaanradiologi_model->get('','noorder',array('no_rawat'=>$post['no_rawat']),'','','',1)->result();
		
		if(@$res_noorder[0]->noorder)
		{
			if(isset($post['kd_jenis_prw_rad']))
			{

				foreach ($post['kd_jenis_prw_rad'] as $key => $value) {
					$permintaan_pemeriksaan_radiologi[] = array(
						'noorder' => $res_noorder[0]->noorder,
						'kd_jenis_prw' => $value,
						'stts_bayar' => 'Belum'
					);
				}

				if(!$this->Permintaanpemeriksaanradiologi_model->insert('',$permintaan_pemeriksaan_radiologi,TRUE))
				{
					$analisis = $this->saveanalisis($post,'',5,'update');
					$this->Analisispengguna_model->insert('',$analisis);
					$error = '';
					$html = $this->showTableRad($post['no_rawat']);
				}
				else
				{
					$error = 1;
					$html = '<span class="text-danger">Data gagal disimpan.</span>';
				}

			}
			else
			{
				$error = 1;
				$html = '<span class="text-danger">Data tidak boleh kosong.</span>';
			}
		}
		else
		{
			if(isset($post['kd_jenis_prw_rad']))
			{
				$cek = $this->Permintaanradiologi_model->get('','count(*) as jml',array('no_rawat'=>$post['no_rawat']))->result();
				$get_number = $this->Permintaanradiologi_model->get('','ifnull(MAX(CONVERT(RIGHT(noorder,10),signed)),0) as noorder',array('tgl_permintaan'=>$post['tgl']))->result();
				$noorder = getNumberLab($get_number,$post['tgl'],'rad');

				$permintaan_radiologi = array(
					'noorder' => $noorder,
					'no_rawat' => $post['no_rawat'],
					'tgl_permintaan' => $post['tgl'],
					'jam_permintaan' => date('H:i:s'),
					'tgl_sampel' => '0000-00-00',
					'jam_sampel' => '00:00:00',
					'tgl_hasil' => '0000-00-00',
					'jam_hasil' => '00:00:00',
					'dokter_perujuk' => $post['kd_dokter'],
					'status' => 'ralan'
				);

				$this->Permintaanradiologi_model->insert('',$permintaan_radiologi,FALSE);

				foreach ($post['kd_jenis_prw_rad'] as $key => $value) {
					$permintaan_pemeriksaan_radiologi[] = array(
						'noorder' => $noorder,
						'kd_jenis_prw' => $value,
						'stts_bayar' => 'Belum'
					);
				}

				if(!$this->Permintaanpemeriksaanradiologi_model->insert('',$permintaan_pemeriksaan_radiologi,TRUE))
				{
					$analisis = $this->saveanalisis($post,'',5,'insert');
					$this->Analisispengguna_model->insert('',$analisis);
					$error = '';
					$html = $this->showTableRad($post['no_rawat']);
				}
				else
				{
					$error = 1;
					$html = '<span class="text-danger">Data gagal disimpan.</span>';
				}
			}
			else
			{
				$error = 1;
				$html = '<span class="text-danger">Data tidak boleh kosong.</span>';
			}
		}
		
		$datas = array(
			'view' => $html,
			'error' => $error
		);

		echo json_encode($datas);
	}

	public function deleterads()
	{
		$this->load->model(array('Permintaanpemeriksaanradiologi_model','Permintaanradiologi_model'));
		$post = $this->input->post();
		$getrad = $this->Permintaanpemeriksaanradiologi_model->get('','count(*) as total',array('noorder'=>$post['noorder']))->result();
		
		if($getrad[0]->total > 1)
		{
			if($post['noorder'])
			{
				if($this->Permintaanpemeriksaanradiologi_model->delete('',array('noorder'=>$post['noorder'],'kd_jenis_prw'=>$post['kd_jenis_prw'])))
				{
					$html = $this->showTableRad($post['no_rawat']);
					$error = '';
				}
				else
				{
					$html = '<span class="text-danger">Maaf data tidak bisa dihapus.</span>';
					$error = 1;
				}

				$datas = array(
					'view' => $html,
					'error' => $error
				); 

				echo json_encode($datas);
			}
		}
		else
		{
			if($post['noorder'])
			{
				if($this->Permintaanpemeriksaanradiologi_model->delete('',array('noorder'=>$post['noorder'],'kd_jenis_prw'=>$post['kd_jenis_prw'])))
				{
					$this->Permintaanradiologi_model->delete('',array('noorder'=>$post['noorder'],'no_rawat'=>$post['no_rawat']));
					$html = $this->showTableRad($post['no_rawat']);
					$error = '';
				}
				else
				{
					$html = '<span class="text-danger">Maaf data tidak bisa dihapus.</span>';
					$error = 1;
				}

				$datas = array(
					'view' => $html,
					'error' => $error
				); 

				echo json_encode($datas);
			}
		}
	}

	private function showTableRad($no_rawat)
	{
		$this->load->model(array('Permintaanradiologi_model'));
		// Resep Obat
        $resrads = $this->Permintaanradiologi_model->getPermintaanRad(array(
			'permintaan_radiologi.no_rawat' => $no_rawat
		));

		if($resrads)
		{
			$html = '<table class="table table-striped table-bordered table-responsive-md">';
				$html .= '<thead class="bg-success text-white">';
					$html .= '<tr>';
						$html .= '<th width="10">No</th>';
						$html .= '<th>Nama Permintaan Radiologi</th>';
						$html .= '<th width="50">Aksi</th>';
					$html .= '</tr>';
				$html .= '</thead>';
				$html .= '<tbody class="permintaan_lab">';
					$n = 1;
					foreach ($resrads as $k => $v) {
						$html .= '<tr data-no_rawat="'.$v->no_rawat.'" data-kd_jenis_prw="'.$v->kd_jenis_prw.'" data-noorder="'.$v->noorder.'">';
							$html .= '<input type="hidden" name="kd_jenis_prw[]" value="'.$v->kd_jenis_prw.'">';
							$html .= '<td>'.$n.'</td>';
							$html .= '<td>'.$v->nm_perawatan.'</td>';
							$html .= '<td style="cursor:pointer;" class="deleterads"><button type="button" class="btn btn-danger btn-sm"> Hapus</button></td>';
						$html .= '</tr>';
						$n++;
					}
				$html .= '</tbody>';
			$html .= '</table>';
		}
		else
		{
			$html = '';
		}

		return $html;
	}

	private function showTableRadRiwayat($no_rawat)
	{
		$this->load->model(array('Hasilradiologi_model'));
		$resrads 	= $this->Hasilradiologi_model->get('','hasil',array(
			'no_rawat' => $no_rawat
		))->result();
		if($resrads)
		{
			$html = '<table class="table table-striped table-bordered table-responsive-md">';
				$html .= '<thead>';
					$html .= '<tr>';
						$html .= '<th width="10">No</th>';
						$html .= '<th>Hasil Radiologi</th>';
					$html .= '</tr>';
				$html .= '</thead>';
				$html .= '<tbody>';
				$no=0;
					foreach (@$resrads as $k => $v) {
						$no++;
						$html .= '<tr>';
							$html .= '<td align="center">'.$no.'</td>';
							$html .= '<td>'.$v->hasil.'</td>';
						$html .= '</tr>';
					}
				$html .= '</tbody>'; 
			$html .= '</table>';
		}
		else
		{
			$html = '<p class="text-bold text-center">Data tidak tersedia.</p>';
		}

		return $html;
	}

	public function getRadiologi()
	{
		$this->load->model(array('Jnsperawatanradiologi_model'));
		$keys 	= $_GET['term'];
		if(isset($keys))
		{
			$where = 'kd_jenis_prw like \'%'.strtoupper($keys).'%\' OR nm_perawatan like \'%'.strtoupper($keys).'%\'';
			$res = $this->Jnsperawatanradiologi_model->get('','kd_jenis_prw,nm_perawatan,kelas',$where,'','','kd_jenis_prw ASC')->result();
			if(count($res) > 0)
			{
				$res_rad = array();
				$no=0;
				foreach($res as $key => $value)
				{
					$no++;
					$res_rad[] = array(
						'keys' => str_replace(' ','',trim(str_replace(".", "", $value->kd_jenis_prw))),
						'id' => $value->kd_jenis_prw,
						'value' => $value->nm_perawatan . ' - (' . $value->kelas . ')'
					);
				}
			}
			else
			{
				$res_rad[] = array(
					'keys' => '',
					'id' => 'No',
					'value' => 'Data tidak ditemukan.'
				);
			}
			echo json_encode($res_rad);				
		}
	}

	public function history_radiologi_last()
	{
		$post = $this->input->post();
		$this->load->model(array('Permintaanradiologi_model'));
		$res = $this->Permintaanradiologi_model->getPermintaanRad(array(
			'permintaan_radiologi.no_rawat' => $post['no_rawat']
		));
		
		echo '<div class="border bg-light col-11 pt-2">';
		echo '<div class="form-group">';
			echo '<label><h5>Data Riwayat Radiologi Masa Lalu</h5></label>';
		echo '</div>';
		echo '<hr>';
		echo '<table class="table table-striped table-responsive-md" id="list-dataradiologi-riwayat">';
		echo '<tbody>';
		foreach ($res as $k => $v) {
			echo '<tr id="'.str_replace(' ','',trim(str_replace('.','',$v->kd_jenis_prw))).'" style="cursor:pointer;" class="pilihradiologi" data-id="'.$v->kd_jenis_prw.'" data-keys="'.str_replace(' ','',trim(str_replace('.','',$v->kd_jenis_prw))).'" data-nama="'.$v->nm_perawatan.'">';
				echo '<td class="tr_mod"><h6>'.$v->nm_perawatan.'</h6></td>';
				echo '<td><h6><i class="fa fa-transfer"> Pindahkan</i></h6></td>';
			echo '</tr>';
		}	
		echo '</tbody>';
		echo '</table>';
		echo '</div>';
	}
	/*
	End module radiologi
	*/


	/*
	Module Tindakan
	Tindakan buat untuk cek apakah tersedia data
	*/
	public function savetind()
	{
		$this->load->model(array('Rawatjldrpr_model'));
		$post = $this->input->post();
		foreach ($post['kd_jenis_prw_tindakan'] as $key => $value) {
			$permintaan_pemeriksaan_tindakan[] = array(
				'no_rawat' => $post['no_rawat'],
				'kd_jenis_prw' => $value,
				'kd_dokter' => $post['kd_dokter'],
				'nip' => 'RAJAL',
				'tgl_perawatan' => $post['tgl'],
				'jam_rawat' => date('H:i:s'),
				'material' => $post['material'][$key],
				'bhp' => $post['bhp'][$key],
				'tarif_tindakandr' => $post['tarif_tindakandr'][$key],
				'tarif_tindakanpr' => $post['tarif_tindakanpr'][$key],
				'kso' => $post['kso'][$key],
				'menejemen' => $post['menejemen'][$key],
				'biaya_rawat' => $post['biaya_rawat'][$key]
			);
		}

		$total = $this->Rawatjldrpr_model->get('','count(*) as total',array('no_rawat'=>$post['no_rawat']))->result();

		if(is_array($permintaan_pemeriksaan_tindakan))
		{
			if($total[0]->total > 0)
			{
				if($this->Rawatjldrpr_model->delete('',array('no_rawat'=>$post['no_rawat'])))
				{

					$this->Rawatjldrpr_model->insert('',$permintaan_pemeriksaan_tindakan,TRUE);
					$analisis = $this->saveanalisis($post,'',6,'update');
					$this->Analisispengguna_model->insert('',$analisis);
					$error = '';
					$html = $this->showTableTindakan($post['no_rawat']);
				}
				else
				{
					$error = 1;
					$html = '<span class="text-danger">Data gagal disimpan.</span>';
				}
			}
			else
			{
				if(!$this->Rawatjldrpr_model->insert('',$permintaan_pemeriksaan_tindakan,TRUE))
				{
					$analisis = $this->saveanalisis($post,'',6,'insert');
					$this->Analisispengguna_model->insert('',$analisis);
					$error = '';
					$html = $this->showTableTindakan($post['no_rawat']);
				}
				else
				{
					$error = 1;
					$html = '<span class="text-danger">Data gagal disimpan.</span>';
				}
			}
			
		}
		else
		{
			$error = 1;
			$html = '<span class="text-danger">Data tidak dalam bentuk array.</span>';
		}

		$datas = array(
			'view' => $html,
			'error' => $error
		);

		echo json_encode($datas);
	}

	private function showTableTindakan($no_rawat)
	{
		$this->load->model(array('Rawatjldrpr_model'));
		// Resep Obat
        $resrads = $this->Rawatjldrpr_model->getPermintaanTindakan(array(
			'rawat_jl_drpr.no_rawat' => $no_rawat
		));
        	
		if($resrads)
		{
			$html = '<table class="table table-striped table-bordered table-responsive-md">';
				$html .= '<thead class="bg-success text-white">';
					$html .= '<tr>';
						$html .= '<th width="10">No</th>';
						$html .= '<th>Nama Permintaan</th>';
						$html .= '<th width="50">Aksi</th>';
					$html .= '</tr>';
				$html .= '</thead>';
				$html .= '<tbody class="permintaan_tindakana">';
					$n = 1;
					foreach ($resrads as $k => $v) {
						$html .= '<tr data-no_rawat="'.$v->no_rawat.'" data-kd_jenis_prw="'.$v->kd_jenis_prw.'">';
							$html .= '<input type="hidden" name="kd_jenis_prw_tindakan[]" value="'.$v->kd_jenis_prw.'">';
							$html .= '<input type="hidden" name="material[]" value="'.$v->kd_jenis_prw.'">';
							$html .= '<input type="hidden" name="bhp[]" value="'.$v->kd_jenis_prw.'">';
							$html .= '<input type="hidden" name="tarif_tindakanpr[]" value="'.$v->kd_jenis_prw.'">';
							$html .= '<input type="hidden" name="tarif_tindakandr[]" value="'.$v->kd_jenis_prw.'">';
							$html .= '<input type="hidden" name="kso[]" value="'.$v->kd_jenis_prw.'">';
							$html .= '<input type="hidden" name="menejemen[]" value="'.$v->kd_jenis_prw.'">';
							$html .= '<input type="hidden" name="biaya_rawat[]" value="'.$v->kd_jenis_prw.'">';
							$html .= '<td>'.$n.'</td>';
							$html .= '<td>'.$v->nm_perawatan.'</td>';
							$html .= '<td style="cursor:pointer;" class="deletetind"><button type="button" class="btn btn-danger btn-sm"> Hapus</button></td>';
						$html .= '</tr>';
						$n++;
					}
				$html .= '</tbody>';
			$html .= '</table>';
		}
		else
		{
			$html = '<p class="text-bold text-center">Data tidak tersedia.<p>';
		}

		return $html;
	}

	private function showTableRiwayatTindakan($no_rawat)
	{
		$this->load->model(array('Rawatjldrpr_model'));
		// Resep Obat
        $resrads = $this->Rawatjldrpr_model->getPermintaanTindakan(array(
			'rawat_jl_drpr.no_rawat' => $no_rawat
		));
        	
		if($resrads)
		{
			$html = '<table class="table table-striped table-bordered table-responsive-md">';
				$html .= '<thead class="bg-success text-white">';
					$html .= '<tr>';
						$html .= '<th width="10">No</th>';
						$html .= '<th>Nama Permintaan</th>';
						$html .= '<th width="50">Aksi</th>';
					$html .= '</tr>';
				$html .= '</thead>';
				$html .= '<tbody class="permintaan_tindakana">';
					$n = 1;
					foreach ($resrads as $k => $v) {
						$html .= '<tr data-no_rawat="'.$v->no_rawat.'" data-kd_jenis_prw="'.$v->kd_jenis_prw.'">';
							$html .= '<input type="hidden" name="kd_jenis_prw_tindakan[]" value="'.$v->kd_jenis_prw.'">';
							$html .= '<input type="hidden" name="material[]" value="'.$v->kd_jenis_prw.'">';
							$html .= '<input type="hidden" name="bhp[]" value="'.$v->kd_jenis_prw.'">';
							$html .= '<input type="hidden" name="tarif_tindakanpr[]" value="'.$v->kd_jenis_prw.'">';
							$html .= '<input type="hidden" name="tarif_tindakandr[]" value="'.$v->kd_jenis_prw.'">';
							$html .= '<input type="hidden" name="kso[]" value="'.$v->kd_jenis_prw.'">';
							$html .= '<input type="hidden" name="menejemen[]" value="'.$v->kd_jenis_prw.'">';
							$html .= '<input type="hidden" name="biaya_rawat[]" value="'.$v->kd_jenis_prw.'">';
							$html .= '<td>'.$n.'</td>';
							$html .= '<td>'.$v->nm_perawatan.'</td>';
							$html .= '<td style="cursor:pointer;" data-no_rawat="'.$v->no_rawat.'" data-kd_jenis_prw="'.$v->kd_jenis_prw.'" class="deletetind"><button type="button" class="btn btn-danger btn-sm"> Hapus</button></td>';
						$html .= '</tr>';
						$n++;
					}
				$html .= '</tbody>';
			$html .= '</table>';
		}
		else
		{
			$html = '<p class="text-bold text-center">Data tidak tersedia.<p>';
		}

		return $html;
	}

	public function deletetindakan()
	{
		$this->load->model(array('Rawatjldrpr_model'));
		$post = $this->input->post();
		
		if($post)
		{
			if($this->Rawatjldrpr_model->delete('',array('no_rawat'=>$post['no_rawat'],'kd_jenis_prw'=>$post['kd_jenis_prw'])))
			{
				$html = $this->showTableTindakan($post['no_rawat']);
				$error = '';
			}
			else
			{
				$html = '<span class="text-danger">Maaf data tidak bisa dihapus.</span>';
				$error = 1;
			}
		}
		else
		{
			$html = $this->showTableTindakan($post['no_rawat']);
			$error = '';
		}

		$datas = array(
			'view' => $html,
			'error' => $error
		); 

		echo json_encode($datas);
	}

	public function getTindakan()
	{
		$this->load->model(array('Jnsperawatan_model'));
		$keys	= $_GET['nama_perawatan'];
		$kd_pj 	= $_GET['kd_pj'];
		$kd_poli= $_GET['kd_poli'];

		if(isset($keys))
		{
			$where = 'kd_jenis_prw like \'%'.strtoupper($keys).'%\' OR nm_perawatan like \'%'.strtoupper($keys).'%\' and kd_pj=\''.$kd_pj.'\' and kd_poli=\''.$kd_poli.'\'';
			$res = $this->Jnsperawatan_model->get('','kd_jenis_prw,nm_perawatan,material,bhp,tarif_tindakandr,tarif_tindakanpr,kso,menejemen,total_byrdrpr',$where,'','','kd_jenis_prw ASC')->result();
			if(count($res) > 0)
			{
				$res_rad = array();
				$no=0;
				foreach($res as $key => $value)
				{
					$no++;
					$res_rad[] = array(
						'id' => $value->kd_jenis_prw,
						'value' => $value->nm_perawatan,
						'material' => $value->material,
						'bhp' => $value->bhp,
						'tarif_tindakandr' => $value->tarif_tindakandr,
						'tarif_tindakanpr' => $value->tarif_tindakanpr,
						'kso' => $value->kso,
						'menejemen' => $value->menejemen,
						'biaya_rawat' => $value->total_byrdrpr,
					);
				}
			}
			else
			{
				$res_rad[] = array(
					'id' => 'No',
					'value' => 'Data tidak ditemukan.'
				);
			}
			echo json_encode($res_rad);				
		}
	}

	public function history_tindakan_last()
	{
		$post = $this->input->post();
		$this->load->model(array('Rawatjldrpr_model'));
		// Resep Obat
        $resrads = $this->Rawatjldrpr_model->getPermintaanTindakan(array(
			'rawat_jl_drpr.no_rawat' => $post['no_rawat']
		));

        echo '<div class="border bg-light col-11 pt-2">';
		echo '<div class="form-group">';
			echo '<label><h5>Data Riwayat Tindakan Masa Lalu</h5></label>';
		echo '</div>';
		echo '<hr>';
		echo '<table class="table table-striped table-responsive-md" id="list-datatindakan-riwayat">';
		echo '<tbody>';
		foreach ($resrads as $k => $v) {
			echo '<tr id="'.$v->kd_jenis_prw.'" style="cursor:pointer;" class="pilihtindakan" data-kd_jenis_prw="'.$v->kd_jenis_prw.'" data-material="'.$v->material.'" data-bhp="'.$v->bhp.'" data-kso="'.$v->kso.'" data-menejemen="'.$v->menejemen.'" data-tarif_tindakanpr="'.$v->tarif_tindakanpr.'" data-tarif_tindakandr="'.$v->tarif_tindakandr.'" data-biaya_rawat="'.$v->biaya_rawat.'" data-nm_perawatan="'.$v->nm_perawatan.'" data-nm_kategori="'.$v->nm_kategori.'">';
				echo '<td class="tr_mod"><h6>'.$v->nm_perawatan.'</h6></td>';
				echo '<td><h6><i class="fa fa-transfer"> Pindahkan</i></h6></td>';
			echo '</tr>';
		}	
		echo '</tbody>';
		echo '</table>';
		echo '</div>';

	}
	/*
	End module tindakan
	*/

	/*
	Starts module pemeriksaan
	*/
	public function savepemeriksaan()
	{
		$this->load->model(array('Catatanperawatan_model'));
		$post = $this->input->post();
		$get = $this->Catatanperawatan_model->get('','count(*) as total',array('no_rawat'=>$post['no_rawat']))->result();
		$catatan  = 'S:'.$post['s'].'\n';
		$catatan .= 'O:'.$post['o'].'\n';
		$catatan .= 'A:'.$post['a'].'\n';
		$catatan .= 'P:'.$post['p'];

		if($get[0]->total > 0)
		{
			$data = array(
				'catatan' => $catatan
			);

			if($this->Catatanperawatan_model->update('',$data,array('no_rawat'=>$post['no_rawat'])))
			{
				$error = '';
				$msg = 'Data berhasil diupdate';
				$s = $post['s'];
				$o = $post['o'];
				$a = $post['a'];
				$p = $post['p'];

				$analisis = $this->saveanalisis($post,'',1,'update');
				$this->Analisispengguna_model->insert('',$analisis);
			}
			else
			{
				$error = 1;
				$msg = 'Data gagal diupdate';
				$s = '';
				$o = '';
				$a = '';
				$p = '';
			}
		}
		else
		{
			$data = array(
				'no_rawat' => $post['no_rawat'],
				'tanggal' => date('Y-m-d'),
				'jam' => date('H:i:s'),
				'kd_dokter' => $post['kd_dokter'],
				'catatan' => $catatan
			);

			if($this->Catatanperawatan_model->insert('',$data))
			{
				$error = 1;
				$msg = 'Data gagal disimpan';
				$s = '';
				$o = '';
				$a = '';
				$p = '';
				
			}
			else
			{
				$error = '';
				$msg = 'Data berhasil disimpan';
				$s = $post['s'];
				$o = $post['o'];
				$a = $post['a'];
				$p = $post['p'];

				$analisis = $this->saveanalisis($post,'',1,'insert');
				$this->Analisispengguna_model->insert('',$analisis);
			}
		}

		$datas = array(
			'error' => $error,
			'msg' => $msg,
			's' => $s,
			'o' => $o,
			'a' => $a,
			'p' => $p
		);

		echo json_encode($datas);
	}

	public function editpemeriksaan()
	{
		$post = $this->input->post();
		$this->load->model(array('Pemeriksaanralan_model'));
		$res = $this->Pemeriksaanralan_model->get('','*',array('no_rawat'=>$post['NoRawat']))->result();
		
		if(@$post['edit'])
		{
			$html = $this->tablePemeriksaanEdit($post['NoRawat']);
		}
		else
		{
			$html = $this->tablePemeriksaanCancel($post['NoRawat']);
		}
		

		echo $html;
	}

	public function updatepemeriksaan()
	{
		$this->load->model(array('Pemeriksaanralan_model'));
		$data = array();
		
		$data = $this->instrument->_post('',true);
		$no_rawat 	= $data['no_rawat'];
		$getPemeriksaan = $this->Pemeriksaanralan_model->get('','count(*) as total',array('no_rawat'=>$no_rawat))->result();
		unset($data['no_rawat']);
		if(@$getPemeriksaan[0]->total)
		{
			if($this->Pemeriksaanralan_model->update('',$data,array('no_rawat'=>$no_rawat)))
			{
				$error = '';
				$html = $this->tablePemeriksaanCancel($no_rawat);
			}
			else
			{
				$error = 1;
				$html = '<p class="text-danger">Data gagal diupdate.</p>';
			}
		}
		else
		{
			if($this->Pemeriksaanralan_model->insert('',$this->instrument->_post('',true)))
			{
				$error = 1;
				$html = '<p class="text-danger">Data gagal disimpan.</p>';
				
			}
			else
			{
				$error = '';
				$html = $this->tablePemeriksaanCancel($no_rawat);
			}
		}
		
		$data = array(
			'error' => $error,
			'html' => $html
		);
		echo json_encode($data);
	}	

	private function tablePemeriksaanEdit($no_rawat)
	{
		$res = $this->Pemeriksaanralan_model->get('','*',array('no_rawat'=>$no_rawat))->result();
		$suhu_tubuh = isset($res[0]->suhu_tubuh) ? $res[0]->suhu_tubuh : '';
		$tensi = isset($res[0]->tensi) ? $res[0]->tensi : '';
		$nadi = isset($res[0]->nadi) ? $res[0]->nadi : '';
		$respirasi = isset($res[0]->respirasi) ? $res[0]->respirasi : '';
		$tinggi = isset($res[0]->tinggi) ? $res[0]->tinggi : '';
		$berat = isset($res[0]->berat) ? $res[0]->berat : '';
		$gcs = isset($res[0]->gcs) ? $res[0]->gcs : '';
		$imun_ke = isset($res[0]->imun_ke) ? $res[0]->imun_ke : '';
		$rtl = isset($res[0]->rtl) ? $res[0]->rtl : '';
		$alergi = isset($res[0]->alergi) ? $res[0]->alergi : '';
		$keluhan = isset($res[0]->keluhan) ? $res[0]->keluhan : '';
		$pemeriksaan = isset($res[0]->pemeriksaan) ? $res[0]->pemeriksaan : '';

		$html = '<table class="" style="padding: 0px; margin: 0px; width:100%;">';
			$html .= '<tr style="padding: 0px; margin: 0px;">';
				$html .= '<td width="100">&nbsp; - Suhu Tubuh</td>';
				$html .= '<td width="10">:</td>';
				$html .= '<td><input type="text" name="suhu_tubuh" class="form-control input-sm" value='.$suhu_tubuh.'></td>';
				$html .= '<td width="20">&nbsp; -</td>';
				$html .= '<td width="100">Tensi</td>';
				$html .= '<td width="10">:</td>';
				$html .= '<td><input type="text" name="tensi" class="form-control input-sm" value='.$tensi.'></td>';
			$html .= '</tr>';
			$html .= '<tr>';
				$html .= '<td>&nbsp; - Nadi</td>';
				$html .= '<td>:</td>';
				$html .= '<td><input type="text" name="nadi" class="form-control input-sm" value='.$nadi.'></td>';
				$html .= '<td>&nbsp; -</td>';
				$html .= '<td>Respirasi</td>';
				$html .= '<td>:</td>';
				$html .= '<td><input type="text" name="respirasi" class="form-control input-sm" value='.$respirasi.'></td>';
			$html .= '</tr>';
			$html .= '<tr>';
				$html .= '<td>&nbsp; - Tinggi</td>';
				$html .= '<td>:</td>';
				$html .= '<td><input type="text" name="tinggi" class="form-control input-sm" value='.$tinggi.'></td>';
				$html .= '<td>&nbsp; -</td>';
				$html .= '<td>Berat</td>';
				$html .= '<td>:</td>';
				$html .= '<td><input type="text" name="berat" class="form-control input-sm" value='.$berat.'></td>';
			$html .= '</tr>';
			$html .= '<tr>';
				$html .= '<td>&nbsp; - gcs</td>';
				$html .= '<td>:</td>';
				$html .= '<td><input type="text" name="gcs" class="form-control input-sm" value='.$gcs.'></td>';
				$html .= '<td>&nbsp; -</td>';
				$html .= '<td>Imun Ke</td>';
				$html .= '<td>:</td>';
				$html .= '<td><input type="text" name="imun_ke" class="form-control input-sm" value='.$imun_ke.'></td>';
			$html .= '</tr>';
			$html .= '<tr>';
				$html .= '<td>&nbsp; - Rtl</td>';
				$html .= '<td>:</td>';
				$html .= '<td><input type="text" name="rtl" class="form-control input-sm" value='.$rtl.'></td>';
				$html .= '<td>&nbsp; -</td>';
				$html .= '<td>Alergi</td>';
				$html .= '<td>:</td>';
				$html .= '<td><input type="text" name="alergi" class="form-control input-sm" value='.$alergi.'></td>';
			$html .= '</tr>';
			$html .= '<tr>';
				$html .= '<td>&nbsp; - Keluhan</td>';
				$html .= '<td>:</td>';
				$html .= '<td><textarea class="form-control" name="keluhan">'.$keluhan.'</textarea></td>';
				$html .= '<td>&nbsp; -</td>';
				$html .= '<td>Pemeriksaan</td>';
				$html .= '<td>:</td>';
				$html .= '<td><textarea class="form-control" name="pemeriksaan">'.$pemeriksaan.'</textarea></td>';
			$html .= '</tr>';
			$html .= '<tr>';
				$html .= '<td class="p-2" colspan="7">';
					$html .= '<button class="btn btn-primary btn-sm update-pemeriksaan">Update</button> &nbsp;';
					$html .= '<button class="btn btn-warning btn-sm cancel-pemeriksaan">Cancel</button>';
				$html .= '</td>';
			$html .= '</tr>';
		$html .= '</table>';
		return $html;
	}

	private function tablePemeriksaanCancel($no_rawat)
	{
		$this->load->model(array('Pemeriksaanralan_model'));
		$res = $this->Pemeriksaanralan_model->get('','*',array('no_rawat'=>$no_rawat))->result();
		$suhu_tubuh = isset($res[0]->suhu_tubuh) ? $res[0]->suhu_tubuh : '';
		$tensi = isset($res[0]->tensi) ? $res[0]->tensi : '';
		$nadi = isset($res[0]->nadi) ? $res[0]->nadi : '';
		$respirasi = isset($res[0]->respirasi) ? $res[0]->respirasi : '';
		$tinggi = isset($res[0]->tinggi) ? $res[0]->tinggi : '';
		$berat = isset($res[0]->berat) ? $res[0]->berat : '';
		$gcs = isset($res[0]->gcs) ? $res[0]->gcs : '';
		$imun_ke = isset($res[0]->imun_ke) ? $res[0]->imun_ke : '';
		$rtl = isset($res[0]->rtl) ? $res[0]->rtl : '';
		$alergi = isset($res[0]->alergi) ? $res[0]->alergi : '';
		$keluhan = isset($res[0]->keluhan) ? $res[0]->keluhan : '';
		$pemeriksaan = isset($res[0]->pemeriksaan) ? $res[0]->pemeriksaan : '';
		$html = '<table class="" style="padding: 0px; margin: 0px; width:100%;">';
				$html .= '<tr style="padding: 0px; margin: 0px;">';
					$html .= '<td width="100">&nbsp; - Suhu Tubuh</td>';
					$html .= '<td width="10">:</td>';
					$html .= '<td><b>'.$suhu_tubuh.'</b></td>';
					$html .= '<td width="20">&nbsp; -</td>';
					$html .= '<td width="100">Tensi</td>';
					$html .= '<td width="10">:</td>';
					$html .= '<td><b>'.$tensi.'</b></td>';
				$html .= '</tr>';
				$html .= '<tr>';
					$html .= '<td>&nbsp; - Nadi</td>';
					$html .= '<td>:</td>';
					$html .= '<td><b>'.$nadi.'</b></td>';
					$html .= '<td>&nbsp; -</td>';
					$html .= '<td>Respirasi</td>';
					$html .= '<td>:</td>';
					$html .= '<td><b>'.$respirasi.'</b></td>';
				$html .= '</tr>';
				$html .= '<tr>';
					$html .= '<td>&nbsp; - Tinggi</td>';
					$html .= '<td>:</td>';
					$html .= '<td><b>'.$tinggi.'</b></td>';
					$html .= '<td>&nbsp; -</td>';
					$html .= '<td>Berat</td>';
					$html .= '<td>:</td>';
					$html .= '<td><b>'.$berat.'</b></td>';
				$html .= '</tr>';
				$html .= '<tr>';
					$html .= '<td>&nbsp; - gcs</td>';
					$html .= '<td>:</td>';
					$html .= '<td><b>'.$gcs.'</b></td>';
					$html .= '<td>&nbsp; -</td>';
					$html .= '<td>Imun Ke</td>';
					$html .= '<td>:</td>';
					$html .= '<td><b>'.$imun_ke.'</b></td>';
				$html .= '</tr>';
				$html .= '<tr>';
					$html .= '<td>&nbsp; - Rtl</td>';
					$html .= '<td>:</td>';
					$html .= '<td><b>'.$rtl.'</b></td>';
					$html .= '<td>&nbsp; -</td>';
					$html .= '<td>Alergi</td>';
					$html .= '<td>:</td>';
					$html .= '<td><b>'.$alergi.'</b></td>';
				$html .= '</tr>';
				$html .= '<tr>';
					$html .= '<td>&nbsp; - Keluhan</td>';
					$html .= '<td>:</td>';
					$html .= '<td><b>'.$keluhan.'</td>';
					$html .= '<td>&nbsp; -</td>';
					$html .= '<td>Pemeriksaan</td>';
					$html .= '<td>:</td>';
					$html .= '<td><b>'.$pemeriksaan.'</b></td>';
				$html .= '</tr>';
				$html .= '<tr>';
					$html .= '<td class="p-2" colspan="7">';
						$html .= '<button class="btn btn-primary btn-sm edit-pemeriksaan">Edit</button>';
					$html .= '</td>';
				$html .= '</tr>';
			$html .= '</table>';
			return $html;
	}

	private function tablePemeriksaan($no_rawat)
	{
		$this->load->model(array('Pemeriksaanralan_model'));
		$res = $this->Pemeriksaanralan_model->get('','*',array('no_rawat'=>$no_rawat))->result();
		$suhu_tubuh = isset($res[0]->suhu_tubuh) ? $res[0]->suhu_tubuh : '';
		$tensi = isset($res[0]->tensi) ? $res[0]->tensi : '';
		$nadi = isset($res[0]->nadi) ? $res[0]->nadi : '';
		$respirasi = isset($res[0]->respirasi) ? $res[0]->respirasi : '';
		$tinggi = isset($res[0]->tinggi) ? $res[0]->tinggi : '';
		$berat = isset($res[0]->berat) ? $res[0]->berat : '';
		$gcs = isset($res[0]->gcs) ? $res[0]->gcs : '';
		$imun_ke = isset($res[0]->imun_ke) ? $res[0]->imun_ke : '';
		$rtl = isset($res[0]->rtl) ? $res[0]->rtl : '';
		$alergi = isset($res[0]->alergi) ? $res[0]->alergi : '';
		$keluhan = isset($res[0]->keluhan) ? $res[0]->keluhan : '';
		$pemeriksaan = isset($res[0]->pemeriksaan) ? $res[0]->pemeriksaan : '';
		$html = '<table class="" style="padding: 0px; margin: 0px; width:100%;">';
				$html .= '<tr style="padding: 0px; margin: 0px;">';
					$html .= '<td width="100">&nbsp; - Suhu Tubuh</td>';
					$html .= '<td width="10">:</td>';
					$html .= '<td><b>'.$suhu_tubuh.'</b></td>';
					$html .= '<td width="20">&nbsp; -</td>';
					$html .= '<td width="100">Tensi</td>';
					$html .= '<td width="10">:</td>';
					$html .= '<td><b>'.$tensi.'</b></td>';
				$html .= '</tr>';
				$html .= '<tr>';
					$html .= '<td>&nbsp; - Nadi</td>';
					$html .= '<td>:</td>';
					$html .= '<td><b>'.$nadi.'</b></td>';
					$html .= '<td>&nbsp; -</td>';
					$html .= '<td>Respirasi</td>';
					$html .= '<td>:</td>';
					$html .= '<td><b>'.$respirasi.'</b></td>';
				$html .= '</tr>';
				$html .= '<tr>';
					$html .= '<td>&nbsp; - Tinggi</td>';
					$html .= '<td>:</td>';
					$html .= '<td><b>'.$tinggi.'</b></td>';
					$html .= '<td>&nbsp; -</td>';
					$html .= '<td>Berat</td>';
					$html .= '<td>:</td>';
					$html .= '<td><b>'.$berat.'</b></td>';
				$html .= '</tr>';
				$html .= '<tr>';
					$html .= '<td>&nbsp; - gcs</td>';
					$html .= '<td>:</td>';
					$html .= '<td><b>'.$gcs.'</b></td>';
					$html .= '<td>&nbsp; -</td>';
					$html .= '<td>Imun Ke</td>';
					$html .= '<td>:</td>';
					$html .= '<td><b>'.$imun_ke.'</b></td>';
				$html .= '</tr>';
				$html .= '<tr>';
					$html .= '<td>&nbsp; - Rtl</td>';
					$html .= '<td>:</td>';
					$html .= '<td><b>'.$rtl.'</b></td>';
					$html .= '<td>&nbsp; -</td>';
					$html .= '<td>Alergi</td>';
					$html .= '<td>:</td>';
					$html .= '<td><b>'.$alergi.'</b></td>';
				$html .= '</tr>';
				$html .= '<tr>';
					$html .= '<td>&nbsp; - Keluhan</td>';
					$html .= '<td>:</td>';
					$html .= '<td><b>'.$keluhan.'</td>';
					$html .= '<td>&nbsp; -</td>';
					$html .= '<td>Pemeriksaan</td>';
					$html .= '<td>:</td>';
					$html .= '<td><b>'.$pemeriksaan.'</b></td>';
				$html .= '</tr>';
				/*$html .= '<tr>';
					$html .= '<td class="p-2" colspan="7">';
						$html .= '<button class="btn btn-primary btn-sm edit-pemeriksaan">Edit</button>';
					$html .= '</td>';
				$html .= '</tr>';*/
			$html .= '</table>';
			return $html;
	}
	/*
	End module tindakan
	*/

	/*
	Start module soap
	*/
	private function tableSoap($no_rawat)
	{
		$this->load->model(array('Catatanperawatan_model'));
		$res = $this->Catatanperawatan_model->get('','*',array('no_rawat'=>$no_rawat))->result();
		$expsoap = explode('\n',@$res[0]->catatan);
		foreach($expsoap as $k => $v)
        {	
        	$frist = substr($v,0,1);
        	$d[strtolower($frist)] = $v;
        }
		$html = '<table class="" style="padding: 0px; margin: 0px; width:100%;">';
				$html .= '<tr style="padding: 0px; margin: 0px;">';
					$html .= '<td width="10">S</td>';
					$html .= '<td width="10">:</td>';
					$html .= '<td><b>'.@$d['s'].'</b></td>';
				$html .= '</tr>';
				$html .= '<tr>';
					$html .= '<td>O</td>';
					$html .= '<td>:</td>';
					$html .= '<td><b>'.@$d['o'].'</b></td>';
				$html .= '</tr>';
				$html .= '<tr>';
					$html .= '<td>A</td>';
					$html .= '<td>:</td>';
					$html .= '<td><b>'.@$d['a'].'</b></td>';
				$html .= '</tr>';
				$html .= '<tr>';
					$html .= '<td>P</td>';
					$html .= '<td>:</td>';
					$html .= '<td><b>'.@$d['p'].'</b></td>';
				$html .= '</tr>';
			$html .= '</table>';
			return $html;
	}

	public function history_soap_last()
	{
		$post = $this->input->post();
		$this->load->model(array('Catatanperawatan_model'));
		$res = $this->Catatanperawatan_model->get('','*',array('no_rawat'=>$post['no_rawat']))->result();
		$expsoap = explode('\n',@$res[0]->catatan);
		
		$no = 1;

		$soap = [
			's' => '<div class="form-group">
		<label>S (subjektif) :  Data subjektif Berisi data dari pasien melalui anamnesis (wawancara) yang merupakan ungkapan langsung.</label>
		<textarea class="form-control" name="s">',
		'o' => '<div class="form-group">
		<label>O (objektif) :  Data objektif Data yang dari hasil observasi melalui pemeriksaan fisik.</label>
		<textarea class="form-control" name="o">',
		'a' => '<div class="form-group">
		<label>A (assesment) : Analisis dan interpretasi Berdasarkan data yang terkumpul kemudian dibuat kesimpulan yang meliputi diagnosis, antisipasi diagnosis atau masalah potensial, serta perlu tidaknya dilakukan tindakan segera.</label>
		<textarea class="form-control" name="a">',
		'p' => '<div class="form-group">
		<label>P (plan) : Perencanaan Merupakan rencana dari tindakan yang akan diberikan termasuk asuhan mandiri, kolaborasi, diagnosis atau labolatorium, serta konseling untuk tindak lanjut.</label>
		<textarea class="form-control" name="p">'
		];

		if($res)
		{
			$html = '<form method="post" id="formsimpan_pemeriksaan_history" action="#">';
			foreach($expsoap as $k => $v)
	        {	
	        	$html .= '<div class="form-group">';
	        	$frist = substr($v,0,1);
	        	$d[strtolower($frist)] = $v;
	        	$html .= @$soap[strtolower(@$frist)] . substr(@$v,2) .'</textarea>';
	        	$html .= '</div>';
	        }

	        $html .= '<button type="button" id="simpanpemeriksaan_history" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Copy Riwayat</button>';
	        $html .= '</form>';
		}
		else
		{
			$html = '<p>Data tidak tersedia.</p>';
		}
		
       	echo $html;
	}

	public function norawatTglkunjungan()
	{
		$tahun = $this->uri->segment(3);
		$bulan = $this->uri->segment(4);
		$hari = $this->uri->segment(5);
		$d = $tahun . '-' . $bulan .'-'. $hari;
		$s = $this->instrument->indoDays($d);
		$html = '<table class="table table-bordered table-striped mb-2" style="padding: 0px; margin: 0px; width:100%;">';
			$html .= '<tr>';
				$html .= '<td class="text-center" colspan="3"><b>DATA TANGGAL RIWAYAT</b></td>';
			$html .= '</tr>';
			$html .= '<tr>';
				$html .= '<td width="160">Tanggal Kunjungan</td>';
				$html .= '<td width="10">:</td>';
				$html .= '<td>'. $s .' / '. date('d M Y',strtotime($tahun . '-' . $bulan .'-'. $hari)).'</td>';
			$html .= '</tr>';
		$html .= '</table>';
		echo $html;
	}
	/*
	End module soap
	*/

	// fungsi analisis
	private function saveanalisis($post,$kat='',$katform='',$status='')
	{
		global $Cf;
		$timeEx             = explode(':',$post['t']);
        $time               = $timeEx[2] + ($timeEx[1] * 60) + ($timeEx[0] * 60 * 60);

        $kategori = (empty($kat)) ? 'RAJAL' : $kat;

		$analisis['kd_dokter'] = isset($post['kd_dokter']) ? $post['kd_dokter'] : '';
		$analisis['kd_poli'] = isset($post['kd_poli']) ? $post['kd_poli'] : '';
		$analisis['no_rawat'] = isset($post['no_rawat']) ? $post['no_rawat'] : '';
		$analisis['tanggal'] = date('Y-m-d H:i:s');
		$analisis['kategori'] = $kategori;
		$analisis['kategori_form'] = $Cf->_kategoriform[$kategori][$katform];
		$analisis['status'] = $status;
		$analisis['waktu'] = $time;

		return $analisis;
	}

}