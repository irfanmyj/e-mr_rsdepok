<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modules extends backend_controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('Modules_model'));
	}
	
	public function index()
	{
		try {
			global $Cf;
			$post 	= $this->input->post();
			$search	= isset($post['search']) ? $post['search'] : '';
			$limit 	= isset($post['limit']) ? $post['limit'] : $Cf->default_limit;
			$offset = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

			$where = '';
			if($search)
			{
				$where = 'name_module like \'%'.$search.'%\' OR link like \'%'.$search.'%\'';
			} 

			$count 	= $this->Modules_model->count('',$where);
			$get 	= $this->Modules_model->get('','',$where,'','','status',$limit,$offset)->result();
			$get1 	= $this->Modules_model->get('','module_id,name_module',array('status'=>'Y'))->result();
			$row_module= $this->instrument->datasToArray($get1,'module_id','name_module','');
			array_unshift($row_module, '----------');

			$this->instrument->pagination(site_url($this->uri->segment(1).'/index'),$count,$limit);

			//print_r($get);
			$category = array(
				''	=> '------------',
				'top' => 'Top',
				'middle' => 'Middle',
				'left'	=> 'Left'
			);

			$view = array(
				'title' 	=> 'Formulir Administrasi Halaman Module',
				'category' 	=> $category,
				'row' 		=> $get,
				'total_rows'=> $count,
				'search'	=> $search,
				'limit'		=> $Cf->limit_rows,
				'limit_selected'=> $limit,
				'row_module'=> $row_module,
				'access_level'=> $Cf->access_level,
				'status'	=> $Cf->status,
				'folder' 	=> 'modules',
				'breadcrumb'=> $this->mybreadcrumb->render(),
				'file' 		=> 'index'
			);
			/*echo $this->db->version();
			exit();*/
			$this->site->view('inc',$view);	
		} catch (Exception $e) {
			$msg = 'Error : '. $e->getMessage();
			return $msg;
		}
	}

	public function save()
	{
		try {
			global $Cf;
			
			$fields = array(
				'category',
				'name_module',
				'link',
				'view',
				'add',
				'update',
				'delete',
				'download',
				'upload',
				'icon',
				'atribut',
				'status');

			$datas = $this->instrument->_post($fields);

			$rules = $this->Modules_model->rules;
			$this->form_validation->set_rules($rules);

			if($this->input->post('id'))
			{
				if($this->form_validation->run() == FALSE)
				{
					$_SESSION['error'] = '1';
					$_SESSION['msg']   = 'Data <b> TIDAK </b> berhasil diperbarui, silakan coba lagi nanti.';
				}
				else
				{
					$_SESSION['error'] = '';
					$_SESSION['msg']   = 'Data telah berhasil diperbarui.';
					$datas['parent_id'] = isset($_POST['parent_id']) ? $_POST['parent_id'] : '';
					$this->Modules_model->update('',$datas,array('module_id'=>$this->input->post('id')));
				}
			}
			else
			{
				if($this->form_validation->run() == FALSE)
				{
					$_SESSION['error'] = '1';
					$_SESSION['msg']   = 'Data <b> TIDAK </b> berhasil disimpan, silakan coba lagi nanti.';
				}
				else
				{
					$_SESSION['error'] = '';
					$_SESSION['msg']   = 'Data telah berhasil simpan.';
					// Get SORT
					$wh_sort = array('parent_id'=>$_POST['parent_id'], 'category'=>'top');
					$get_sort = $this->Modules_model->count('',$wh_sort);
					$datas['sort'] = $get_sort+1;
					$datas['parent_id'] = isset($_POST['parent_id']) ? $_POST['parent_id'] : '';

					$this->Modules_model->insert('',$datas,FALSE);
				}
			}

		} catch (Exception $e) {
			$_SESSION['error'] = '1';
			$_SESSION['msg'] = $e->getMessage();
		}

		$this->instrument->generateReturn(base_url($this->uri->segment(1).'/index'));

	}

	public function delete($data='')
	{
		if(strpos($data, 'k') > 0)
		{
			$idls = explode('k', $data);
			if(is_array($idls))
			{
				foreach ($idls as $idv)
				{
					$det = $this->Modules_model->get('','name_module','module_id=\''.$idv.'\'');
					if($this->Modules_model->delete('',array('module_id'=>$idv)))
					{
						$_SESSION['error'] = '';
		            	$_SESSION['msg']   = 'Data telah berhasil dihapus.';
					}
					else
					{
						$_SESSION['error'] = '1';
		        		$_SESSION['msg']   = 'Data <b> TIDAK </b> berhasil dihapus, silakan coba lagi nanti.';
						//$this->Get_model->createHistory('Delete bahasa dengan nama : '.$det[0]->bahasa.'',$this->session->userdata('ID'));
					}
				}
			}	
		}
		else
		{
			if($this->Modules_model->delete('',array('module_id'=>$data)))
			{
				$_SESSION['error'] = '';
		        $_SESSION['msg']   = 'Data telah berhasil dihapus.';
			}
			else
			{
				$_SESSION['error'] = '1';
		       	$_SESSION['msg']   = 'Data <b> TIDAK </b> berhasil dihapus, silakan coba lagi nanti.';
			}
		}
		
		$this->instrument->generateReturn(base_url($this->uri->segment(1).'/index'));
	}
}
