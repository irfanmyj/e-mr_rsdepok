<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends backend_controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('Auth_model','Dokter_model','Reset_model','Jadwaldokter_model'));
		$this->load->library('email');
	}
	
	public function index()
	{
		$datas = array(
			'file' => 'login',
			'folder' => 'page'
		);
		$this->site->view('inc',$datas);
	}

	public function login()
	{
		$datas = array(
			'file' => 'login',
			'folder' => 'page'
		);
		$this->site->view('inc',$datas);
	}

	public function checking()
	{
		try {
			global $Cf;
			$fields = array(
				'username',
				'password'
			);
			$datas = $this->instrument->_post($fields);
			$rules = $this->Auth_model->rules;
			$this->form_validation->set_rules($rules);
			
			if($this->form_validation->run() == FALSE)
			{
				$url 	= base_url('auth/login');
				$error 	= 1;
				throw new Exception(validation_errors());
			}
			else
			{
				$get_user = $this->Auth_model->get('',"AES_DECRYPT(id_user,'nur') as id_user, AES_DECRYPT(password,'windi') as password, password as password2","id_user = AES_ENCRYPT('{$datas['username']}','nur')",'','','',1)->result();
				/*print_r($get_user);
				exit();*/
				if(!$get_user)
				{
					$url 	= base_url('auth/login');
					$error = 1;
					throw new Exception('Kode dokter tidak terdaftar atau tidak aktif.');
				}
				else
				{
					$hari = $this->instrument->indoDays(date('Y-m-d'));
					$get_dokter = $this->Dokter_model->get('','nm_dokter,jk,tgl_lahir',array('kd_dokter'=>$get_user[0]->id_user))->result();
					$get_jawdal = $this->Jadwaldokter_model->get('','kd_poli',array('kd_dokter'=>$get_user[0]->id_user,'hari_kerja'=>$hari))->result();
					/*print_r($get_jawdal);
					exit();*/
					if($get_user[0]->password==$datas['password'])
					{			
						$login_data = array(
							'id_user' => $get_user[0]->id_user,
							'nm_dokter' => $get_dokter[0]->nm_dokter,
							'jk' => $get_dokter[0]->jk,
							'tgl_lahir' => $get_dokter[0]->tgl_lahir,
							'id_level' => 'Dokter',
							'kd_poli' => $get_jawdal,
							'status' =>  'Y',
							'logged_in' => TRUE,
						);
						
						$this->session->set_userdata($login_data);

						$url 	= base_url('dashboard/index');
						$error = '';
						throw new Exception('Selamat Anda Telah Berhasil Login.');
					}
					else if($get_user[0]->password2==sha1($datas['password'].$datas['username']))
					{
						$login_data = array(
							'id_user' => $get_user[0]->id_user,
							'nm_dokter' => $get_dokter[0]->nm_dokter,
							'jk' => $get_dokter[0]->jk,
							'tgl_lahir' => $get_dokter[0]->tgl_lahir,
							'id_level' => 'Dokter',
							'kd_poli' => $get_jawdal,
							'status' =>  'Y',
							'logged_in' => TRUE,
						);
						
						$this->session->set_userdata($login_data);

						$url 	= base_url('dashboard/index');
						$error = '';
						throw new Exception('Selamat Anda Telah Berhasil Login.');
					}
					
					else
					{
						$url 	= base_url('auth/login');
						$error = 1;
						throw new Exception('Kata kunci tidak valid1.');
					}
				}
			}

		} catch (Exception $e) {
			$_SESSION['error'] = $error;
			$_SESSION['msg'] = $e->getMessage();
		}
		//echo $_SESSION['msg'];
		$this->instrument->generateReturn($url);
	}

	public function lupapassword()
	{
		try {
			global $Cf;
				$post = $this->input->post();
				$fields = array(
					'kd_dokter',
					'email'
				);

				$datas = $this->instrument->_post($fields);
				$data['code'] = isset($datas['kd_dokter']) ? sha1($datas['kd_dokter'].date('Y-m-d H:i:s')) : '';	
				$data['kd_dokter'] = isset($datas['kd_dokter']) ? $datas['kd_dokter'] : '';	
				$data['date'] = date('Y-m-d H:i:s');
				$data['sts_reset'] = 'N';

				$link = base_url('auth/validasi/'.$data['code']);

			    $this->email->initialize($Cf->configEmail);


			    $this->email->from('rsuddepokinfo@gmail.com', 'Rsud Depok Informasi');
			    $this->email->to($datas['email']); 

			    // Lampiran email, isi dengan url/path file
		        $this->email->attach('http://rsud.depok.go.id/wp-content/uploads/2017/09/pp.jpg');

			    // Subject email
		        $this->email->subject('ALAMAT LINK RESET PASSWORD APLIKASI DOKTER RSUD DEPOK');
		        // Isi email
		        $this->email->message('Ini adalah email informasi reset password yang dikirm aplikasi dokter rsud depok.<br><br> Untuk memulai membuat password baru silahkan klik link ini : <strong><a href='.$link.' target="_blank" rel="noopener">disini</a></strong> untuk melihat tutorialnya.');

		        if ($this->email->send()) {
		            $error = '';
					if($this->Reset_model->insert('',$data,false))
					{
						$error = 1;
						throw new Exception('Reset <b> PASSWORD </b> gagal.');
					}
					else
					{
						$error = '';
						throw new Exception('Link reset password telah dikirm ke email anda.');
					}
		        } 
		        else 
		        {
		            $error = 1;
					throw new Exception('Email tidak terkirim '. $this->email->print_debugger());
		        }
			} catch (Exception $e) {
				$ds['error'] = $error;
				$ds['msg'] = $e->getMessage();
			}	

			$dt = array(
				'error' => $ds['error'],
				'msg' => $ds['msg']
			);
			echo json_encode($dt);
			//$this->instrument->generateReturn(base_url($this->uri->segment(1).'/login'));
	}

	public function cekdokter()
	{
		//$post = $this->input->post();
		$fields = array(
			'kode_dokter'
		);
		$datas = $this->instrument->_post($fields);

		if(!empty($datas['kode_dokter']))
		{
			$res = $this->Dokter_model->count('',array('kd_dokter'=>$datas['kode_dokter']));

		}

		$data = array(
			'jumlah' => $res
		);

		echo json_encode($data);
	}

	public function cekemail()
	{
		//$post = $this->input->post();
		$fields = array(
			'kode_dokter',
			'email'
		);
		$datas = $this->instrument->_post($fields);

		if(!empty($datas['email']))
		{
			$res = $this->Dokter_model->count('',array('email'=>$datas['email'],'kd_dokter'=>$datas['kode_dokter']));

		}

		$data = array(
			'jumlah' => $res
		);

		echo json_encode($data);
	}

	public function sendemail($toemail='',$link='')
	{
		$config['protocol']    	= 'smtp';
	    $config['smtp_host']    = 'ssl://smtp.googlemail.com';
	    $config['smtp_port']    = 465;
	    $config['smtp_timeout'] = '7';
	    $config['smtp_user']    = 'irfanmyj@gmail.com';
	    $config['smtp_pass']    = 'bismilahirahmanirahim';
	    $config['charset']    	= 'utf-8';
	    $config['newline']    	= "\r\n";
	    $config['mailtype'] 	= 'html'; // or html
	    //$config['validation'] 	= TRUE; // bool whether to validate email or not      
	    $this->load->library('email');

	    $this->email->initialize($config);


	    $this->email->from('irfanmyj@gmail.com', 'irfanmyj');
	    $this->email->to($toemail); 

	    // Lampiran email, isi dengan url/path file
        $this->email->attach('https://masrud.com/content/images/20181215150137-codeigniter-smtp-gmail.png');

	    // Subject email
        $this->email->subject('ALAMAT LINK RESET PASSWORD APLIKASI DOKTER RSUD DEPOK');
        // Isi email
        $this->email->message("Ini adalah contoh email CodeIgniter yang dikirim menggunakan SMTP email Google (Gmail).<br><br> Klik <strong><a href='https://masrud.com/post/kirim-email-dengan-smtp-gmail' target='_blank' rel='noopener'>disini</a></strong> untuk melihat tutorialnya.");
  

	    // Tampilkan pesan sukses atau error
        if ($this->email->send()) {
            $error = '';
			throw new Exception('Email terkirim.');
        } else {
            $error = 1;
			throw new Exception('Email tidak terkirim '. $this->email->print_debugger());
        }

	    

	     //$this->load->view('email_view');
	}
	
	public function validasi($code= '')
	{
		$id = isset($code) ? $code : '';
		$res = $this->Reset_model->get('','*',array('code'=>$id))->result();
		if($res[0]->sts_reset=='N')
		{
			$datas = array(
				'title' => 'Reset Password',
				'kd_dokter' => $res[0]->kd_dokter,
				'code' => $res[0]->code,
				'file' => 'reset',
				'folder' => 'page'
			);
		}
		else
		{
			$datas = array(
				'title' => 'Reset Password',
				'kd_dokter' => $res[0]->kd_dokter,
				'code' => $res[0]->code,
				'file' => 'resets',
				'folder' => 'page'
			);
		}
		$this->site->view('inc',$datas);
	}

	public function updatepassword()
	{
		global $Cf;
		$post = $this->input->post();

		$fields = array(
			'code',
			'kd_dokter',
			'password'
		);
		$datas = $this->instrument->_post($fields);
		$data = array(
			'password2' => isset($datas) ? sha1($datas['password'].$datas['kd_dokter']) : ''
		);
		$dtemail = $this->Dokter_model->get('','email',array('kd_dokter'=>$datas['kd_dokter']))->result();

		$data1 = array(
			'sts_reset' => 'Y',
			'password' => $datas['password']
		);

		$this->email->initialize($Cf->configEmail);
	    $this->email->from('rsuddepokinfo@gmail.com', 'Rsud Depok Informasi');
	    $this->email->to($dtemail[0]->email); 

	    // Lampiran email, isi dengan url/path file
        $this->email->attach('http://rsud.depok.go.id/wp-content/uploads/2017/09/pp.jpg');

	    // Subject email
        $this->email->subject('DATA PERUBAHAN PASSWORD ANDA');
        // Isi email
        $this->email->message('Mohon simpan akun anda dengan baik, jangan beritahu siapapun. <br>Username : '.$datas['kd_dokter'].' <br> Password : '.$datas['password'].'');
        if ($this->email->send()){
        	if($this->Auth_model->update('',$data,"id_user = AES_ENCRYPT('{$datas['kd_dokter']}','nur')"))
			{
				$this->Reset_model->update('',$data1,array('code'=>$datas['code']));
				$error = '';
				$msg = 'Password berhasil di reset.';
			}
			else
			{
				$error = 1;
				$msg = 'Password gagal di reset.';
			}
        }
        else
        {
        	$error = 1;
			$msg = 'Email gagal mengirim data.';
        }

		echo json_encode(array('error'=>$error,'msg'=>$msg));
	}

	public function logout(){
		//$this->Get_model->createHistory('Akun ini berhasil logout :'.$this->session->userdata('nm_lengkap').'',$this->session->userdata('id_user'));
		$this->session->sess_destroy();
		redirect(base_url('auth/login'));
	}
}
