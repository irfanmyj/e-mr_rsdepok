<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Booking extends backend_controller {

	public function __construct(){
		parent::__construct();
		$this->load->model(array('Booking_model'));
		$this->site->is_logged_in();
	}

	public function index()
	{
		global $Cf;

		$keys 	= array('search');
		$datas 	= $this->instrument->_post($keys);
		$search	= isset($datas['search']) 	? $datas['search'] : '';
		$limit 	= isset($post['limit']) 	? $post['limit'] : '';
		$offset = ($this->uri->segment(3)) 	? $this->uri->segment(3) : 0;
		$dNow 	= date('Y-m-d');
		$dNext 	= $this->instrument->AddDate($dNow,$Cf->dayShow,'days');

		$where = array(
			'booking_registrasi.kd_dokter' => $this->session->userdata('id_user'),
			'booking_registrasi.tanggal_periksa' => $dNow
		);

		if($search)
		{
			$where = '
				booking_registrasi.no_rkm_medis like \'%'.$search.'%\'
				and booking_registrasi.kd_dokter = \''.$this->session->userdata('id_user').'\'
				and booking_registrasi.tanggal_periksa >= \''.$dNow.'\'
			';
		}

		$res_booking 	= $this->Booking_model->getPasienRajal($where,$limit,$offset);
		$data = array(
			'title'		=> 'Jumlah Data Pasien Booking',
			'titled'	=> 'Jumlah Data Pasien Booking Tanggal '. date('d M Y',strtotime($dNow)) .' Sampai '. date('d M Y',strtotime($dNext)),
			'row' 		=> $res_booking,
			'file'		=> 'index',
			'folder'	=> 'booking'
		);
		
		$this->site->view('inc',$data);
	}
}
