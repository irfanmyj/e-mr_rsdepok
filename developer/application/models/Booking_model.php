<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking_model extends MY_Model{

	protected $_table_name = 'booking_registrasi';
	protected $_primary_key = 'no_rkm_medis';
	protected $_order_by = 'booking_registrasi.tanggal_periksa ASC';
	protected $_order_by_type = '';

	private $tbjoin = array(
			'pasien' 	 => array(
				'metode' => 'INNER',
				'relasi' => 'pasien.no_rkm_medis=booking_registrasi.no_rkm_medis'
			),
			'poliklinik' => array( // nama tabel
				'metode' => 'INNER', // metode join
				'relasi' => 'poliklinik.kd_poli=booking_registrasi.kd_poli' // filed relasi tabel antar tabel
			),
			'penjab' => array(
				'metode' => 'INNER',
				'relasi' => 'penjab.kd_pj=booking_registrasi.kd_pj'
			)
		);

	private $field = '
			booking_registrasi.no_reg,
			booking_registrasi.waktu_kunjungan,
			booking_registrasi.no_rkm_medis,
			booking_registrasi.tanggal_periksa,
			booking_registrasi.tanggal_booking,
			booking_registrasi.jam_booking,
			booking_registrasi.status,
			poliklinik.nm_poli,
			penjab.png_jawab,
			pasien.nm_pasien,
			pasien.tgl_lahir
		';

	public function __construct(){
		parent::__construct();
	}

	public function getPasienRajal($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','',$this->_order_by,$limit,$offset)->result();
	}
}