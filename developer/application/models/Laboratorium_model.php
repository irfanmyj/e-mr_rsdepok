<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laboratorium_model extends MY_Model{

	protected $_table_name = 'detail_periksa_lab';
	protected $_primary_key = 'no_rawat';
	protected $_order_by = '';
	protected $_order_by_type = '';

	public function __construct(){
		parent::__construct();
	}

	private $field4 = '
		detail_periksa_lab.kd_jenis_prw,
		detail_periksa_lab.nilai,
		detail_periksa_lab.nilai_rujukan,
		detail_periksa_lab.keterangan,
		jns_perawatan_lab.nm_perawatan
	';

	private $tbjoin4 = array(
		'jns_perawatan_lab' => array(
			'metode' => 'inner',
			'relasi' => 'jns_perawatan_lab.kd_jenis_prw=detail_periksa_lab.kd_jenis_prw'
		)
	);

	public function getHistoryDetailPeriksa($where)
	{
		return $this->getJoin('',$this->tbjoin4,$this->field4,$where)->result();
	}

}