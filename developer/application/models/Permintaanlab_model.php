<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permintaanlab_model extends MY_Model{

	protected $_table_name = 'permintaan_lab';
	protected $_primary_key = 'noorder';
	protected $_order_by = '';
	protected $_order_by_type = '';

	public function __construct(){
		parent::__construct();
	}

	private $fieldLabs = '
    	permintaan_lab.noorder,
		permintaan_lab.no_rawat,
		permintaan_pemeriksaan_lab.kd_jenis_prw,
		jns_perawatan_lab.nm_perawatan
	';

	private $tbjoinLabs = array(
		'permintaan_pemeriksaan_lab' => array(
			'metode' => 'inner',
			'relasi' => 'permintaan_pemeriksaan_lab.noorder=permintaan_lab.noorder'
		),
		'jns_perawatan_lab' => array(
			'metode' => 'inner',
			'relasi' => 'jns_perawatan_lab.kd_jenis_prw=permintaan_pemeriksaan_lab.kd_jenis_prw'
		)
	);

	public function getPermintaanLab($where)
	{
		return $this->getJoin('',$this->tbjoinLabs,$this->fieldLabs,$where)->result();
	}

}