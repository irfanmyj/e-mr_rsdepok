<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resepdokter_model extends MY_Model{

	protected $_table_name = 'resep_dokter';
	protected $_primary_key = '';
	protected $_order_by = '';
	protected $_order_by_type = '';

	public function __construct(){
		parent::__construct();
	}

}