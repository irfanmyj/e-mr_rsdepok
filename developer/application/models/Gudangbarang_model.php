<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gudangbarang_model extends MY_Model{

	protected $_table_name = 'gudangbarang';
	protected $_primary_key = 'kode_brng';
	protected $_order_by = '';
	protected $_order_by_type = '';

	public function __construct(){
		parent::__construct();
	}

}