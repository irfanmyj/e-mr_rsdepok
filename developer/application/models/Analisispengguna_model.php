<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Analisispengguna_model extends MY_Model{

	protected $_table_name = 'analisis_pengguna';
	protected $_primary_key = 'id';
	protected $_order_by = '';
	protected $_order_by_type = '';

	public function __construct(){
		parent::__construct();
	}

}