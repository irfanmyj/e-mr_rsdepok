<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dokter_model extends MY_Model{

	protected $_table_name = 'dokter';
	protected $_primary_key = 'kd_dokter';
	protected $_order_by = 'kd_dokter';
	protected $_order_by_type = 'ASC';

	public $rules = array(
		'kd_dokter' => array(
            'field' => 'kd_dokter',
            'label' => 'Kode Dokter',
            'rules' => 'trim|required'
		),
		'nm_dokter' => array(
            'field' => 'nm_dokter',
            'label' => 'Nama Dokter',
            'rules' => 'trim|required'
		)
	);

	public function __construct(){
		parent::__construct();
	}

}