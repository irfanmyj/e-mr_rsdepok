<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rawatinap_model extends MY_Model{

	protected $_table_name = 'reg_periksa';
	protected $_primary_key = 'no_rawat';
	protected $_order_by = '';
	protected $_order_by_type = '';

	public function __construct(){
		parent::__construct();
	}

	private $tbjoin = array(
		'pasien' 	 => array(
			'metode' => 'INNER',
			'relasi' => 'pasien.no_rkm_medis=reg_periksa.no_rkm_medis'
		),
		'poliklinik' => array( // nama tabel
			'metode' => 'INNER', // metode join
			'relasi' => 'poliklinik.kd_poli=reg_periksa.kd_poli' // filed relasi tabel antar tabel
		),
		'penjab' => array(
			'metode' => 'INNER',
			'relasi' => 'penjab.kd_pj=reg_periksa.kd_pj'
		)
	);

	private $field = '
		reg_periksa.no_reg,
		reg_periksa.no_rawat,
		reg_periksa.no_rkm_medis,
		reg_periksa.tgl_registrasi,
		reg_periksa.jam_reg,
		reg_periksa.umurdaftar,
		reg_periksa.stts,
		poliklinik.nm_poli,
		penjab.png_jawab,
		pasien.nm_pasien,
		pasien.tgl_lahir,
		pasien.jk
	';

	public function getPaseinRajal($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','reg_periksa.no_reg ASC',$limit,$offset)->result();
	}

	private $tbjoin1 = array(
		'poliklinik' => array(
			'metode' => 'INNER',
			'relasi' => 'poliklinik.kd_poli=reg_periksa.kd_poli'
		),
		'pasien' => array(
			'metode' => 'INNER',
			'relasi' => 'pasien.no_rkm_medis=reg_periksa.no_rkm_medis'
		),
		'penjab' => array(
			'metode' => 'INNER',
			'relasi' => 'penjab.kd_pj=reg_periksa.kd_pj'
		),
		'bahasa_pasien' => array(
			'metode' => 'INNER',
			'relasi' => 'bahasa_pasien.id=pasien.bahasa_pasien'
		),
		'kamar_inap' => array(
			'metode' => 'INNER',
			'relasi' => 'kamar_inap.no_rawat=reg_periksa.no_rawat'
		),
		'kamar' => array(
			'metode' => 'INNER',
			'relasi' => 'kamar.kd_kamar=kamar_inap.kd_kamar'
		),
		'dpjp_ranap' => array(
			'metode' => 'INNER',
			'relasi' => 'dpjp_ranap.no_rawat=reg_periksa.no_rawat'
		),
		'bangsal' => array(
			'metode' => 'INNER',
			'relasi' => 'bangsal.kd_bangsal=kamar.kd_bangsal'
		)
	);

	private $field1 = '
		reg_periksa.no_reg,
		reg_periksa.no_rkm_medis,
		reg_periksa.stts,
		reg_periksa.status_poli,
		reg_periksa.kd_poli,
		reg_periksa.tgl_registrasi,
		bangsal.nm_bangsal as nm_poli,
		pasien.nm_pasien,
		pasien.tgl_lahir,
		pasien.jk,
		penjab.png_jawab,
		bahasa_pasien.nama_bahasa
	';
	public function getPaseinRajalAll($where='')
	{
		return $this->getJoin('',$this->tbjoin1,$this->field1,$where)->result();
	}

	private $tbjoin2 = array(
		'pasien' => array(
			'metode' => 'INNER',
			'relasi' => 'pasien.no_rkm_medis=reg_periksa.no_rkm_medis'
		),
		'kamar_inap' => array(
			'metode' => 'INNER',
			'relasi' => 'kamar_inap.no_rawat=reg_periksa.no_rawat'
		),
		'dpjp_ranap' => array(
			'metode' => 'INNER',
			'relasi' => 'dpjp_ranap.no_rawat=reg_periksa.no_rawat'
		)
	);

	private $field2 = '
		reg_periksa.no_reg,
		reg_periksa.no_rkm_medis,
		reg_periksa.no_rawat,
		reg_periksa.stts,
		reg_periksa.tgl_registrasi,
		pasien.nm_pasien
	';

	private $orderby2 = 'reg_periksa.no_reg ASC';

	public function getPaseinRajal2($where='')
	{
		return $this->getJoin('',$this->tbjoin2,$this->field2,$where,'reg_periksa.no_rawat','',$this->orderby2)->result();
	}


	private $tbjoin3 = array(
		
	);

	private $field3 = '
		reg_periksa.no_rawat,
		reg_periksa.tgl_registrasi,
		reg_periksa.status_lanjut
	';

	private $orderby3 = 'reg_periksa.tgl_registrasi DESC';

	public function getPaseinRajal3($where='')
	{
		return $this->getJoin('',$this->tbjoin3,$this->field3,$where,'reg_periksa.no_rawat','',$this->orderby3)->result_array();
	}

}