<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rawatjalan_model extends MY_Model{

	protected $_table_name = 'reg_periksa';
	protected $_primary_key = 'no_rawat';
	protected $_order_by = '';
	protected $_order_by_type = '';
	public function __construct(){
		parent::__construct();
	}

	private $tbjoin = array(
		'pasien' 	 => array(
			'metode' => 'INNER',
			'relasi' => 'pasien.no_rkm_medis=reg_periksa.no_rkm_medis'
		),
		'poliklinik' => array( // nama tabel
			'metode' => 'INNER', // metode join
			'relasi' => 'poliklinik.kd_poli=reg_periksa.kd_poli' // filed relasi tabel antar tabel
		),
		'penjab' => array(
			'metode' => 'INNER',
			'relasi' => 'penjab.kd_pj=reg_periksa.kd_pj'
		)
	);

	private $field = '
		reg_periksa.no_reg,
		reg_periksa.no_rawat,
		reg_periksa.no_rkm_medis,
		reg_periksa.tgl_registrasi,
		reg_periksa.jam_reg,
		reg_periksa.umurdaftar,
		reg_periksa.stts,
		poliklinik.nm_poli,
		penjab.png_jawab,
		pasien.nm_pasien,
		pasien.tgl_lahir,
		pasien.jk
	';

	public function getPaseinRajal($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','reg_periksa.no_reg ASC',$limit,$offset)->result();
	}

	private $tbjoin1 = array(
		'poliklinik' => array(
			'metode' => 'INNER',
			'relasi' => 'poliklinik.kd_poli=reg_periksa.kd_poli'
		),
		'pasien' => array(
			'metode' => 'INNER',
			'relasi' => 'pasien.no_rkm_medis=reg_periksa.no_rkm_medis'
		),
		'penjab' => array(
			'metode' => 'INNER',
			'relasi' => 'penjab.kd_pj=reg_periksa.kd_pj'
		),
		'bahasa_pasien' => array(
			'metode' => 'INNER',
			'relasi' => 'bahasa_pasien.id=pasien.bahasa_pasien'
		)
	);

	private $field1 = '
		reg_periksa.no_reg,
		reg_periksa.no_rkm_medis,
		reg_periksa.stts,
		reg_periksa.status_poli,
		reg_periksa.kd_poli,
		reg_periksa.kd_pj,
		poliklinik.nm_poli,
		pasien.nm_pasien,
		pasien.tgl_lahir,
		pasien.jk,
		penjab.png_jawab,
		bahasa_pasien.nama_bahasa
	';
	public function getPaseinRajalAll($where='')
	{
		return $this->getJoin('',$this->tbjoin1,$this->field1,$where)->result();
	}

	private $tbjoin2 = array(
		'pasien' => array(
			'metode' => 'INNER',
			'relasi' => 'pasien.no_rkm_medis=reg_periksa.no_rkm_medis'
		)
	);

	private $field2 = '
		reg_periksa.no_reg,
		reg_periksa.no_rkm_medis,
		reg_periksa.no_rawat,
		reg_periksa.stts,
		reg_periksa.tgl_registrasi,
		pasien.nm_pasien
	';

	private $orderby2 = 'no_reg ASC, jam_reg ASC';

	public function getPaseinRajal2($where)
	{
		return $this->getJoin('',$this->tbjoin2,$this->field2,$where,'','',$this->orderby2)->result();
	}


	private $tbjoin3 = array(
		'poliklinik' => array(
			'metode' => 'INNER',
			'relasi' => 'poliklinik.kd_poli=reg_periksa.kd_poli'
		)
	);

	private $field3 = '
		reg_periksa.no_rawat,
		reg_periksa.tgl_registrasi,
		poliklinik.nm_poli
	';

	private $orderby3 = 'reg_periksa.tgl_registrasi DESC';

	public function getPaseinRajal3($where='')
	{
		return $this->getJoin('',$this->tbjoin3,$this->field3,$where,'','',$this->orderby3)->result_array();
	}

	private $field4 = '
		reg_periksa.no_reg,
		reg_periksa.no_rkm_medis,
		reg_periksa.no_rawat,
		pasien.nm_pasien
	';

	private $tbjoin4 = array(
		'pasien' => array(
			'metode' => 'INNER',
			'relasi' => 'pasien.no_rkm_medis=reg_periksa.no_rkm_medis'
		)
	);

	private $orderby4 = 'no_reg ASC, jam_reg ASC';

	public function getAntrianPasien($where)
	{
		return $this->getJoin('',$this->tbjoin4,$this->field4,$where,'','',$this->orderby4)->result();
	}

	private $field5 = '
		reg_periksa.no_rawat,
		reg_periksa.kd_poli,
		reg_periksa.tgl_registrasi,
		poliklinik.nm_poli,
		dokter.nm_dokter
	';

	private $tbjoin5 = array(
		'poliklinik' => array(
			'metode' => 'inner',
			'relasi' => 'poliklinik.kd_poli=reg_periksa.kd_poli'
		),
		'dokter' => array(
			'metode' => 'inner',
			'relasi' => 'dokter.kd_dokter=reg_periksa.kd_dokter'
		)
	);

	public function getHistoryRegperiksa($where)
	{
		return $this->getJoin('',$this->tbjoin5,$this->field5,$where)->result();
	}

	public function getPaseinRajal4($where='')
	{
		return $this->get('','no_rawat',$where,'','','tgl_registrasi DESC',2)->result_array();
	}

}