<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jnsperawatanradiologi_model extends MY_Model{

	protected $_table_name = 'jns_perawatan_radiologi';
	protected $_primary_key = 'kd_jenis_prw';
	protected $_order_by = '';
	protected $_order_by_type = '';

	public function __construct(){
		parent::__construct();
	}

}