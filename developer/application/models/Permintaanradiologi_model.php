<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permintaanradiologi_model extends MY_Model{

	protected $_table_name = 'permintaan_radiologi';
	protected $_primary_key = 'noorder';
	protected $_order_by = '';
	protected $_order_by_type = '';

	public function __construct(){
		parent::__construct();
	}

	private $fieldRad = '
    	permintaan_radiologi.noorder,
		permintaan_radiologi.no_rawat,
		permintaan_pemeriksaan_radiologi.kd_jenis_prw,
		jns_perawatan_radiologi.nm_perawatan
	';

	private $tbjoinRad = array(
		'permintaan_pemeriksaan_radiologi' => array(
			'metode' => 'inner',
			'relasi' => 'permintaan_pemeriksaan_radiologi.noorder=permintaan_radiologi.noorder'
		),
		'jns_perawatan_radiologi' => array(
			'metode' => 'inner',
			'relasi' => 'jns_perawatan_radiologi.kd_jenis_prw=permintaan_pemeriksaan_radiologi.kd_jenis_prw'
		)
	);

	public function getPermintaanRad($where)
	{
	   return $this->getJoin('',$this->tbjoinRad,$this->fieldRad,$where)->result();
	}

}