<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Diagnosapasien_model extends MY_Model{

	protected $_table_name = 'diagnosa_pasien';
	protected $_primary_key = 'no_rawat';
	protected $_order_by = '';
	protected $_order_by_type = '';

	public function __construct(){
		parent::__construct();
	}


	private $field = '
		diagnosa_pasien.no_rawat,
		diagnosa_pasien.kd_penyakit,
		diagnosa_pasien.prioritas,
		diagnosa_pasien.status_penyakit,
		diagnosa_pasien.status,
		penyakit.nm_penyakit
	';

	private $tbjoin = array(
		'penyakit' => array(
			'metode' => 'inner',
			'relasi' => 'penyakit.kd_penyakit=diagnosa_pasien.kd_penyakit'
		)
	);

	private $orderby = 'diagnosa_pasien.prioritas ASC';

	public function getDiagnosa($where='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','',$this->orderby)->result();
	}

	private $field1 = '
		diagnosa_pasien.kd_penyakit,
		diagnosa_pasien.prioritas,
		penyakit.nm_penyakit
	';

	private $tbjoin1 = array(
		'penyakit' => array(
			'metode' => 'inner',
			'relasi' => 'penyakit.kd_penyakit=diagnosa_pasien.kd_penyakit'
		)
	);

	public function getHistoryDiagnosa($where)
	{
		return $this->getJoin('',$this->tbjoin1,$this->field1,$where,'','','diagnosa_pasien.prioritas asc')->result();
	}
}