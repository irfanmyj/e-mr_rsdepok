<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reset_model extends MY_Model{

	protected $_table_name = 'reset';
	protected $_primary_key = 'code';
	protected $_order_by = 'date';
	protected $_order_by_type = 'ASC';

	public function __construct(){
		parent::__construct();
	}

}