<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Masteraturanpakai_model extends MY_Model{

	protected $_table_name = 'master_aturan_pakai';
	protected $_primary_key = 'aturan';
	protected $_order_by = '';
	protected $_order_by_type = '';

	public function __construct(){
		parent::__construct();
	}

}