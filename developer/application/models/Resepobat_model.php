<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resepobat_model extends MY_Model{

	protected $_table_name = 'resep_obat';
	protected $_primary_key = 'no_resep';
	protected $_order_by = '';
	protected $_order_by_type = '';

	public function __construct(){
		parent::__construct();
	}


	public function getDataObat($where)
	{
		$fieldObt = '
        	resep_obat.no_resep,
			resep_obat.no_rawat,
			resep_dokter.kode_brng,
			resep_dokter.jml,
			resep_dokter.aturan_pakai,
			databarang.nama_brng
		';

		$tbjoinObt = array(
			'resep_dokter' => array(
				'metode' => 'inner',
				'relasi' => 'resep_dokter.no_resep=resep_obat.no_resep'
			),
			'databarang' => array(
				'metode' => 'inner',
				'relasi' => 'databarang.kode_brng=resep_dokter.kode_brng'
			)
		);

		return $this->getJoin('',$tbjoinObt,$fieldObt,$where)->result();
	}
}