<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends MY_Model{

	protected $_table_name = 'user';
	protected $_primary_key = 'id_user';
	protected $_order_by = 'id_user';
	protected $_order_by_type = 'ASC';

	public $rules = array(
		'username' => array(
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'trim|required'
		),
		'password' => array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'trim|required'
		)
	);

	public function __construct(){
		parent::__construct();
	}

	/*public function checking($id_user)
	{
		if($id_user)
		{
			$sql = "SELECT AES_DECRYPT(id_user,'nur') as id_user, AES_DECRYPT(password,'windi') as password FROM user WHERE id_usser = AES_ENCRYPT('{$id_user}','nur')";
			$res = $this->getQuery($sql);
			if($res)
			{
				return $res;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	}*/

}