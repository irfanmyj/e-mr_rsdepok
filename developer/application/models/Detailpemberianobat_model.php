<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detailpemberianobat_model extends MY_Model{

	protected $_table_name = 'detail_pemberian_obat';
	protected $_primary_key = 'no_rawat';
	protected $_order_by = '';
	protected $_order_by_type = '';

	public function __construct(){
		parent::__construct();
	}

	public function getDataObat($norawat)
	{
		$fieldObt = '
        	detail_pemberian_obat.no_rawat,
        	detail_pemberian_obat.kode_brng,
        	detail_pemberian_obat.tgl_perawatan,
			detail_pemberian_obat.jam,
			detail_pemberian_obat.biaya_obat,
			detail_pemberian_obat.jml,
			detail_pemberian_obat.total,
			aturan_pakai.aturan,
			databarang.nama_brng
		';

		$tbjoinObt = array(
			'aturan_pakai' => array(
				'metode' => 'INNER',
				'relasi' => 'aturan_pakai.no_rawat=detail_pemberian_obat.no_rawat and aturan_pakai.kode_brng=detail_pemberian_obat.kode_brng'
			),
			'databarang' => array(
				'metode' => 'INNER',
				'relasi' => 'databarang.kode_brng=detail_pemberian_obat.kode_brng'
			)
		);

		$whereObt = array(
			'detail_pemberian_obat.no_rawat' => $norawat
		);

        return $this->getJoin('',$tbjoinObt,$fieldObt,$whereObt)->result();
	}

}