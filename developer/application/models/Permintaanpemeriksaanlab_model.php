<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permintaanpemeriksaanlab_model extends MY_Model{

	protected $_table_name = 'permintaan_pemeriksaan_lab';
	protected $_primary_key = 'noorder';
	protected $_order_by = '';
	protected $_order_by_type = '';

	public function __construct(){
		parent::__construct();
	}

}