<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permintaanpemeriksaanradiologi_model extends MY_Model{

	protected $_table_name = 'permintaan_pemeriksaan_radiologi';
	protected $_primary_key = 'noorder';
	protected $_order_by = '';
	protected $_order_by_type = '';

	public function __construct(){
		parent::__construct();
	}

}