<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Diagnosapenyakit_model extends MY_Model{

	protected $_table_name = 'penyakit';
	protected $_primary_key = 'kd_penyakit';
	protected $_order_by = '';
	protected $_order_by_type = '';

	public function __construct(){
		parent::__construct();
	}

}